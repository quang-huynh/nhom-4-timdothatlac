'use strict';


var app = {
    init: function() {
        this.common();
        this.dashboard();
        this.item_create_lost();
        this.item_create_found();
        this.item_list();
        this.user();
        this.notification();
        this.editItem();
    },

    common: function() {

    },



    dashboard: function() {
        // $('#dashboard .btn-submit').on('click', function() {
        //     var url = $('#dashboard .btn-submit').data('url');
        //     var data = []; //something

        //     $.ajax({
        //         url: url,
        //         type: 'default GET (Other values: POST)',
        //         dataType: 'json',
        //         data: data,
        //     })
        //     .done(function() {
        //         console.log("success");
        //     })
        //     .fail(function() {
        //         console.log("error");
        //     })
        //     .always(function() {
        //         console.log("complete");
        //     });

        // });
    },


    item_create_lost: function() {
        $('#item_customCategory').change(function() {
            var category_id = $(this).val();
            $.when(getPropertiesByCategoryId(category_id))
                .then(function(data) {
                    var properties = data;
                    showPropertiesByCatergory(properties);

                    return properties;
                });
        });

        function showPropertiesByCatergory(properties) {
            var item_customDetails = $('#item_customDetails');
            item_customDetails.empty();
            var htmlProperty = "";
            for (var index = 0; index < properties.length; index++) {
                var item = properties[index];
                htmlProperty +=
                    "<div class='form-group'>" +
                    "<label class='control-label' for='item_customDetails_field_" + index + "'>" + item.name + "</label>" +
                    "<input type='text' id='item_customDetails_field_" + index + "' name='item[customDetails][" + index + "][description]' data-help='' class='form-control'>" +
                    "<input type='hidden' name='item[customDetails][" + index + "][id]' value=" + item.id + " class='form-control'>" +
                    "</div>";
            }
            item_customDetails.append(htmlProperty);
        }

        function getPropertiesByCategoryId(category_id) {
            var df = jQuery.Deferred();
            var url = "/ItemController/getPropertiesByCategory";
            $.ajax({
                url: url,
                type: "post",
                async: true,
                dataType: "json",
                data: {
                    category_id: category_id,
                    //csrf_test_name : $('meta[name="_token"]').attr('content')
                },
                success: function(result) {
                    console.log("call server api - get properties successfully.");
                    df.resolve(result);
                },
                error: function(result) {
                    console.log("call server api - get properties error.");
                    df.resolve(result);
                }
            });
            return df.promise();
        }
    },


    item_create_found: function() {

        /**
         * @author: ntdat
         * add event get value when user change combobox category
         */
        $('#item_customCategory').change(function() {
            var category_id = $(this).val();
            $.when(getPropertiesByCategoryId(category_id))
                .then(function(data) {
                    var properties = data;
                    showPropertiesByCatergory(properties);
                });
        });

        /**
         * @author: ntdat
         * add event get value when user change combobox location
         */
        $('.item-location-1, .item-location-2, .item-location-3').change(function(e) {
            e.preventDefault();

            var target = e.target;
            var location_id = $(target).val();
            var item = $(target).nextAll(':not(br)');
            var indexItem = $(target).index();
            if (indexItem === 0) {
                if ($.isNumeric(location_id)) {
                    $(item[0]).removeClass("hidden");
                    $(item[1]).addClass("hidden");
                    showLocationChild(location_id, item[0]);
                } else {
                    $(item[0]).addClass("hidden");
                    $(item[1]).addClass("hidden");
                }
            } else if (indexItem === 2) {
                if ($.isNumeric(location_id)) {
                    $(item[0]).removeClass("hidden");
                    $(item[1]).addClass("hidden");
                    showLocationChild(location_id, item[0]);
                } else {
                    $(item[0]).addClass("hidden");
                }
            } else {
                $(item[0]).removeClass("hidden");
                showLocationChild(location_id, item[0]);
            }
        });

        /**
         * @author: ntdat
         * @param: {int} location id,  item select option
         */
        function showLocationChild(location_id, target) {
            $.when(getLocationById(location_id))
                .then(function(data) {
                    var locations = data;
                    $(target).empty();
                    var html = "<option value=''>Mời chọn</option>";
                    $(target).append(html);
                    for (var index = 0; index < locations.length; index++) {
                        var item = locations[index];
                        var htmlLocation = "<option value=" + item.id + ">" + item.name + "</option>";
                        $(target).append(htmlLocation);
                    }
                });
        }

        /**
         * @author: ntdat
         * @param: {array} property
         */
        function showPropertiesByCatergory(properties) {
            var item_customDetails = $('#item_customDetails');
            item_customDetails.empty();
            for (var index = 0; index < properties.length; index++) {
                var item = properties[index];
                var htmlProperty =
                    "<div class='form-group'>" +
                    "<label class='control-label' for='item_customDetails_field_0'>" + item.name + "</label>" +
                    "<input type='text' id='item_customDetails_field_" + index + "' name='item[customDetails][" + index + "][value]' data-help='' class='form-control' required='required'>" +
                    "<input type='hidden' name='item[customDetails][" + index + "][id]' value=" + item.id + " class='form-control' required='required'>" +
                    "</div>";
                item_customDetails.append(htmlProperty);
            }
        }

        /**
         * @author: ntdat
         * @param: {int} category_id 
         * @return: list properties by category_id
         */
        function getPropertiesByCategoryId(category_id) {
            var df = jQuery.Deferred();
            var url = "/ItemController/getPropertiesByCategory";
            $.ajax({
                url: url,
                type: "post",
                async: true,
                dataType: "json",
                data: {
                    category_id: category_id
                },
                success: function(result) {
                    console.log("call server api - get properties successfully.");
                    df.resolve(result);
                },
                error: function(result) {
                    console.log("call server api - get properties error.");
                    df.resolve(result);
                }
            });
            return df.promise();
        }

        /**
         * @author: ntdat
         * @param: {int} location_id 
         * @return: list location by parent id
         */
        function getLocationById(location_id) {
            var df = jQuery.Deferred();
            var url = "/ItemController/getLocationById";
            $.ajax({
                url: url,
                type: "post",
                async: true,
                dataType: "json",
                data: {
                    location_id: location_id
                },
                success: function(result) {
                    console.log("call server api - get location successfully.");
                    df.resolve(result);
                },
                error: function(result) {
                    console.log("call server api - get location error.");
                    df.resolve(result);
                }
            });
            return df.promise();
        }

    },

    item_list: function() {
        /*var d = new Date(); 
        var day = ("0" + d.getDate()).slice(-2);
        var month = ("0" + (d.getMonth() + 1)).slice(-2);
        var hours =  add_0(d.getHours());
        var minute = add_0(d.getMinutes());
        
        $("#dateRangeStart").val( d.getFullYear()+'-'+month+'-'+day+"T"+hours+':'+minute );

    }

        $("#dateRangeEnd").val( d.getFullYear()+'-'+month+'-'+day+"T"+hours+':'+minute );*/
        $('#category').selectize({
            plugins: ['remove_button'],
            sortField: {
                field: 'text',
                direction: 'asc'
            }
        });
        $('#locations').selectize({ plugins: ['remove_button'] });

    },


    user: function() {

    },

    // function add_0(num){
    //     if(num < 10){
    //         return num = "0"+num;
    //     }else return num;
    // }

    notification: function() {

        $('.notification-header').on('click', function() {
            $.when(getNotification()).then(function(data, status) {
                if (status === 'success') {
                    if (data != null) {
                        var notifications = data;
                        showNotification(notifications);
                    }
                }
            });
        });

        function showNotification(notifications) {
            var domElement = $('#notifications .notifications');
            domElement.empty();
            for (var index = 0; index < notifications.length; index++) {
                var item = notifications[index];
                var template = '<div class="views-row">' +
                    '<div class="views-field views-field-title">Ref: <b>' + item.id + '</b></div>' +
                    '<div class="views-field views-field-body"><a href="javascript:;" hreflang="en">' + item.title + '</a></div>' +
                    '<div class="views-field views-field-field-notice-status">' + item.status + '</div>' +
                    '</div>';
                domElement.append(template);
            }
        }

        /**
         * @author: ntdat
         * @return: list notification by status 'unread'
         */
        function getNotification() {
            var df = jQuery.Deferred();
            var url = "/NotificationController/getAllByStatus";
            $.ajax({
                url: url,
                type: "get",
                async: true,
                dataType: "json",
                success: function(result, status) {
                    console.log("call server api - get notification successfully.");
                    df.resolve(result, status);
                },
                error: function(result, status) {
                    console.log("call server api - get notification error.");
                    df.resolve(result, status);
                }
            });
            return df.promise();
        }
    },

    editItem : function(){
        $('#deleteItem').on('click', function() {
            var result = confirm("Bạn chắc chắn muốn Xoá?");
            if (result) {
                var id = $(this).data('id');
                window.location = '/ItemController/deleteItem/' +id;
            }
        });
    }

}

$().ready(function() {
    app.init();
});