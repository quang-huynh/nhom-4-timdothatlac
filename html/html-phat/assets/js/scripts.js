(function($) {

    $(document).ready(function() {
        // mobileMenu();
        menutoggle();
        notificationIcon();
        closeNotification();
    });

    function mobileMenu() {
        $('.navbar-toggle').mobileMenu({
            targetWrapper: '.header .menu',
        });
    }
    
    function menutoggle() {
        $('button.navbar-toggle').on('click', function(){
            $('body').toggleClass('action-menu');
        });
    }

    function notificationIcon() {
		// $('#block-views-block-notices-block-1 .view-content').addClass('hidden');
		$('.notification-header').on('click', function(){
			$('body').toggleClass('notification');
		});
    }
    
    function closeNotification() {
        $('.close-notification').on('click', function(){
          $('body').removeClass('notification')
        });
      }

})(jQuery);
