(function ($) {

	$(document).ready(function () {
		// mobileMenu();
		menutoggle();
		notificationIcon();
		closeNotification();
	});

	window.onload = function () {
		lineDraw();
		//repeat for demo purposes
		setInterval(lineDraw, 5000);

		var options = {
			title: {
				text: "Vật Thất Lạc",
				fontColor: "#92b558",
				fontFamily: "Open Sans",
				fontWeight: "600",
			},
			data: [{
				type: "pie",
				startAngle: 45,
				showInLegend: "true",
				legendText: "{label}",
				indexLabel: "{label} ({y})",
				yValueFormatString: "#,##0.#" % "",
				dataPoints: [{
						label: "Báo Mất",
						y: 36,
						color: "#F08080"
					},
					{
						label: "Báo Thấy",
						y: 31,
						color: "#39cccc"
					},
					{
						label: "Trùng Khớp",
						y: 7,
						color: "#00c0ef"
					},
					{
						label: "Đã Xử Lý",
						y: 7,
						color: "#00a65a"
					}
				]
			}]
		};

		var optionslost = {
			animationEnabled: true,
			theme: "light2",
			title: {
				text: "Báo Mất",
				fontColor: "#92b558",
				fontFamily: "Open Sans",
				fontWeight: "600",
			},
			axisX: {
				valueFormatString: "DD MMM"
			},
			axisY: {
				minimum: -10
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				verticalAlign: "bottom",
				horizontalAlign: "left",
				dockInsidePlotArea: true,
				itemclick: toogleDataSeries
			},
			data: [{
				type: "line",
				showInLegend: true,
				name: "Mất",
				lineDashType: "dash",
				xValueFormatString: "DD MMM, YYYY",
				color: "#F08080",
				dataPoints: [{
						x: new Date(2017, 10, 1),
						y: 20
					},
					{
						x: new Date(2017, 10, 2),
						y: 5
					},
					{
						x: new Date(2017, 10, 3),
						y: 2
					},
					{
						x: new Date(2017, 10, 4),
						y: 5
					},
					{
						x: new Date(2017, 10, 5),
						y: 8
					},
					{
						x: new Date(2017, 10, 6),
						y: 16
					},
					{
						x: new Date(2017, 10, 7),
						y: 3
					},
					{
						x: new Date(2017, 10, 8),
						y: 4
					},
					{
						x: new Date(2017, 10, 9),
						y: 6
					},
					{
						x: new Date(2017, 10, 10),
						y: 6
					},
					{
						x: new Date(2017, 10, 11),
						y: 7
					},
					{
						x: new Date(2017, 10, 12),
						y: 10
					},
					{
						x: new Date(2017, 10, 13),
						y: 8
					},
					{
						x: new Date(2017, 10, 14),
						y: 9
					},
					{
						x: new Date(2017, 10, 15),
						y: 9
					}
				]
			}]
		};

		var optionsfound = {
			animationEnabled: true,
			theme: "light2",
			title: {
				text: "Báo Thấy",
				fontColor: "#92b558",
				fontFamily: "Open Sans",
				fontWeight: "600",
			},
			axisX: {
				valueFormatString: "DD MMM"
			},
			axisY: {
				minimum: -10
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				verticalAlign: "bottom",
				horizontalAlign: "left",
				dockInsidePlotArea: true,
				itemclick: toogleDataSeries
			},
			data: [{
				type: "line",
				showInLegend: true,
				name: "Thấy",
				lineDashType: "dash",
				xValueFormatString: "DD MMM, YYYY",
				color: "#39cccc",
				yValueFormatString: "#,##0K",
				dataPoints: [{
						x: new Date(2017, 10, 1),
						y: 1
					},
					{
						x: new Date(2017, 10, 2),
						y: 2
					},
					{
						x: new Date(2017, 10, 3),
						y: 3
					},
					{
						x: new Date(2017, 10, 4),
						y: 5
					},
					{
						x: new Date(2017, 10, 5),
						y: 9
					},
					{
						x: new Date(2017, 10, 6),
						y: 12
					},
					{
						x: new Date(2017, 10, 7),
						y: 2
					},
					{
						x: new Date(2017, 10, 8),
						y: 1
					},
					{
						x: new Date(2017, 10, 9),
						y: 3
					},
					{
						x: new Date(2017, 10, 10),
						y: 6
					},
					{
						x: new Date(2017, 10, 11),
						y: 3
					},
					{
						x: new Date(2017, 10, 12),
						y: 20
					},
					{
						x: new Date(2017, 10, 13),
						y: 23
					},
					{
						x: new Date(2017, 10, 14),
						y: 2
					},
					{
						x: new Date(2017, 10, 15),
						y: 4
					}
				]
			}]
		};

		var optionsmatches = {
			animationEnabled: true,
			theme: "light2",
			title: {
				text: "Vật Trùng Khớp",
				fontColor: "#92b558",
				fontFamily: "Open Sans",
				fontWeight: "600",
			},
			axisX: {
				valueFormatString: "DD MMM"
			},
			axisY: {
				minimum: -10
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				verticalAlign: "bottom",
				horizontalAlign: "left",
				dockInsidePlotArea: true,
				itemclick: toogleDataSeries
			},
			data: [{
				type: "line",
				showInLegend: true,
				name: "Trùng Khớp",
				lineDashType: "dash",
				color: "#00c0ef",
				dataPoints: [{
						x: new Date(2017, 10, 1),
						y: 10
					},
					{
						x: new Date(2017, 10, 2),
						y: 20
					},
					{
						x: new Date(2017, 10, 3),
						y: 4
					},
					{
						x: new Date(2017, 10, 4),
						y: 2
					},
					{
						x: new Date(2017, 10, 5),
						y: 9
					},
					{
						x: new Date(2017, 10, 6),
						y: 9
					},
					{
						x: new Date(2017, 10, 7),
						y: 9
					},
					{
						x: new Date(2017, 10, 8),
						y: 9
					},
					{
						x: new Date(2017, 10, 9),
						y: 15
					},
					{
						x: new Date(2017, 10, 10),
						y: 14
					},
					{
						x: new Date(2017, 10, 11),
						y: 10
					},
					{
						x: new Date(2017, 10, 12),
						y: 15
					},
					{
						x: new Date(2017, 10, 13),
						y: 15
					},
					{
						x: new Date(2017, 10, 14),
						y: 15
					},
					{
						x: new Date(2017, 10, 15),
						y: 15
					}
				]
			}]
		};

		var optionsdiposal = {
			animationEnabled: true,
			theme: "light2",
			title: {
				text: "Đã Xử Lý",
				fontColor: "#92b558",
				fontFamily: "Open Sans",
				fontWeight: "600",
			},
			axisX: {
				valueFormatString: "DD MMM"
			},
			axisY: {
				minimum: -10
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				verticalAlign: "bottom",
				horizontalAlign: "left",
				dockInsidePlotArea: true,
				itemclick: toogleDataSeries
			},
			data: [{
				type: "line",
				showInLegend: true,
				name: "Đã trả",
				lineDashType: "dash",
				color: "#00a65a",
				dataPoints: [{
						x: new Date(2017, 10, 1),
						y: 2
					},
					{
						x: new Date(2017, 10, 2),
						y: 0
					},
					{
						x: new Date(2017, 10, 3),
						y: 0
					},
					{
						x: new Date(2017, 10, 4),
						y: 5
					},
					{
						x: new Date(2017, 10, 5),
						y: 0
					},
					{
						x: new Date(2017, 10, 6),
						y: 3
					},
					{
						x: new Date(2017, 10, 7),
						y: 0
					},
					{
						x: new Date(2017, 10, 8),
						y: 4
					},
					{
						x: new Date(2017, 10, 9),
						y: 1
					},
					{
						x: new Date(2017, 10, 10),
						y: 10
					},
					{
						x: new Date(2017, 10, 11),
						y: 10
					},
					{
						x: new Date(2017, 10, 12),
						y: 5
					},
					{
						x: new Date(2017, 10, 13),
						y: 5
					},
					{
						x: new Date(2017, 10, 14),
						y: 5
					},
					{
						x: new Date(2017, 10, 15),
						y: 5
					}
				]
			}]
		};

		$("#chartContainerAll").CanvasJSChart(options);
		$("#chartContainerLost").CanvasJSChart(optionslost);
		$("#chartContainerFound").CanvasJSChart(optionsfound);
		$("#chartContainerMatches").CanvasJSChart(optionsmatches);
		$("#chartContainerDisposal").CanvasJSChart(optionsdiposal);

		function toogleDataSeries(e) {
			if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			} else {
				e.dataSeries.visible = true;
			}
			e.chart.render();
		}

	}





	function mobileMenu() {
		$('.navbar-toggle').mobileMenu({
			targetWrapper: '.header .menu',
		});
	};

	function menutoggle() {
		$('button.navbar-toggle').on('click', function () {
			$('body').toggleClass('action-menu');
		});
	};

	function notificationIcon() {
		// $('#block-views-block-notices-block-1 .view-content').addClass('hidden');
		$('.notification-header').on('click', function () {
			$('body').toggleClass('notification');
		});
	};

	function closeNotification() {
		$('.close-notification').on('click', function () {
			$('body').removeClass('notification')
		});
	};

	function lineDraw() {
		var path = document.querySelector('.line-animated path');
		var length = path.getTotalLength();
		// Clear any previous transition
		path.style.transition = path.style.WebkitTransition =
			'none';

		// Set up the starting positions
		path.style.strokeDasharray = length + ' ' + length;
		path.style.strokeDashoffset = length;
		// Trigger a layout so styles are calculated & the browser 
		// picks up the starting position before animating
		path.getBoundingClientRect();
		// Define our transition
		path.style.transition = path.style.WebkitTransition =
			'stroke-dashoffset 7s ease-in-out';
		// Go!
		path.style.strokeDashoffset = '0';
		//0 is the image fully animated, 988.01 is the starting point.
	};





})(jQuery);
