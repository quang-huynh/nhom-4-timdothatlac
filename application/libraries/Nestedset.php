<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Nestedset {
    var $CI;
    
    public function __construct(){
        $this->CI =& get_instance();
    }

    function showOption($data, $selected = 0, $parent_id = 0, $char = '')
    {
        foreach ($data as $key => $item)
        {
            // Nếu là chuyên mục con thì hiển thị
            if ($item->parent_id == $parent_id)
            {
                if( $selected == $item->id ) {
                    echo '<option value="'.$item->id.'" selected>';
                } else {
                    echo '<option value="'.$item->id.'">';
                }                
                    echo $char . $item->name;
                echo '</option>';
                 
                // Xóa chuyên mục đã lặp
                unset($data[$key]);
                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                $this->showOption($data, $selected, $item->id, $char.'---');
            }
        }
    }
} 