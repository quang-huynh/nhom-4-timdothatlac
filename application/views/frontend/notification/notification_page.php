<section id="main" class="main">
    <div class="container">
        <div class="block block-header-page">
            <h1 class="page-title">Trang Thông báo chung</h1>
            <nav class="breadcrumb">
                <ol>
                <li>
                    <a href="<?=base_url()?>">Trang Chủ</a>
                </li>
                <li>
                    <a href="#">Trang Thông báo chung</a>
                </li>
                </ol>
            </nav>
        </div>
        <div class="block block-filter">
            <div class="row">
            <div class="col-sm-12">
                <div class="box box-default filters">
                <form role="form" method="get" action="<?php echo base_url( 'thong-bao-tim-theo-ngay' ) ?>" class="form">
                    <div class="box-header">
                        <h3 class="box-title">
                            <i class="fa fa-fw fa-filter"></i>
                            Tìm Kiếm
                        </h3>
                        <div class="box-tools">
                            <button type="submit" id="submit" name="submit" class="btn btn-sm btn-default"><i class="fa fa-filter"></i> Kiếm</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label class="control-label" for="dateRangeStart">Khoảng thời gian</label>
                            <div class="input-daterange input-group">
                                <input type="datetime-local" id="dateRangeStart" name="dateRangeStart" class="form-control">
                                <span class="input-group-addon">to</span>
                                <input type="datetime-local" id="dateRangeEnd" name="dateRangeEnd" class="form-control">
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
        <div class="block block-list-box box-padding">
            <div class="items">
                <?php if(isset($notifications) && $notifications) {
                    foreach($notifications as $item) {
                        settype($item->content,'int');
                        $uri = empty($item->content)? '#':base_url('chi-tiet/'.$item->content);
                        ?>
                        <a class="item box box-default" href="<?=$uri?>">
                            <div class="box-body">
                                <div class="row">
                                <div class="col-sm-2 col-md-4 col-lg-2">                                    
                                    <span class="small text-muted">Mã </span><?= $item->id?>
                                    <div class="image-wrapper">
                                        <span class="primary-image">
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-8 col-lg-6 item-details">
                                    <h4>
                                        <?= $item->title ?>
                                    </h4>
                                    <p><?= $item->short_description ?></p>
                                </div>
                                <div class="col-sm-4 col-md-12 col-lg-4">
                                    <ul class="list-unstyled hidden-xs">
                                        <li>Status: <span class="item-status bg-default"><?=@ array_search( $item->status ,unserialize(NOTIFICATION_STATUS));   ?> </span></li>
                                        <li>
                                            Từ <span class="item-status bg-default bg-<?=@array_search( $item->type, unserialize(NOTIFICATION_TYPE) ) ?>"><?=@array_search( $item->type, unserialize(NOTIFICATION_TYPE) ) ?></span>
                                            lúc <?= $item->created_at; ?>
                                        </li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                        </a>
                <?php } }?>
                <!-- <a class="item box box-default" href="#">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 col-md-4 col-lg-2">
                                <p>
                                <span class="small text-muted">Mã</span>
                                85
                                </p>
                                <div class="image-wrapper">
                                <span class="primary-image">
                                </span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-8 col-lg-6 item-details">
                                <h4>
                                Camera
                                </h4>
                                <p>Đã tìm thấy vật trùng khớp</p>
                            </div>
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <ul class="list-unstyled hidden-xs">
                                    <li>Status: <span class="item-status bg-default">Mới</span> <span class="item-status bg-default">Trùng Khớp</span></li>
                                    <li>
                                        Từ <span class="item-status bg-default bg-admin">Quản trị viên</span>
                                        lúc 19/11/2017, 4:02pm
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="item box box-default" href="#">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 col-md-4 col-lg-2">
                                <p>
                                <span class="small text-muted">Mã</span>
                                85
                                </p>
                                <div class="image-wrapper">
                                <span class="primary-image">
                                </span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-8 col-lg-6 item-details">
                                <h4>
                                Camera
                                </h4>
                                <p>Đã tìm thấy vật trùng khớp</p>
                            </div>
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <ul class="list-unstyled hidden-xs">
                                    <li>Status: <span class="item-status bg-default">Đã xử lý</span></li>
                                    <li>
                                        Từ <span class="item-status bg-default bg-admin">Quản trị viên</span>
                                        lúc 19/11/2017, 4:02pm
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </a>       -->
            </div>
            <nav class="pager" role="navigation" aria-labelledby="pagination-heading">
                <!-- <ul class="pager__items js-pager__items">
                    <li class="pager__item pager__item--previous">
                        <a href="?page=1" title="Go to next page" rel="next">
                        <span class="visually-hidden">Prev page</span>
                        <span aria-hidden="true">››</span>
                        </a>
                    </li>
                    <li class="pager__item is-active">
                        <a href="?page=0" title="Current page">
                        <span class="visually-hidden">
                        Current page
                        </span>1</a>
                    </li>
                    <li class="pager__item pager__item--next">
                        <a href="?page=1" title="Go to next page" rel="next">
                        <span class="visually-hidden">Next page</span>
                        <span aria-hidden="true">››</span>
                        </a>
                    </li>
                </ul> -->
                    <!-- Show pagination links -->
                <?php if(isset($links))
                    foreach ($links as $link) {
                        echo $link;
                } ?>
            </nav>
        </div>
    </div>
</section>