<!DOCTYPE html>
<html lang="en">
   <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Tìm Kiếm Vật Thất Lạc | <?php echo $title;?></title>
        <!-- Style -->
        <link rel="icon" href="<?php echo base_url(); ?>public/favicon.png" sizes="16x16 32x32" type="image/png">
        <link href="<?php echo base_url(); ?>public/assets/css/selectize.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/assets/css/style.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/assets/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <meta name="_token" content="<?= $this->security->get_csrf_hash(); ?>" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="front user-logged-in">
        <div id="page" class="page">
            <?php $dt['login'] = $this->session->userdata('login'); ?>
            <?php $this->load->view('frontend/partials/navigation_menu',$dt); ?>
            <?php $this->load->view('frontend/partials/popup_notification', $dt); ?>
            <?php $this->load->view('frontend/partials/header', $dt); ?>

            <?php $this->load->view($content); ?>

        </div>
        
        <!-- Javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>public/assets/includes/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>/public/assets/includes/bootstrap/js/bootstrap.min.js"></script>
         <script src="<?php echo base_url(); ?>public/assets/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/js/mobile_menu.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/js/selectize.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/js/mobile_menu.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/includes/js/custom-file-input.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/includes/js/jquery.canvasjs.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/js/scripts.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
    </body>
</html>