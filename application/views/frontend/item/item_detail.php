<section id="main" class="main">
	<div class="container">
		<div class="block block-header-page">
			<h1 class="page-title">Mã <?=@$itemID?></h1>
			<nav class="breadcrumb">
				<ol>
					<li>
						<a href="index.html">Trang Chủ</a>
					</li>
					<li>
						<a href="#">Trang Báo Tìm Thấy</a>
					</li>
				</ol>
			</nav>
		</div>
		<div class="box box-default">
			<div class="box-body">
				<p class="status-bar lead">
					Mã&nbsp;
					<span class="item-reference pad">
						<b><?=@$itemID?></b>
					</span> &nbsp; Vật:&nbsp;
					<span class="item-type bg-default pad nowrap"><?= ( !empty($detail) && $detail[0]->type == 1 ) ? "Mất" : "Thấy"  ?></span> &nbsp;&nbsp; Trạng Thái:&nbsp;
					<span class="item-type bg-default"><?= @$itemStatus ?></span> &nbsp;&nbsp; <?= ( !empty($detail) && $detail[0]->type == 1 ) ? "Mất" : "Thấy"  ?>&nbsp;tại:&nbsp;
					<span class="item-type bg-default"><?= !empty($location) ? $location[0]['name'] : "" ?></span>
				</p>
			</div>
		</div>
		<div class="block block-list-details">
			<div class="row">
				<div class="col-md-12 col-print-sm-12">
					<div class="box box-default">
						<div class="box-header">
							<h3 class="box-title">
								<i class="fa fa-list-alt"></i>&nbsp; Chi tiết</h3>
							<div class="box-tools pull-right">
								<a href="<?=base_url('chi-tiet/edit-lost-item-details-'.@$itemID .'.html')?>" class="btn btn-sm btn-default">
									<i class="fa fa-edit"></i>&nbsp; Cập nhật</a>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-sm-4 col-md-4 images-wrapper">
									<div class="primary-image">
										<?php if( !empty($image) ) { ?>
										<img class="img-thumbnail img-fluid" src="<?=base_url($image[0]->path)?>" width="480" height="360" alt="">
										<?php } else { ?>
										<!-- <img class="img-thumbnail img-fluid" src="<?=base_url('public\assets\images\demo\\5iPhone_8_1.jpg')?>" width="480" height="360" alt=""> -->
										<?php } ?>										
									</div>
									<div class="gallery-images">
										<div class="row small-gutter">
										</div>
									</div>
								</div>
								<div class="col-sm-8 col-md-8 item-wrapper">
									<div class="item">
										<h4>
											<?= ( !empty($category) ) ? $category[0]['name'] : ""; ?>
										</h4>
										<div class="table-responsive">
											<table class="table table-condensed">
												<colgroup>
													<col width="30%">
													<col>
												</colgroup>
												<tbody>
													<tr>
														<th>Serial Number</th>
														<td><?= @$detail[0]->tag_number; ?></td>
													</tr>
													<?php foreach ($property as $value) { ?>
														<tr>
															<th><?= $value['name'] ?></th>
															<td><?= $value['value'] ?></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box box-default owner-details">
						<div class="box-header">
							<h3 class="box-title">
								<i class="fa fa-user"></i>&nbsp; <?= ( !empty($detail) && $detail[0]->type == 1 ) ? "Chủ sở hữu" : ( !empty($detail) && $detail[0]->type == 2 ) ? "Người tìm thấy" : "Không phân biệt"; ?></h3>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<table class="table">
									<colgroup>
										<col width="1">
										<col>
									</colgroup>
									<tbody>
										<tr>
											<th class="nobr">Họ tên</th>
											<td><?= $detail[0]->fullname ?></td>
										</tr>
										<tr>
											<th class="nobr">Email</th>
											<td><?= $detail[0]->email ?></td>
										</tr>
										<tr>
											<th class="nobr">Số điện thoại</th>
											<td><?= $detail[0]->phone_number ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="box box-default notes">
						<div class="box-header">
							<h3 class="box-title">
								<i class="fa fa-file-text-o"></i>&nbsp; Mô tả</h3>
						</div>
						<div class="box-body">
							<?= $detail[0]->detail ?>
							<hr>
						</div>
					</div>
					<div class="box box-default matches">
						<div class="box-header">
							<h3 class="box-title">
								<?php if( $detail[0]->status == 3 ){ ?>
									<i class="fa fa-check"></i>&nbsp; Matched
								<?php }else{ ?> 
									<i class="fa fa-search"></i>&nbsp; Possible Matches
								<?php } ?>
							</h3>
							<div class="box-tools pull-right">
								<a href="<?=base_url('chi-tiet/bo-loc/'.$itemID) ?>" class="btn btn-sm btn-default">
									<i class="fa fa-list-ul"></i>&nbsp; Find More</a>
								<button type="button" class="btn btn-sm btn-default refresh-matches" onclick="refresh()">
									<i class="fa fa-refresh"></i>&nbsp; Làm mới</button>
							</div>
						</div>
						<?php if( !empty( $matches ) ) { ?>
						<div class="box-body">
							<p class="text-center loader hide">
								<i class="fa fa-refresh fa-lg fa-spin"></i>&nbsp;&nbsp; Searching for matches...</p>
							<div id="matches" class="matches-list">
							<?php foreach ($matches as $key => $value) { ?>
								<a class="item" href="<?= base_url('chi-tiet/'. $itemID . '/so-sanh/' . $value->id ) ?>">
									<div class="row">
										<div class="col-xs-2">
											<p>
												<span class="small text-muted">Mã</span>
												<?= $value->id; ?>
											</p>
										</div>
										<div class="col-xs-7">
											<h4><?= $value->category_name; ?></h4>
											<div class="table-responsive">
												<table class="table table-condensed">
													<colgroup>
														<col width="30%">
														<col>
													</colgroup>
													<tbody>
														<tr>
															<th>Serial Number</th>
															<td><?= @$value->tag_number ?></td>
														</tr>
														<?php 
															$property = explode('::', $value->property_name );
															foreach ($property as $property_item) {
																$item = explode('||', $property_item ); ?>
																<tr>
																	<th><?=@$item[0]?></th>
																	<td><?=@$item[1]?></td>
																</tr>
															<?php }
														?>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-xs-3">
											<ul class="list-unstyled">
												<li>Phần trăm :
													<span class="item-status bg-default"><?= @$value->percent ?>%</span>
												</li>
												<li>Status:
													<?= @$itemStatus ?>
												</li>
												<li>
													<span class="item-type bg-default"><?= ( $value->type == 1 ) ? "Mất" : "Thấy" ?></span>
													lúc <?= date('d/m/Y H:i:s', strtotime($value->created_at)) ?>
												</li>
												<li><?= ( $value->type == 1 ) ? "Mất" : "Thấy" ?> tại
													<span class="item-location bg-default"><?= $value->location_name ?></span>
												</li>
											</ul>
										</div>
									</div>
								</a>
							<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	function refresh() {
		$.ajax({
			url : "<?= base_url('itemcontroller/itemMatches') ?>",
            type : "post",
            dataType:"json",
            data : { id : <?= $itemID; ?> },
            success : function (data){
                $('#matches').html(data);
            }
		});
	}
</script>