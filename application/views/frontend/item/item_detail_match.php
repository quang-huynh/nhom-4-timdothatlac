<section id="main" class="main">
			<div class="container">
				<div class="block block-header-page">
					<h1 class="page-title">Trang So sánh trùng khớp</h1>
					<nav class="breadcrumb">
						<ol>
							<li>
								<a href="index.html">Trang Chủ</a>
							</li>
							<li>
								<a href="#">Trang So sánh trùng khớp</a>
							</li>
						</ol>
					</nav>
				</div>
				<div class="block block-form-lost">
					<div class="row matching-items">
						<div class="col-sm-6">
							<div class="box box-default">
								<div class="box-body">
									<p class="status-bar text-lg">
										Mã:&nbsp;
										<span class="item-reference pad">
											<b><?=@$itemID?></b>
										</span>
										&nbsp; Vật:&nbsp;
										<span class="item-type bg-default pad-sm nowrap"><?= ( !empty($detail) && $detail[0]->type == 1 ) ? "Mất" : "Thấy"  ?></span>
										&nbsp;&nbsp; Trạng thái:&nbsp;
										<span class="item-status bg-default pad-sm nowrap"><?= @$itemStatus ?></span>
										&nbsp;&nbsp; <?= ( !empty($detail) && $detail[0]->type == 1 ) ? "Mất" : "Thấy"  ?>&nbsp;tại:&nbsp;
										<span class="item-location bg-default pad-sm nowrap"><?= !empty($location) ? $location[0]['name'] : "" ?></span>
									</p>
								</div>
							</div>

							<div class="box box-default">
								<div class="box-header">
									<h3 class="box-title">
										<i class="fa fa-list-alt"></i>&nbsp; Chi tiết</h3>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-sm-4 col-md-4 images-wrapper">
											<div class="primary-image">
											<?php if( !empty($image) ) { ?>
										<img class="img-thumbnail img-fluid" src="<?=base_url($image[0]->path)?>" width="480" height="360" alt="">
										<?php } else { ?>
										<img class="img-thumbnail img-fluid" src="<?=base_url('public\assets\images\demo\\5iPhone_8_1.jpg')?>" width="480" height="360" alt="">
										<?php } ?>										
											</div>
										</div>
										<div class="col-sm-8 col-md-8 item-wrapper">
											<div class="item">
												<h4>
												<?= ( !empty($category) ) ? $category[0]['name'] : ""; ?>
												</h4>
												<div class="table-responsive">
													<table class="table table-condensed">
														<colgroup>
															<col width="30%">
															<col>
														</colgroup>
														<tbody>
															<tr>
																<th>Serial Number</th>
																<td><?= @$detail[0]->tag_number; ?></td>
															</tr>
															<?php foreach ($property as $value) { ?>
																<tr>
																	<th><?= $value['name'] ?></th>
																	<td><?= $value['value'] ?></td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
							<hr class="visible-xs">
						</div>
						<div class="col-sm-6">
							<div class="box box-default">
								<div class="box-body">
									<p class="status-bar text-lg">
										Mã:&nbsp;
										<span class="item-reference pad">
											<b><?=@$itemIDMatched?></b>
										</span>
										&nbsp; Vật:&nbsp;
										<span class="item-type bg-default pad-sm nowrap"><?= ( !empty($detailMatched) && $detailMatched[0]->type == 1 ) ? "Mất" : "Thấy"  ?></span>
										&nbsp;&nbsp; Trạng thái:&nbsp;
										<span class="item-status bg-default pad-sm nowrap"><?= @$itemStatusMatched ?></span>
										&nbsp;&nbsp; <?= ( !empty($detailMatched) && $detailMatched[0]->type == 1 ) ? "Mất" : "Thấy"  ?>&nbsp;tại:&nbsp;
										<span class="item-location bg-default pad-sm nowrap"><?= !empty($locationMatched) ? $locationMatched[0]['name'] : "" ?></span>
									</p>
								</div>
							</div>

							<div class="box box-default">
								<div class="box-header">
									<h3 class="box-title">
										<i class="fa fa-list-alt"></i>&nbsp; Chi tiết</h3>
								</div>
								<div class="box-body">

									<div class="row">
										<div class="col-sm-4 col-md-4 images-wrapper">
											<div class="primary-image">
											<?php if( !empty($image) ) { ?>
											<img class="img-thumbnail img-fluid" src="<?=base_url($image[0]->path)?>" width="480" height="360" alt="">
											<?php } else { ?>
											<!-- <img class="img-thumbnail img-fluid" src="<?=base_url('public\assets\images\demo\\5iPhone_8_1.jpg')?>" width="480" height="360" alt=""> -->
											<?php } ?>	
											</div>
										</div>

										<div class="col-sm-8 col-md-8 item-wrapper">
											<div class="item">
												<h4>
												<?= ( !empty($categoryMatched) ) ? $categoryMatched[0]['name'] : ""; ?>
												</h4>
												<div class="table-responsive">
													<table class="table table-condensed">
														<colgroup>
															<col width="30%">
															<col>
														</colgroup>
														<tbody>
														<tr>
															<th>Serial Number</th>
															<td><?= @$detailMatched[0]->tag_number; ?></td>
														</tr>
														<?php foreach ($propertyMatched as $value) { ?>
															<tr>
																<th><?= $value['name'] ?></th>
																<td><?= $value['value'] ?></td>
															</tr>
														<?php } ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr class="visible-xs">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-8 col-lg-push-2">
							<div class="box box-default">
								<div style="text-align: center;" class="box-header">
									<h3 class="box-title">So sánh khớp <span class="item-type bg-default pad-sm nowrap"><?=@$percent?>%</span> <?= ($itemStatus == 'matched' ) ? 'Item này đã được matched':'Xác nhận trùng khớp?' ?>  </h3>
								</div>
								<div class="box-body">
								  <?php if( $itemStatus != 'matched' ) { ?> 
										<p>Vui lòng kiểm tra các thông tin ở trên và xác nhận rằng chúng là các mặt hàng đã giống nhau.</p>
										<form role="form" method="post" action="<?=base_url("chi-tiet/$itemID/so-sanh/$itemIDMatched")?>" class="form">
										<hr>
										<div class="row">
											<div class="col-md-5">
												<h5 class="text-bold">Người bị mất vật</h5>
												<?php $userLost = null;
												if( $detail[0]->type == unserialize(TYPE_ITEM)['lost'] ){
													$userLost = $detail[0];
												}elseif( $detailMatched[0]->type == unserialize(TYPE_ITEM)['lost'] ){
													$userLost = $detailMatched[0];
												} ?>
												<p>
													Name: <?= @$userLost->fullname ?>
													<br> Email: <?= @$userLost->email ?>
													<br> <?= @$userLost->phone_number ?> </p>
												<hr class="visible-xs visible-sm">
											</div>
											<div class="col-md-7">
												<div class="form-group">
													<div class="checkbox">
														<label class="" for="match_items_sendOwnerEmail">
															<input type="checkbox" id="match_items_sendOwnerEmail" name="match_items[sendOwnerEmail]" class="" value="1" checked="checked"> Gửi thông báo cho chủ sở hữu, xác nhận bạn đã tìm thấy vật của họ?
														</label>
													</div>
												</div>
												<div class="form-group ">
													<label class="control-label" for="match_items_ownerMessage">Gửi tin nhắn cho user bị mất</label>
													<textarea id="match_items_ownerMessage" name="match_items[ownerMessage]" class="form-control form-control"></textarea>
												</div>
												<p class="text-muted">
													Thông báo sẽ được gửi đến các user đăng tin
												</p>
											</div>
										</div>
										<hr>
										<div class="text-center">
											<a class="btn btn-default" onclick="goBackWWSGO()" href="#">
												<i class="fa fa-chevron-left"></i>&nbsp; Trở về</a>
											<button type="submit" id="match_items_confirmMatch" name="match_items[confirmMatch]" class="btn btn-success">Đồng ý &nbsp;
												<i class="fa fa-check"></i>
											</button>
										</div>
										<input type="hidden" id="match_items_item1_id" name="match_items[item1_id]" class="form-control" value="<?= @$itemID ?>">
										<input type="hidden" id="match_items_item2_id" name="match_items[item2_id]" class="form-control" value="<?= @$itemIDMatched ?>">
										<input type="hidden" name="match_items[percent]" value="<?=@$percent?>" />
										<!-- <input type="hidden" id="match_items__token" name="match_items[_token]" class="form-control" value="6A768ef3c6x5an0my9_SuZDij-MT9At0tnBhjzdEWws"> -->
									</form>
									<?php } ?>									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<script>
function goBackWWSGO() {
    window.history.back();
		return false;
}
</script>