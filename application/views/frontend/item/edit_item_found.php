<section id="item-create-found" class="main item-create-found">
   <div class="container">
      <div class="block block-header-page">
         <h1 class="page-title">Chỉnh sửa chi tiết vật báo tìm thấy</h1>
         <nav class="breadcrumb">
            <ol>
               <li>
                  <a href="<?=base_url()?>">Trang Chủ</a>
               </li>
               <li>
                  <a href="#">Trang Chỉnh sửa chi tiết vật báo tìm thấy</a>
               </li>
            </ol>
         </nav>
      </div>
      <div class="block block-form-lost">
        <div class="row">
            <div class="col-md-10 col-md-push-1 col-lg-6 col-lg-push-3">
                <?php
                    if(!empty($notify)) {
                        if($notify['status'] === SUCCESS) {?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <strong><?= $notify['status'] ?> ! </strong><?= $notify['message'] ?>
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <strong><?= $notify['status'] ?> ! </strong><?= $notify['message'] ?>
                            </div>
                    <?php }
                    } 
                ?>
                <div class="box box-default">
                    <div class="box-body">
                        <?= form_open_multipart(null, array(
                            'method' => 'post',
                            'id' => 'itemForm',
                            'class' => 'form'
                        )); ?>
                        <fieldset>
                            <legend>Chi Tiết Vật <button id="deleteItem" type="button" class="btn btn-success btn-sm pull-right" data-id="<?= $detail[0]->id; ?>">Xoá</button></legend>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label required" for="item_customCategory">Vật Dụng</label>
                                        <select id="item_customCategory" name="item[customCategory]" required="required" data-help="" class="form-control form-control selectpicker form-control" tabindex="-98">
                                            <option value="" selected="selected">Mời chọn...</option>
                                            <?php foreach ($categories as $category) { ?>
	                                          <option value="<?=$category->id?>"<?= ( $category_detail[0]['id'] == $category->id ) ? " selected" : "" ?>><?=$category->name?></option>
	                                       <?php } ?>
                                        </select>
                                    </div>
                                    <div id="item_customDetails">
                                    	<?php foreach ($property_detail as $key => $property) { ?>
                             		  	<div class='form-group'>
                                 		  	<label class='control-label' for='item_customDetails_field_<?=$key?>'><?=$property['name']?></label>
                                 		  	<input type='text' id='item_customDetails_field_<?=$key?>' name='item[customDetails][<?=$key?>][value]' data-help='' class='form-control' required='required' value="<?=$property['value']?>" />
                                 		  	<input type='hidden' name='item[customDetails][<?=$key?>][id]' class='form-control' required='required' value="<?=$property['property_id']?>" />
                             		  	</div>
                                 	<?php } ?>                                     	
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label required" for="item_location">Khu vực</label>
                                        <div id="#item_location">
                                            <select name="item[location]" data-help="Where was the item lost?" class="form-control form-control selectpicker form-control item-location-1" tabindex="-98">
                                                <option value="" selected="selected">Mời chọn...</option>
                                                <?php $this->nestedset->showOption($locations, $location_detail[0]['id']); ?> 
                                            </select>
                                        </div>

                                        <span class="help-block small">Nơi bạn làm mất đồ vật?</span>
                                    </div>
                                    <div class="form-group "><label class="control-label" for="item_description">Mô tả chi tiết khác</label><textarea id="item_description" name="item[description]" data-help="Any other details, marks, or descriptive information." class="form-control form-control"><?= $detail[0]->detail; ?></textarea><span class="help-block small">Cung cấp thêm bất kỳ thông tin để hỗ trợ tốt hơn cho hệ thống.</span></div>
                                    <div class="form-group ">
                                        <label class="control-label required" for="item_reportedAt">Tìm thấy lúc</label>
                                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="item_reportedAt" name="item[reportedAt]" required="required" autocomplete="off" data-help="<i class=&quot;fa fa-clock-o&quot;></i> America / New York" data-addon-left="<i class=&quot;fa fa-calendar&quot;></i>" class="form-control" value="<?= date('m/d/Y H:i:s', strtotime($detail[0]->detail)) ?>"></div>
                                        <span class="help-block small"><i class="fa fa-clock-o"></i> Giờ Việt Nam</span>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                           <legend>Kho Lưu Trữ</legend>
                           <div class="row">
                             <div class="col-sm-12">
                               <div class="form-group ">
                                  <label class="control-label required" for="item_location">Khu Vực Lưu Trữ</label>
                                  <select id="item_location" name="item[storage_location_id]" data-help="Where was the item lost?" class="form-control form-control selectpicker form-control" tabindex="-98">
                                        <?php
                                            if(isset($storageLocations))
                                                foreach ($storageLocations as $item) { ?>
                                                <option value="<?= $item->id; ?>">
                                                    <?= $item->name; ?>
                                                </option>
                                        <?php } ?>
                                   </select>
                                  <span class="help-block small">
                                   Nơi sẽ được lưu trữ ở đâu? Để trống nếu không chắc chắn về vị trí lưu trữ, điều này có thể được thay đổi sau.</span>
                               </div>
                            </div>
                           </div>
                        </fieldset>
                        <hr>
                        <label class="control-label">Image Upload</label>
                        <div class="dropzone dz-clickable box-file">
                                <input type="file" name='imageItem' id="file-2" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                        </div>
                         <!-- <input type='file' id='image-item' name='imageItem' data-help='' class='form-control'> -->
                         <hr>
                        <div class="text-center">
                           <button type="submit" id="item_save" name="item[save]" class="btn btn-success"><i class="fa fa-check"></i>&nbsp; Lưu</button>
                           <!-- <button type="submit" id="item_saveAndAdd" name="item[saveAndAdd]" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp; Save &amp; Add Another</button> -->
                        </div>
                     <?= form_close();?>
                    </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>