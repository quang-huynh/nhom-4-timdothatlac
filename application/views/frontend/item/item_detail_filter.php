<section id="main" class="main">
			<div class="container">
				<div class="block block-header-page">
					<h1 class="page-title">Mã 85</h1>
					<nav class="breadcrumb">
						<ol>
							<li>
								<a href="index.html">Trang Chủ</a>
							</li>
							<li>
								<a href="#">Trang Báo Tìm Thấy</a>
							</li>
						</ol>
					</nav>
				</div>
				<div class="box box-default">
			<div class="box-body">
				<p class="status-bar lead">
					Mã&nbsp;
					<span class="item-reference pad">
						<b><?=@$itemID?></b>
					</span> &nbsp; Vật:&nbsp;
					<span class="item-type bg-default pad nowrap"><?= ( !empty($detail) && $detail[0]->type == 1 ) ? "Mất" : "Thấy"  ?></span> &nbsp;&nbsp; Trạng Thái:&nbsp;
					<span class="item-type bg-default"><?= @$itemStatus ?></span> &nbsp;&nbsp; Mất&nbsp;tại:&nbsp;
					<span class="item-type bg-default"><?= !empty($location) ? $location[0]['name'] : "" ?></span>
				</p>
			</div>
		</div>
		<div class="block block-list-details">
			<div class="row">
				<div class="col-md-6 col-print-sm-6">
					<div class="box box-default">
						<div class="box-header">
							<h3 class="box-title">
								<i class="fa fa-list-alt"></i>&nbsp; Chi tiết</h3>
							<div class="box-tools pull-right">
								<a href="<?=base_url('chi-tiet/edit-lost-item-details-'.@$itemID .'.html')?>" class="btn btn-sm btn-default">
									<i class="fa fa-edit"></i>&nbsp; Cập nhật</a>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-sm-4 col-md-4 images-wrapper">
									<div class="primary-image">
										<?php if( !empty($image) ) { ?>
										<img class="img-thumbnail img-fluid" src="<?=base_url($image[0]->path)?>" width="480" height="360" alt="">
										<?php } else { ?>
										<img class="img-thumbnail img-fluid" src="<?=base_url('public\assets\images\demo\\5iPhone_8_1.jpg')?>" width="480" height="360" alt="">
										<?php } ?>										
									</div>
									<div class="gallery-images">
										<div class="row small-gutter">
										</div>
									</div>
								</div>
								<div class="col-sm-8 col-md-8 item-wrapper">
									<div class="item">
										<h4>
											<?= ( !empty($category) ) ? $category[0]['name'] : ""; ?>
										</h4>
										<div class="table-responsive">
											<table class="table table-condensed">
												<colgroup>
													<col width="30%">
													<col>
												</colgroup>
												<tbody>
													<tr>
														<th>Serial Number</th>
														<td><?= @$detail[0]->tag_number; ?></td>
													</tr>
													<?php foreach ($property as $value) { ?>
														<tr>
															<th><?= $value['name'] ?></th>
															<td><?= $value['value'] ?></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box box-default matches">
						<div class="box-header">
							<h3 class="box-title">
								<?php if( $detail[0]->status == 3 ){ ?>
									<i class="fa fa-check"></i>&nbsp; Matched
								<?php }else{ ?> 
									<i class="fa fa-search"></i>&nbsp; Possible Matches
								<?php } ?>
							</h3>
						</div>
					</div>
					<div class="box box-default owner-details">
						<div class="box-header">
							<h3 class="box-title">
								<i class="fa fa-user"></i>&nbsp; <?= ( !empty($detail) && $detail[0]->type == 1 ) ? "Chủ sở hữu" : ( !empty($detail) && $detail[0]->type == 2 ) ? "Người tìm thấy" : "Không phân biệt"; ?></h3>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<table class="table">
									<colgroup>
										<col width="1">
										<col>
									</colgroup>
									<tbody>
										<tr>
											<th class="nobr">Họ tên</th>
											<td><?= $detail[0]->fullname ?></td>
										</tr>
										<tr>
											<th class="nobr">Email</th>
											<td><?= $detail[0]->email ?></td>
										</tr>
										<tr>
											<th class="nobr">Số điện thoại</th>
											<td><?= $detail[0]->phone_number ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="box box-default notes">
						<div class="box-header">
							<h3 class="box-title">
								<i class="fa fa-file-text-o"></i>&nbsp; Mô tả</h3>
						</div>
						<div class="box-body">
							<?= $detail[0]->detail ?>
							<hr>
						</div>
					</div>
				</div>
						<div class="col-md-6 col-print-sm-6">
						<div class="box box-default filters filter-details">
							<form role="form" method="get" action="<?= base_url($action.'/'.$itemID ) ?>" class="form form-inline">
								<!-- <input type="hidden" name="keys" value="<?=!empty($keys)? $keys:''?>" /> -->
								<div class="box-header">
										<h3 class="box-title">
											<i class="fa fa-fw fa-filter"></i>
											Filters
										</h3>
										<div class="box-tools">
											<button type="submit" value="submit" id="submit" name="submit" class="btn btn-sm btn-default"><i class="fa fa-filter"></i> Apply Filters</button>
										</div>
								</div>
								<div class="box-body">
										
												<div class="form-group ">
													<label class="control-label" for="category">Vật dụng</label>
														<select id="category" multiple placeholder="Chọn vật dụng..." name="category[]" data-live-search="1" data-size="10" data-icon-base="fa" data-tick-icon="fa-check" class="form-control form-control selectpicker form-control" tabindex="-98">
																<optgroup label="Current Categories">
																	<?php foreach($categorys as $category ){ ?>
																	<option title="<?=$category['description']?>" <?php
																		if(!empty($this->input->get('category'))) echo in_array($category['id'],$this->input->get('category'))? 'selected':''?> value="<?=$category['id']?>"><?=$category['name']?></option>
																	<?php } // end foreach caterory ?>
																</optgroup>
																<!--<optgroup label="Old Categories">
																	<option value="Camera">Camera</option>
																</optgroup>-->
														</select>
												</div>
												<div class="form-group ">
													<label class="control-label" for="locations">Khu Vực</label>
														<select id="locations" multiple placeholder="Chọn khu vực..." name="locations[]" data-live-search="1" data-size="10" data-icon-base="fa" data-tick-icon="fa-check" class="form-control form-control selectpicker form-control" tabindex="-98">
															<?php foreach($locations as $location){ ?>
															<option <?php
																		if(!empty($this->input->get('locations'))) echo in_array($location['id'],$this->input->get('locations'))? 'selected':''?> value="<?=$location['id']?>"><?=$location['name']?></option>
															<?php } // foreach location ?>
														</select>
												</div>
												<!-- <div class="form-group">
                                <label class="control-label" for="dateRangeStart">Lost/Found Date</label>
                                <div class="input-daterange input-group">
                                    <input type="datetime-local" id="dateRangeStart" name="dateRangeStart" class="form-control" value="<?=$this->input->get('dateRangeStart')?>">
                                    <span class="input-group-addon">to</span>
                                    <input type="datetime-local" <?=$this->input->get('dateRangeEnd')?> id="dateRangeEnd" name="dateRangeEnd" class="form-control">
                                </div>
                              </div> -->
												<div class="form-group ">
													<label class="control-label" for="query">Search</label>
													<input type="text" id="query" name="keys" value="<?=!empty($keys)? $keys:''?>" class="form-control">
												</div>
										
								</div>
							</form>
				    </div>
							<div class="items">
								<!-- <a class="item box box-default" href="#">
									<div class="box-body">
										<div class="row">
											<div class="col-sm-2 col-md-4 col-lg-2">
												<p>
													<span class="small text-muted">Mã</span>
													85
												</p>
												<div class="image-wrapper">
													<span class="primary-image">
													</span>
												</div>
											</div>
											<div class="col-sm-6 col-md-8 col-lg-6 item-details">
												<h4>
													Camera
												</h4>
												<div class="table-responsive">
													<table class="table table-condensed">
														<colgroup>
															<col width="30%">
															<col>
														</colgroup>
														<tbody>
															<tr>
																<th>Serial Number</th>
																<td>WS81F4M7CQ1</td>
															</tr>
															<tr>
																<th>Camera Make</th>
																<td>Canon</td>
															</tr>
															<tr>
																<th>Camera Model</th>
																<td>EOS 1200D</td>
															</tr>
															<tr>
																<th>Camera Type</th>
																<td>Digital/SLR</td>
															</tr>
															<tr>
																<th>Resolution</th>
																<td>18</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div class="col-sm-4 col-md-12 col-lg-4">
												<ul class="list-unstyled hidden-xs">
													<li>Status:
														<span class="item-status bg-default">New</span>
													</li>
													<li>
														<span class="item-type bg-default">Thấy</span>
														lúc 19/11/2017, 4:02pm
													</li>
													<li>thấy tại
														<span class="item-location bg-default">C23</span>
													</li>
													<li class="owner" title="Item's owner">
														<i class="fa fa-user"></i> Test User
													</li>
													<li class="owner-email" title="Item owner's email address">
														<i class="fa fa-envelope-o"></i> test-user@wwsgo.com
													</li>
												</ul>
											</div>
										</div>
									</div>
								</a> -->
								<?php if( !empty( $matches ) ){ ?> 
									<?php foreach ($matches as $key => $value) {?> 
										<a class="item box box-default" href="<?= base_url('chi-tiet/'.$itemID. '/so-sanh/' . $value->id) ?>">
										<div class="box-body">
											<div class="row">
												<div class="col-sm-2 col-md-4 col-lg-2">
													<p>
														<span class="small text-muted">Mã</span>
														<?= $value->id; ?>
													</p>
													<div class="image-wrapper">
														<span class="primary-image">
														</span>
													</div>
												</div>
												<div class="col-sm-6 col-md-8 col-lg-6 item-details">
													<h4>
													<?= $value->category_name; ?>
													</h4>
													<div class="table-responsive">
														<table class="table table-condensed">
															<colgroup>
																<col width="30%">
																<col>
															</colgroup>
															<tbody>
																<tr>
																	<th>Serial Number</th>
																	<td><?= @$value->tag_number ?></td>
																</tr>
																<?php 
															$property = explode('::', $value->property_name );
															foreach ($property as $property_item) {
																		$item = explode('||', $property_item ); ?>
																		<tr>
																			<th><?=$item[0]?></th>
																			<td><?=$item[1]?></td>
																		</tr>
																	<?php }
																?>
															</tbody>
														</table>
													</div>
												</div>
												<div class="col-sm-4 col-md-12 col-lg-4">
													<ul class="list-unstyled hidden-xs">
														<li>Phần trăm :
															<span class="item-status bg-default"><?= @$value->percent ?>%</span>
														</li>
														<li>Status:
															<span class="item-status bg-default">New</span>
														</li>
														<li>
															<span class="item-type bg-default"><?= ( $value->type == 1 ) ? "Mất" : "Thấy" ?></span>
															lúc <?= date('d/m/Y H:i:s', strtotime($value->created_at)) ?>
														</li>
														<li><?= ( $value->type == 1 ) ? "Mất" : "Thấy" ?> tại
															<span class="item-location bg-default"><?= $value->location_name ?></span>
														</li>
														<li class="owner" title="Item's owner">
															<i class="fa fa-user"></i> <?= $value->fullname ?>
														</li>
														<li class="owner-email" title="Item owner's email address">
															<i class="fa fa-envelope-o"></i> <?= $value->email ?>
														</li>
														<li class="owner-phone" title="Item owner's contact number">
															<i class="fa fa-mobile"></i> <?=$value->phone_number	 ?>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</a>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>