<section id="item-create-list" class="item-create-list main">
    <div class="container">
        <div class="block block-header-page">
            <h1 class="page-title"><?=$title?></h1>
            <nav class="breadcrumb">
                <ol>
                    <li>
                       <a href="<?=base_url()?>">Trang Chủ</a>
                    </li>
                    <?php foreach( $breadcrumbs as $breadcrumb ): ?>
                    <li>
                       <a href="<?=base_url($breadcrumb['uri']);?>"><?=$breadcrumb['text']?></a>
                    </li>
                    <?php endforeach; ?>
                </ol>
            </nav>
        </div>
            <div class="block block-filter">
              <div class="row">
                <div class="col-sm-12">
                  <div class="box box-default filters">
                    <form role="form" method="get" action="<?= base_url($action) ?>" class="form">
                    <input type="hidden" name="keys" value="<?=!empty($keys)? $keys:''?>" />
                       <div class="box-header">
                          <h3 class="box-title">
                             <i class="fa fa-fw fa-filter"></i>
                             Filters
                          </h3>
                          <div class="box-tools">
                             <button type="submit" value="submit" id="submit" name="submit" class="btn btn-sm btn-default"><i class="fa fa-filter"></i> Apply Filters</button>
                          </div>
                       </div>
                       <div class="box-body">
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="form-group ">
                                <label class="control-label required" for="category">Vật dụng</label>
                                  <select id="category" multiple placeholder="Chọn vật dụng..." name="category[]" data-live-search="1" data-size="10" data-icon-base="fa" data-tick-icon="fa-check" class="form-control form-control selectpicker form-control" tabindex="-98">
                                      <optgroup label="Current Categories">
                                        <?php foreach($categorys as $category ){ ?>
                                        <option title="<?=$category['description']?>" <?php
                                          if(!empty($this->input->get('category'))) echo in_array($category['id'],$this->input->get('category'))? 'selected':''?> value="<?=$category['id']?>"><?=$category['name']?></option>
                                        <?php } // end foreach caterory ?>
                                      </optgroup>
                                      <!--<optgroup label="Old Categories">
                                        <option value="Camera">Camera</option>
                                      </optgroup>-->
                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group ">
                                <label class="control-label required" for="locations">Khu Vực</label>
                                  <select id="locations" multiple placeholder="Chọn khu vực..." name="locations[]" data-live-search="1" data-size="10" data-icon-base="fa" data-tick-icon="fa-check" class="form-control form-control selectpicker form-control" tabindex="-98">
                                    <?php foreach($locations as $location){ ?>
                                    <option <?php
                                          if(!empty($this->input->get('locations'))) echo in_array($location['id'],$this->input->get('locations'))? 'selected':''?> value="<?=$location['id']?>"><?=$location['name']?></option>
                                    <?php } // foreach location ?>
                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label" for="dateRangeStart">Lost/Found Date</label>
                                <div class="input-daterange input-group">
                                    <input type="datetime-local" id="dateRangeStart" name="dateRangeStart" class="form-control" value="<?=$this->input->get('dateRangeStart')?>">
                                    <span class="input-group-addon">to</span>
                                    <input type="datetime-local" <?=$this->input->get('dateRangeEnd')?> id="dateRangeEnd" name="dateRangeEnd" class="form-control">
                                </div>
                              </div>
                            </div>
                          </div>
                       </div>
                    </form>
                 </div>
                </div>
              </div>
            </div>
       <div class="block block-list-box box-padding">
         <div class="items">
            <?php $ids = []; if(!empty($items)) foreach($items as $item):  
                if( isset( $ids[$item['ma']] ) ) continue;
                if( $this->uri->segment(2) == 'trung-khop' ){
                    #$ids[$item['ma']] = 1;
                    if( array_search( $item['type'], unserialize(TYPE_ITEM) ) === false ){
                        die(' type ko đúng dịnh dạng ' . $item['ma']);
                    }
                    $matched_item = $this->itemModel->getItemMatched($item['ma'],$item['type'], unserialize(MATCHING_STATUS)['matched'] );
                    
                    if( isset($matched_item[0]['item_lost_id'] ) &&  !empty($matched_item[0]['item_lost_id']) ){
                        $type_diff = (array_search($item['type'],unserialize(TYPE_ITEM))=='lost')? 'found':'lost';
                        $ids[ $matched_item[0][ 'item_'.$type_diff.'_id' ] ]  = 1;
                        #$matched_item_detail = $this->itemModel->getById();
                        $matched_item_detail = [];
                        
                        foreach( $all_items as $item_tmp ){
                            if( $item_tmp['ma'] == $matched_item[0][ 'item_'.$type_diff.'_id' ] ){
                                $matched_item_detail = $item_tmp;
                                break;
                            }
                        }
                    }else{
                        die(' đã match nh7ng ko thấy sp matched');
					}
                }
                ?>
            <a class="item box box-default" id="<?=@$item['ma']?>" href="<?=base_url('chi-tiet/'.$item['ma'])?>">
              <div class="box-body">
                  <div class="row">
                    <div class="col-sm-2 col-md-4 col-lg-2">
                        <p>
                          <span class="small text-muted">Mã</span>
                          <?=$item['ma']?>
                        </p>
                        <div class="image-wrapper">
                          <span class="primary-image">
                          </span>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8 col-lg-6 item-details">
                        <h4>
                          <?=$item['category']?>
                        </h4>
                        <div class="table-responsive">
                          <table class="table table-condensed">
                              <colgroup>
                                <col width="30%">
                                <col>
                              </colgroup>
                              <tbody>
                                <?php $propertypes = $this->itemModel->getProperty($item['ma']); 
                                if(!empty($propertypes)) foreach($propertypes as $propertype): ?>
                                <tr>
                                    <th><?=$propertype['name']?></th>
                                    <td><?=$propertype['value']?></td>
                                </tr>
                                <?php endforeach; ?>
                              </tbody>
                          </table>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-12 col-lg-4">
                        <ul class="list-unstyled hidden-xs">
                          <li>Status: <span class="item-status bg-default"> <?= array_search($item['status'],unserialize(STATUS_ITEM)) ?> </span></li>
                          <li>
                              <span class="item-type bg-default"><?=($item['type']==1)? 'Mất':'Tìm thấy' ?></span>
                              lúc <?= date('d/m/Y h:i:s', strtotime($item['created_at']) )  ?>
                          </li>
                          <li><?=($item['type']==1)? 'Mất':'Tìm thấy' ?> tại <span class="item-location bg-default"><?=$item['place']?></span></li>
                          <li class="owner" title="Item's owner">
                              <i class="fa fa-user"></i> <?=$item['fullname']?>
                          </li>
                          <li class="owner-email" title="Item owner's email address">
                              <i class="fa fa-envelope-o"></i> <?=$item['email']?>
                          </li>
                          <li class="owner-phone" title="Item owner's contact number">
							  <i class="fa fa-mobile"></i> <?=$item['sdt'] ?>
						  </li>
                        </ul>
                    </div>
                  </div>
                  <?php if( $this->uri->segment(2) == 'trung-khop' ){ ?> 
                    <div class="match-separator">
				        <span>Trùng khớp với</span>
				    </div>
                    <div class="row">
                    <?php if( !empty($matched_item_detail) ){ ?> 
                                
									<div class="col-sm-2 col-md-4 col-lg-2">
										<p>
											<span class="small text-muted">Mã</span>
											<?= $matched_item_detail['ma'] ?>
										</p>
										<div class="image-wrapper">
											<span class="primary-image">
											</span>
										</div>
									</div>
									<div class="col-sm-6 col-md-8 col-lg-6 item-details">
										<h4>
											<?=$matched_item_detail['category'] ?>
										</h4>
										<div class="table-responsive">
											<table class="table table-condensed">
												<colgroup>
													<col width="30%">
													<col>
												</colgroup>
												<tbody>
                                                    <?php $propertypes = $this->itemModel->getProperty($matched_item_detail['ma']); 
                                                    if(!empty($propertypes)) foreach($propertypes as $propertype): ?>
                                                    <tr>
                                                        <th><?=$propertype['name']?></th>
                                                        <td><?=$propertype['value']?></td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
											</table>
										</div>
									</div>
									<div class="col-sm-4 col-md-12 col-lg-4">
										<ul class="list-unstyled hidden-xs">
											<li>Status:
												<span class="item-status bg-default"><?= array_search($matched_item_detail['status'],unserialize(STATUS_ITEM)) ?></span>
											</li>
											<li>
												<span class="item-type bg-default"><?=($matched_item_detail['type']==1)? 'Mất':'Tìm thấy' ?></span>
												lúc <?= date('d/m/Y h:i:s', strtotime($matched_item_detail['created_at']) )  ?>
											</li>
											<li><?=($matched_item_detail['type']==1)? 'Mất':'Tìm thấy' ?> tại
												<span class="item-location bg-default"><?=$matched_item_detail['place'] ?></span>
											</li>
											<li class="owner" title="Item's owner">
												<i class="fa fa-user"></i> <?=$matched_item_detail['fullname']?>
											</li>
											<li class="owner-email" title="Item owner's email address">
												<i class="fa fa-envelope-o"></i> <?=$matched_item_detail['email']?>
											</li>
											<li class="owner-phone" title="Item owner's contact number">
												<i class="fa fa-mobile"></i> <?=$matched_item_detail['sdt'] ?>
											</li>
										</ul>
									</div>
								                  
                    <?php } ?>
                  </div>
                  <?php } ?>
                  
                  
              </div>
            </a>
            <?php endforeach; ?>
         </div>
         <nav class="pager" role="navigation" aria-labelledby="pagination-heading">
            <ul class="pager__items js-pager__items">
                <?=empty($pagelink['prevLink'])? '<li class="pager__item pager__item--previous disabled">
                  <a href="javascript:void(0);" title="Trang trước" rel="next">
                  <span class="visually-hidden">Prev page</span>
                  <span aria-hidden="true">››</span>
                  </a>
                </li>': 
                '<li class="pager__item pager__item--previous">
                  <a href="'.$pagelink['prevLink'].'" title="Trang trước" rel="next">
                  <span class="visually-hidden">Prev page</span>
                  <span aria-hidden="true">››</span>
                  </a>
                </li>' ?>
               <?php if(!empty($pagelist)) foreach($pagelist as $key => $numpage): ?>
               <?php if(empty($numpage)) { ?>
               <li class="pager__item is-active">
                  <a href="javascript:void(0)" title="Trang hiện tại">
                  <span class="visually-hidden">
                  Current page
                  </span><?=$key?></a>
               </li>
               <?php }else{ ?>
                <li class="pager__item">
                  <a href="<?=$numpage?>" title="Đến trang <?=$key?>">
                  <span class="visually-hidden">
                  Page
                  </span><?=$key?></a>
                </li>
               <?php } ?>
               <?php endforeach; ?>
               <?=empty($pagelink['nextLink'])? '<li class="pager__item pager__item--next disabled">
                  <a href="javascript:void(0)" title="Trang kế" rel="next">
                  <span class="visually-hidden">Next page</span>
                  <span aria-hidden="true">››</span>
                  </a>
               </li>':
               '<li class="pager__item pager__item--next">
                  <a href="'.$pagelink['nextLink'].'" title="Trang kế" rel="next">
                  <span class="visually-hidden">Next page</span>
                  <span aria-hidden="true">››</span>
                  </a>
               </li>' ?>
            </ul>
         </nav>
       </div>
    </div>
</section>