<section id="item-create-lost" class="main item-create-found">
   <div class="container">
      <div class="block block-header-page">
         <h1 class="page-title">Trang Báo Mất</h1>
         <nav class="breadcrumb">
            <ol>
               <li>
                  <a href="index.html">Trang Chủ</a>
               </li>
               <li>
                  <a href="#">Trang Báo Mất</a>
               </li>
            </ol>
         </nav>
      </div>
      <div class="block block-form-lost">
         <div class="row">
            <div class="col-md-10 col-md-push-1 col-lg-6 col-lg-push-3">
               <?php
                  if(!empty($notify)) {
                     if($notify['status'] === 'success') {?>
                         <div class="alert alert-success alert-dismissable">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                             <strong><?= $notify['caption'] ?></strong><?= $notify['message'] ?>
                         </div>
                     <?php } else { ?>
                         <div class="alert alert-danger alert-dismissable">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                             <strong><?= $notify['caption'] ?></strong><?= $notify['message'] ?>
                         </div>
                  <?php }
                  } 
               ?>
               <div class="box box-default">
                  <div class="box-body">
                     <?= form_open(NULL, array('id'=>'itemForm', 'class'=>'form', 'role'=>'form')) ?>
                        <fieldset>
                           <legend>Chi Tiết Vật</legend>
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="form-group ">
                                    <label class="control-label required" for="item_customCategory">Vật Dụng</label>
                                    <select id="item_customCategory" name="item[customCategory]" required="required" data-help="" class="form-control form-control selectpicker form-control" tabindex="-98">
                                       <option value="" selected="selected">Mời chọn...</option>
                                       <?php foreach ($categories as $category) { ?>
                                          <option value="<?=$category->id?>"><?=$category->name?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                                 <div id="item_customDetails"></div>
                                 <!-- <div class="form-group "><label class="control-label" for="item_customDetails_field_0">Màu sắc</label><input type="text" id="item_customDetails_field_0" name="item[customDetails][field_0]" data-help="" class="form-control"></div>
                                 <div class="form-group "><label class="control-label required" for="item_customDetails_field_1">Hãng sản xuất</label><input type="text" id="item_customDetails_field_1" name="item[customDetails][field_1]" required="required" dsata-help="" class="form-control"></div> -->
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group "> 
                                    <label class="control-label required" for="item_location">Khu vực</label>
                                   
                                    <select id="item_location" name="item[location]" data-help="Where was the item lost?" class="form-control form-control selectpicker form-control" tabindex="-98">
                                       <?php $this->nestedset->showOption($locations); ?>
                                     </select>
                                    <span class="help-block small">Nơi bạn làm mất đồ vật?</span>
                                 </div>
                                 <div class="form-group "><label class="control-label" for="item_description">Mô tả chi tiết khác</label><textarea id="item_description" name="item[description]" data-help="Any other details, marks, or descriptive information." class="form-control form-control"></textarea><span class="help-block small">Cung cấp thêm bất kỳ thông tin để hỗ trợ tốt hơn cho hệ thống.</span></div>

                                 <div class="form-group">
                                    <label class="control-label required" for="item_reportedAt">Mất Lúc</label>
                                    <div class="input-group date" data-provide="datepicker">
                                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                       <input type="text" id="item_reportedAt" name="item[reportedAt]" placeholder="Mất Lúc" class="form-control datepicker" value="" data-date-format="dd/mm/yyyy" required="required" autocomplete="off" data-help="<i class=&quot;fa fa-clock-o&quot;></i> America / New York" data-addon-left="<i class=&quot;fa fa-calendar&quot;></i>">
                                    </div>
                                    <span class="help-block small"><i class="fa fa-clock-o"></i> Giờ Việt Nam</span>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label" for="tag_number">Tag Number</label>
                                    <input type="text" id="tag_number" name="item[tag_number]" data-help="" class="form-control">
                                 </div>
                              </div>
                           </div>
                        </fieldset>
                         <label class="control-label">Image Upload</label>
                        <div class="dropzone dz-clickable box-file">
                                <input type="file" name='imageItem' id="file-2" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                        </div>
                        <fieldset>
                           <legend>Thông Tin Liên Lạc</legend>
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="form-group "><label class="control-label required" for="item_owner_firstName">Họ</label><input type="text" id="item_owner_firstName" name="item[owner][firstName]" required="required" class="form-control"></div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="form-group "><label class="control-label required" for="item_owner_lastName">Tên</label><input type="text" id="item_owner_lastName" name="item[owner][lastName]" required="required" class="form-control"></div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="form-group "><label class="control-label required" for="item_owner_email">Địa Chỉ Email</label><input type="email" id="item_owner_email" name="item[owner][email]" required="required" autocomplete="off" class="form-control"></div>
                                 <div class="checkbox"><input type="checkbox" id="item_sendOwnerEmail" name="item[sendOwnerEmail]" class="" value="1" checked="checked"><label class="text-center" for="item_sendOwnerEmail">Nhận email từ hệ thống? </label>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group "><label class="control-label" for="item_owner_contactTel">Số Điện Thoại</label><input type="number" id="item_owner_contactTel" name="item[owner][contactTel]" autocomplete="off" class="form-control" min="0" step="1" rel="number-only" /></div>
                              </div>
                           </div>
                        </fieldset>
                        <hr>
                        <div class="text-center">
                           <button type="submit" id="item_save" class="btn btn-success"><i class="fa fa-check"></i>&nbsp; Lưu</button>
                           <!-- <button type="submit" id="item_saveAndAdd" name="item[saveAndAdd]" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp; Save &amp; Add Another</button> -->
                        </div>
                     <?=form_close()?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>