<section id="main" class="main">
    <div class="container">
        <div class="block block-header-page">
            <h1 class="page-title">Thống kê</h1>
            <nav class="breadcrumb">
                <ol>
                <li>
                    <a href="<?=base_url()?>">Trang Chủ</a>
                </li>
                <li>
                    <a href="#">Thống kê</a>
                </li>
                </ol>
            </nav>
        </div>
        <!-- HTML content o day -->
        <div class="block block-form-settings">
            
            <div class="row">
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div id="chartContainerAll" class="chartcontainer"></div>   
                </div>
                
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div id="chartContainerLost" class="chartcontainer"></div>   
                        <div id="chartContainerMatches" class="chartcontainer"></div>                     
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div id="chartContainerFound" class="chartcontainer"></div>            
                        <div id="chartContainerDisposal" class="chartcontainer"></div>            
                </div>
            </div>
        </div>
    </div>
</section>