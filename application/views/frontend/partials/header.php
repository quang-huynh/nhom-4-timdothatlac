<!-- Header -->
<header id="header" class="header" role="header">
    <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu-inner">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="region region-header">
            <div id="block-benestar-branding" class="group-header-inner-5">
                <a href="/" title="Trang Chủ" rel="Trang Chủ" class="site-logo">
                    <img src="<?php echo base_url(); ?>/public/logo.svg" alt="Trang Chủ">
                </a>
            </div>
            <div class="group-header">
                <?php 
                    if( $login ) { ?>
                     <div class="block-switchview">
                        <div class="group-action">
                            <a class="btn switch-view" href="<?php echo base_url( 'bao-mat-do-vat' ) ?>"><span class="fa fa-plus">&nbsp;</span> Báo Mất</a>
                            <a href="<?php echo base_url( 'bao-tim-thay-do-vat' ) ?>" class="btn sort-by"><span class="fa fa-plus">&nbsp;</span>Báo Tìm Thấy</a>
                        </div>
                    </div> 
                   <?php }
                ?>
               
                <div class="search-block-form group-header-inner-3 block block-search" id="block-searchform" role="search">
                    <form action="<?=base_url('tim-kiem')?>" method="get" id="search-block-form" accept-charset="UTF-8">
                        <div class="form-item form-type-search">
                            <input title="Enter the terms you wish to search for." type="search" id="edit-keys" name="keys" value="" size="15" maxlength="128" class="form-search" placeholder="Từ khóa...">
                        </div>
                        <div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" value="Search" class="button form-submit"></div>
                    </form>
                </div>
            </div>
            <?php 
                if( $login ) { ?>
                    <nav role="navigation" id="block-menuheader" class="block block-menu navigation menu--menu-header">
                         <ul class="menu">
                            <li class="menu-item">
                                <a href="#" rel="">Xin chào: <?= $login['fullname'] ?></a>
                            </li>
                        </ul>
                    </nav>
                    <div id="block-iconnotification" class="block block-block-content">
                        <div class="content">
                            <div class="clearfix field field--name-body quickedit-field">
                                <div class="group-action">
                                    <p class="notification-header">Thông báo</p>
                                    <a class="btn btn-logout" href="<?=base_url( 'user/logout' )?>">Đăng xuất</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                <nav role="navigation" id="block-menuheader" class="block block-menu navigation menu--menu-header">
                     <ul class="menu">
                        <li class="menu-item">
                            <a href="<?=base_url( '/user/login' )?>" rel="">Đăng nhập</a>
                        </li>
                    </ul>
                </nav>
            <?php } ?>
        </div>
    </div>
</header>