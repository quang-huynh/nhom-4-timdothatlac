<!-- Navigation -->
<div id="navigation-menu" class="navigation-menu">
    <div class="navigation-inner">
        <div class="navigation-info">
            <div class="region region-navigation-menu">
                <?php if( $login ) { ?>
                <nav role="navigation" id="block-benestar-main-menu" class="block block-menu navigation menu--main">
                    <ul class="menu">
                        <li class="menu-item">
                            <a href="<?php echo base_url( '/' ) ?>" class="is-active">Bảng Tổng Hợp</a>
                        </li>
                        <li class="menu-item menu-item--expanded">
                            <a href="javascript:void(0)">Đồ Vật</a>
                            <ul class="menu">
                            <?php if( $login['role_id'] == unserialize(ROLE_USER)['admin'] || $login['role_id'] == unserialize(ROLE_USER)['staff'] || $login['role_id'] == unserialize(ROLE_USER)['user']) { ?>
                                    <li class="menu-item">
                                        <a href="<?php echo base_url( 'danh-sach-do-vat/' ) ?>">Tất cả</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="<?php echo base_url( 'danh-sach-do-vat/bi-mat' ) ?>">Mất</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="<?php echo base_url( 'danh-sach-do-vat/tim-thay' ) ?>">Tìm thấy</a>
                                    </li>
                                <?php }
                                if( $login['role_id'] == unserialize(ROLE_USER)['admin'] || $login['role_id'] == unserialize(ROLE_USER)['staff'] ) { ?>
                                    <li class="menu-item">
                                        <a href="<?php echo base_url( 'danh-sach-do-vat/trung-khop/' ) ?>">Trùng khớp</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="#">Đã trả</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="#">Đang xử lý</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php if($login['role_id'] == unserialize(ROLE_USER)['admin'] || $login['role_id'] == unserialize(ROLE_USER)['staff'] ) { ?>
                        <li class="menu-item">
                            <a href="<?php echo base_url( 'thong-ke' ) ?>">Thống kê</a>
                        </li>
                        <?php } ?>
                        <?php if($login['role_id'] == unserialize(ROLE_USER)['admin'] ) { ?>
                        <li class="menu-item">
                            <a href="<?php echo base_url( 'admin' ) ?>">Cài đặt hệ thống</a>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
                <?php } ?>
                <div id="block-livechat" class="block block-block-content">
                    <div class="content">
                        <div class="clearfix field field--name-body">
                            <div>
                                <div class="menu-livechat">
                                    <p>Liên hệ chúng tôi</p>
                                    <p>16HCB - Nhom 4</p>
                                    <!-- <a href="#"><button>Chat Trực Tuyến</button></a> -->
                                </div>
                                <div class="menu-copyright">
                                    <p> &copy;2020. WWSGO - Khoa học tự nhiên</p>
                                    <a href="#">Giới thiệu WWSGO</a>
                                    <a href="#">Gửi góp ý</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>