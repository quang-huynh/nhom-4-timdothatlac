<div class="container">
    <h1 class="page-title">Đăng ký tài khoản</h1>
	<div class="col-lg-12">
		<div class="row">
			<?php
                  if(!empty($notify)) {
                     if($notify['status'] === 'success') {?>
                         <div class="alert alert-success alert-dismissable">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                             <strong><?= $notify['caption'] ?></strong><?= $notify['message'] ?>
                         </div>
                     <?php } else { ?>
                         <div class="alert alert-danger alert-dismissable">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                             <strong><?= $notify['caption'] ?></strong><?= $notify['message'] ?>
                         </div>
                  <?php }
                  } 
                  echo validation_errors('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Đăng ký lỗi: </strong>', '</div>');
               ?>
			<?= form_open_multipart(NULL, array("id"=>"registerform", "class"=>"form col-md-8")) ?>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-12 form-group">
							<label>Họ tên</label>
							<input type="text" id="fullname" name='user[fullname]' placeholder="Nhập Họ tên của bạn.." class="form-control" required="" value="<?= set_value('user[fullname]')?>">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label>Email</label>
							<input type="text" id="email" name="user[email]" class="form-control" placeholder="Nhập email của bạn.." required value="<?= set_value('user[email]')?>">
						</div>
						<div class="col-sm-6 form-group">
							<label>Password</label>
							<input type="password" id="password" name="user[password]" class="form-control" required>
						</div>
					</div>					
					<div class="form-group">
						<label>Địa chỉ</label>
						<textarea id="address" name="user[address]" placeholder="Enter Address Here.." rows="3" class="form-control"><?= set_value('user[address]')?></textarea>
					</div>
					<div class="form-group">
						<label>Ngày sinh</label>
						<div class="input-group date" data-provide="datepicker">
			               	<input type="text" id="birthday" name="user[birthday]" placeholder="Ngày sinh" class="form-control datepicker" value="<?= set_value('user[birthday]')?>" data-date-format="dd/mm/yyyy">
			              	<div class="input-group-addon">
			                  	<span class="glyphicon glyphicon-th"></span>
			              	</div>
		          		</div>
					</div>	
					<div class="row">
						<div class="col-sm-6 form-group">
							<label>Số điện thoại</label>
							<input type="text" id="phone_number" name="user[phone_number]" placeholder="" class="form-control" required value="<?= set_value('user[phone_number]')?>">
						</div>
						<div class="col-sm-6 form-group">
							<label>Giới tính</label>
							<select id="gender" name="user[gender]" class="form-control">
								<option value="0">Nam</option>
								<option value="1">Nữ</option>
							</select>
						</div>
					</div>										
					<div class="form-group">
						<label>CMND</label>
						<input type="text" name="user[identify_card]" placeholder="Enter Phone Number Here.." class="form-control" value="<?= set_value('user[identify_card]')?>">
					</div>
					<div class="form-group">
						<label>Ảnh đại diện</label>
						<input id="image" name="image" type="file" class="file">
					</div>
					<button type="Submit" class="btn btn-lg btn-info">Đăng ký</button>					
				</div>
			<?= form_close() ?>
		</div>
	</div>
</div>