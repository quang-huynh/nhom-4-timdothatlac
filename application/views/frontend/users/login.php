<div class="container">
	<h1 class="page-title text-center">Đăng nhập</h1>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-login">				
				<div class="panel-body">
					<div class="row">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Error: </strong>', '</div>'); ?>
						<div class="col-lg-12">
							<?= form_open(NULL, array('role'=>'form', 'id'=>"login-form", 'style'=>"display: block;")) ?>
								<div class="form-group">
									<input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Email" value="<?= set_value('email') ?>">
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="login-submit" id="login-submit" class="btn btn-primary" value="Đăng nhập">
											<a href="<?= base_url('user/register'); ?>" id="register-form-link" class="btn btn-info">Đăng ký</a>
										</div>
									</div>
								</div>
							<?= form_close() ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>