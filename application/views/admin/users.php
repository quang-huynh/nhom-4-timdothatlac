<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Bảng thành viên
      <small><a href="<?php echo base_url('admin/add/users'); ?>">
        <button type="button" class="btn btn-block btn-primary btn-sm">THÊM <span class="glyphicon glyphicon-plus"></span></button>
      </a></small>
    </h1>
  </section>
  <div class="pad margin no-print">
    <div class="callout callout-warning" style="margin-bottom: 0!important;">
      <h4><i class="fa fa-warning"></i> Ghi chú:</h4>
      Tìm kiếm theo tên thành viên
    </div>
    <?php if($delete_success||$name_exist||$NochangePassword_success){ ?>
    <div class="alert alert-danger fade in alert-dismissable col-md-4" style="margin-top: 10px;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
      <strong>Thông báo!</strong> <?php if($delete_success){echo 'Đã xóa.';}elseif($name_exist){echo 'Tên Thành viên đã tồn tại.';}elseif($NochangePassword_success){echo 'Mật khẩu này chưa được thay đổi.';} ?>
    </div>
    <?php }elseif($add_success||$status_success||$changeName_success||$changeRole_success||$changePassword_success){ ?>
    <div class="alert alert-success fade in alert-dismissable col-md-4" style="margin-top:10px;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
      <strong>Thông báo!</strong> <?php if($add_success){echo 'Đã thêm.';}elseif($status_success){echo 'Đổi trạng thái thành công.';}elseif($changeName_success){echo 'Đổi tên thành công.';}elseif($changeRole_success){echo 'Đổi phân quyền thành công.';}elseif($changePassword_success){echo 'Đổi mật khẩu thành công.';} ?>
    </div>
    <?php } ?>
  </div>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <form action="" method="get">
          <div class="box-header">
            <h3 class="box-title" style="text-transform: uppercase;"><small><span class="glyphicon glyphicon-th-list"></span></small> Danh sách thành viên</h3>
            <div class="box-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="search" class="form-control pull-right" placeholder="Tìm kiếm.." value="<?php echo $this->input->get('search'); ?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
              </div>
            </div>
          </div>
          </form>
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover table-bordered">
              <tbody><tr>
                <th class="text-center" style="width: 100px;">STT</th>
                <th>Tên thành viên</th>
                <th class="text-center" style="width: 150px;">Quyền</th>
                <th class="text-center" style="width: 150px;">Ngày đăng ký</th>
                <th class="text-center" style="width: 150px;">Trạng thái</th>
                <th class="text-center" style="width: 150px;"><span class="glyphicon glyphicon-cog"></span></th>
              </tr>
              <?php  $i=0;
                foreach ($user as $k_user => $v_user): $i++;
                  if($v_user['status']==1){$status='Đã kích hoạt';$color_status='success';}
                  else{$status='Đã tắt';$color_status='danger';}

                  $row_role=$this->M_users->row('role',array('id'=>$v_user['created_by']),NULL);
              ?>
              <tr>
                <td class="text-center"><?php echo $i; ?></td>
                <td><?php echo $v_user['fullname'] ?><br>
                  <a href="#" data-toggle="modal" data-target="#change_name<?php echo $v_user['id'];?>" title="Đổi tên Thành viên"><span class="glyphicon glyphicon-edit"></span> Đổi tên Thành viên</a>&nbsp;|&nbsp;
                  <a href="#" data-toggle="modal" data-target="#change_password<?php echo $v_user['id'];?>" title="Đổi Mật khẩu"><span class="glyphicon glyphicon-edit"></span> Đổi Mật khẩu</a>
                </td>
                <td class="text-center"><?php echo $row_role['name']; ?>
                  <a href="#" data-toggle="modal" data-target="#change_role<?php echo $v_user['id'];?>" title="Đổi phân quyền"><span class="glyphicon glyphicon-edit pull-right"></span></a>
                </td>
                <td class="text-center"><?php echo $v_user['created_at'] ?></td>
                <td class="text-center">
                  <span class="label label-<?php echo $color_status; ?>"><?php echo $status; ?></span>
                </td>
                <td class="text-center">
                  <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url('admin/edit/users/'.$v_user['id']); ?>" title="Chỉnh sửa thành viên" style="color:#3c8dbc;">
                    <span class="glyphicon glyphicon-edit"></span>
                  </a> 
                  <a class="btn btn-circle btn-icon-only btn-default" href="#" style="color: #dd4b39;" data-toggle="modal" data-target="#delete_users<?php echo $v_user['id'];?>" title="Xóa thành viên">
                    <span class="glyphicon glyphicon-trash"></span>
                  </a>
                  <a class="btn btn-circle btn-icon-only btn-default" href="#" style="color: green;" data-toggle="modal" data-target="#change_status<?php echo $v_user['id'];?>" title="Đổi trạng thái">
                    <span class="glyphicon glyphicon-refresh"></span>
                  </a>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody></table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php foreach ($user as $k_user => $v_user): ?>
<form action="" method="post">
  <input type="hidden" name="id" value="<?php echo $v_user['id'];?>">
  <div class="modal fade" id="delete_users<?php echo $v_user['id'];?>" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #5ac6de;color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
          <p>Bạn chắc chắn xóa thành viên tên: <b style="color: #dd4b39;"><?php echo $v_user['fullname']; ?></b></p>
        </div>
        <div class="modal-footer">
          <input type="submit" name="delete" class="btn btn-danger" value="Xóa">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php endforeach; ?>

<?php foreach ($user as $k_user => $v_user): ?>
<form action="" method="post">
  <input type="hidden" name="id" value="<?php echo $v_user['id'];?>">
  <div class="modal fade" id="change_status<?php echo $v_user['id'];?>" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #5ac6de;color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
          <p><span class="glyphicon glyphicon-refresh" style="color: green;"></span> Đổi trạng thái.</p>
        </div>
        <div class="modal-footer">
          <input type="submit" name="status" class="btn btn-primary" value="Đổi">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php endforeach; ?>

<?php foreach ($user as $k_user => $v_user): ?>
<form action="" method="post">
  <input type="hidden" name="id" value="<?php echo $v_user['id'];?>">
  <div class="modal fade" id="change_name<?php echo $v_user['id'];?>" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #5ac6de;color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
          <p><label>Đổi tên Thành viên:</label>
            <input type="text" name="name" value="<?php echo $v_user['fullname']; ?>" class="form-control" required>
          </p>
        </div>
        <div class="modal-footer">
          <input type="submit" name="change_name" class="btn btn-primary" value="Đổi">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php endforeach; ?>

<?php foreach ($user as $k_user => $v_user): ?>
<form action="" method="post">
  <input type="hidden" name="id" value="<?php echo $v_user['id'];?>">
  <div class="modal fade" id="change_password<?php echo $v_user['id'];?>" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #5ac6de;color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
          <p><label>Đổi mật khẩu: (Nhập mật khẩu mới)</label>
            <input type="password" name="password" class="form-control" required="">
          </p>
        </div>
        <div class="modal-footer">
          <input type="submit" name="change_password" class="btn btn-primary" value="Đổi">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php endforeach; ?>

<?php foreach ($user as $k_user => $v_user): ?>
<form action="" method="post">
  <input type="hidden" name="id" value="<?php echo $v_user['id'];?>">
  <div class="modal fade" id="change_role<?php echo $v_user['id'];?>" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #5ac6de;color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
          <p><label>Đổi phân quyền:</label>
            <select class="form-control" name="role">
              <?php  
                foreach ($role as $v_role) :

                  if($v_role['id']==$v_user['created_by']){$act_role='selected';}else{$act_role='';}

                  echo '<option value="'.$v_role['id'].'" '.$act_role.'>'.$v_role['name'].'</option>';
                endforeach;
              ?>
            </select>
          </p>
        </div>
        <div class="modal-footer">
          <input type="submit" name="change_role" class="btn btn-primary" value="Đổi">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php endforeach; ?>