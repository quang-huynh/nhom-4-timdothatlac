<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bảng điều khiển
        <small>Control Panel</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">

        <a href="<?php echo base_url('admin/area'); ?>" style="color: black;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><span class="glyphicon glyphicon-map-marker"></span></span>

            <div class="info-box-content">
              <span class="info-box-text">Khu vực</span>
              <span class="info-box-number"><?php echo $count_area; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    	</a>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <a href="<?php echo base_url('admin/users'); ?>" style="color: black;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><span class="glyphicon glyphicon-user"></span></span>

            <div class="info-box-content">
              <span class="info-box-text">Thành viên</span>
              <span class="info-box-number"><?php echo $count_user; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    	</a>
        <!-- /.col -->
        <a href="<?php echo base_url('admin/role'); ?>" style="color: black;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><span class="glyphicon glyphicon-user"></span></span>

            <div class="info-box-content">
              <span class="info-box-text">Phân quyền</span>
              <span class="info-box-number"><?php echo $count_role; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    	</a>
        <!-- /.col -->


      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->