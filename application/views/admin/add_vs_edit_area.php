<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo ($s1=='add') ? 'Thêm' : 'Cập nhật'; ?> khu vực
    </h1>
  </section>
  <section class="content">
      <?php if($edit_success){ ?>
      <div class="alert alert-success fade in alert-dismissable col-md-4" style="margin-top:10px;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
        <strong>Thông báo!</strong> <?php if($edit_success){echo 'Cập nhật thành công.';} ?>
      </div><div class="clearfix"></div>
      <?php } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Nhập đầy đủ thông tin</h3>
            </div>
            <form role="form" action="" method="post">
              <div class="box-body">
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Tên khu vực:</label>
                  <input type="text" class="form-control" name="name" placeholder="Tên khu vực" required="" value="<?php if($s1!='add'){echo $row_users['name'];} ?>">
                </div>
                <div class="form-group col-md-8">
                  <label for="exampleInputEmail1">Mô tả:</label>
                  <input type="text" class="form-control" name="description" placeholder="Mô tả" value="<?php if($s1!='add'){echo $row_users['description'];} ?>">
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Khu vực:</label>
                  <select class="form-control col-md-4" name="parent_id">
                    <option value="0">- Khu vực -</option>
                    <?php ($s1=='add') ? add_area($location) : edit_area($location,$row_users); ?>
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Kích hoạt:</label>
                  <select class="form-control col-md-4" name="status">
                    <?php
                      $status = array('1' => 'Có', '0' => 'Không');

                      $act_status='';

                      foreach ($status as $k_status => $v_status):

                        if($s1!='add'){($k_status==$row_users['status']) ? $act_status='selected':$act_status='';}

                        echo '<option value="'.$k_status.'" '.$act_status.'>'.$v_status.'</option>';

                      endforeach;
                    ?>
                  </select>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php echo ($s1=='add') ? 'Thêm' : 'Cập nhật'; ?></button>
                <a href="<?php echo base_url('admin/area'); ?>"><button type="button" class="btn btn-default pull-right">Thoát</button></a>
              </div>
            </form>
          </div>
      </div>
      <div class="pad margin no-print">
        <div class="callout callout-warning" style="margin-bottom: 0!important;">
          <h4><i class="fa fa-warning"></i> Ghi chú:</h4>
          Tên khu vực: Không được trùng.<br>
        </div>
      </div>
    </section>
</div>