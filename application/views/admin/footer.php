<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 5.0
  </div>
  <strong>Copyright &copy; 2017-2018 <a>16HCB</a>.</strong> All rights
  reserved.
</footer>