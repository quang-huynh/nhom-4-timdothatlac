<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/admin/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/admin/dist/css/AdminLTE.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url('/admin/login'); ?>"><b>ĐĂNG NHẬP</a>
  </div>
  <div class="login-box-body">
    <form action="" method="post">
      <?php if($email_fail || $password_fail){ ?>
      <div class="alert alert-danger fade in alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
        <strong>Thất bại!</strong> <?php if($email_fail){echo 'Email không chính xác.';}elseif ($password_fail){ echo 'Mật khẩu không chính xác.';} ?>
      </div>
      <?php } ?>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email" required="" value="<?php echo $email; ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Mật khẩu" required="" value="<?php echo $password; ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">&nbsp;</div>
        <div class="col-xs-4">
            <input type="submit" name="login" value="Đăng nhập" class="btn btn-primary btn-block btn-flat">
        </div>
        <div class="col-xs-4">&nbsp;</div>
      </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url(); ?>public/assets/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/bootstrap/js/bootstrap.min.js"></script>
</body></html>
