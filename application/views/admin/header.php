<header class="main-header">
  <a href="index2.html" class="logo">
    <span class="logo-mini"><b>A</b></span>
    <span class="logo-lg"><b>Admin</b></span>
  </a>
  <nav class="navbar navbar-static-top">
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo base_url(); ?>public/assets/admin/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
            <span class="hidden-xs"><?php echo $row_session['fullname']; ?></span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="<?php echo base_url(); ?>public/assets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
              <p>
                <?php echo $row_session['fullname']; ?>
                <small><?php echo $row_session['created_at']; ?></small>
              </p>
            </li>
            <li class="user-footer">
              <div class="pull-left">
                <a href="<?php echo base_url().'admin/edit/users/'.$row_session['id']; ?>" class="btn btn-default btn-flat">Xem thông tin cá nhân</a>
              </div>
              <div class="pull-right">
                <a href="<?php echo base_url('/admin/logout'); ?>" class="btn btn-default btn-flat">Đăng xuất</a>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>