<!DOCTYPE html><html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <title>Admin</title>

  <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/admin/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/admin/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/admin/dist/css/skins/_all-skins.min.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php  
    $this->load->view('admin/header');
    $this->load->view('admin/sidebar');
    $this->load->view('admin/'.$main);
    $this->load->view('admin/footer');
  ?>

</div>

<script src="<?php echo base_url(); ?>public/assets/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/plugins/fastclick/fastclick.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/dist/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/plugins/chartjs/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/dist/js/pages/dashboard2.js"></script>
<script src="<?php echo base_url(); ?>public/assets/admin/dist/js/demo.js"></script>

</body></html>
