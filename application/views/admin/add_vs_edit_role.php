<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Thêm phân quyền
    </h1>
  </section>
  <section class="content">
    <?php if($name_exist){ ?>
    <div class="alert alert-danger fade in alert-dismissable col-md-4" style="margin-top: 10px;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
      <strong>Thông báo!</strong> <?php if($name_exist){echo 'Tên phân quyền đã tồn tại.';} ?>
    </div><div class="clearfix"></div>
    <?php }elseif($edit_success){ ?>
    <div class="alert alert-success fade in alert-dismissable col-md-4" style="margin-top:10px;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
      <strong>Thông báo!</strong> <?php if($edit_success){echo 'Cập nhật thành công.';} ?>
    </div><div class="clearfix"></div>
    <?php } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Nhập đầy đủ thông tin</h3>
            </div>
            <form role="form" action="" method="post">
              <div class="box-body">
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Tên phân quyền: <span style="color: red;">(*)</span></label>
                  <input type="text" class="form-control" name="name" placeholder="Tên phân quyền" required="" value="<?php if($s1=='add'){echo $name;}else{echo $row_role['name'];} ?>">
                </div>
                <div class="form-group col-md-8">
                  <label for="exampleInputEmail1">Mô tả:</label>
                  <input type="text" class="form-control" name="description" placeholder="Mô tả" value="<?php if($s1=='add'){echo $description;}else{echo $row_role['description'];} ?>">
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php if($s1=='add'){echo 'Thêm';}else{echo 'Cập nhật';} ?></button>
                <a href="<?php echo base_url('admin/role'); ?>"><button type="button" class="btn btn-default pull-right">Thoát</button></a>
              </div>
            </form>
          </div>
      </div>
      <div class="pad margin no-print">
        <div class="callout callout-warning" style="margin-bottom: 0!important;">
          <h4><i class="fa fa-warning"></i> Ghi chú:</h4>
          Tên phân quyền: Không được trùng.
        </div>
      </div>
    </section>
</div>