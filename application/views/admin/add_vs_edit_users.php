<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php if($s1=='add'){echo 'Thêm';}else{echo 'Cập nhật';} ?> thành viên
    </h1>
  </section>
  <section class="content">
      <?php if($email_fail||$name_exist||$email_exist){ ?>
      <div class="alert alert-danger fade in alert-dismissable col-md-4" style="margin-top: 10px;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
        <strong>Thông báo!</strong> <?php if($email_fail){echo 'Email không hợp lệ.';}elseif($name_exist){echo 'Tên đã có người đăng ký.';}elseif($email_exist){echo 'Email đã có người đăng ký.';} ?>
      </div><div class="clearfix"></div>
      <?php }elseif($edit_success){ ?>
      <div class="alert alert-success fade in alert-dismissable col-md-4" style="margin-top:10px;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
        <strong>Thông báo!</strong> <?php if($edit_success){echo 'Cập nhật thành công.';} ?>
      </div><div class="clearfix"></div>
      <?php } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Nhập đầy đủ thông tin</h3>
            </div>
            <form role="form" action="" method="post">
              <div class="box-body">
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Họ Tên:</label>
                  <input type="text" class="form-control" name="fullname" placeholder="Họ Tên" required="" value="<?php if($s1=='add'){echo $fullname;}else{echo $row_users['fullname'];}?>">
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Email:</label>
                  <input type="email" class="form-control" name="email" placeholder="Email" required="" value="<?php if($s1=='add'){echo $email;}else{echo $row_users['email'];}?>">
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="password" placeholder="Password" required="" value="<?php if($s1=='add'){echo $password;}else{echo $row_users['password'];}?>" <?php if($s1!='add'){echo 'disabled';} ?>>
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Địa chỉ:</label>
                  <input type="text" class="form-control" name="address" placeholder="Địa chỉ" value="<?php if($s1=='add'){echo $address;}else{echo $row_users['address'];}?>">
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Số điện thoại:</label>
                  <input type="phone" class="form-control" name="phone_number" placeholder="Số điện thoại" pattern="[0-9]{10,11}" value="<?php if($s1=='add'){echo $phone_number;}else{echo $row_users['phone_number'];}?>">
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Giới tính:</label>
                  <select class="form-control col-md-4" name="gender">
                    <option value="0">- Giới tính -</option>
                    <?php
                      $arr_gender = array('1' => 'Nam', '2' => 'Nữ');

                      $act_gender='';

                      foreach ($arr_gender as $k_gender => $v_gender):

                        if($s1=='add'){ echo ($gender==$k_gender) ? $act_gender='selected' : $act_gender='';}
                          else{ echo ($row_users['gender']==$k_gender) ? $act_gender='selected' : $act_gender='';}

                        echo '<option value="'.$k_gender.'" '.$act_gender.'>'.$v_gender.'</option>';

                      endforeach;
                    ?>
                  </select>
                </div>

                <?php  
                  if($s1!='add')
                  {
                    $birthday=$row_users['birthday'];

                    $birthday=explode('-',$birthday);

                    $xn_ngay=$birthday[2];
                    $xn_thang=$birthday[1];
                    $xn_nam=$birthday[0];
                  }
                ?>

                <div class="form-group col-md-4">
                  <div class="row">
                    <div class="col-md-4">
                      <label for="exampleInputEmail1">Ngày:</label>
                      <select class="form-control col-md-4" name="ngay">
                        <option>Ngày</option>
                        <?php 
                          $act_ngay='';

                          for($i=1;$i<=31;$i++):

                            if($s1=='add'){ echo ($ngay==$i) ? $act_ngay='selected' : $act_ngay='';}
                              else{ echo ($xn_ngay==$i) ? $act_ngay='selected' : $act_ngay='';}

                            echo '<option value="'.$i.'" '.$act_ngay.'>'.$i.'</option>';
                          endfor;
                        ?>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="exampleInputEmail1">Tháng:</label>
                      <select class="form-control col-md-4" name="thang">
                        <option>Tháng</option>
                        <?php 
                          $act_thang='';

                          for($i=1;$i<=12;$i++):

                            if($s1=='add'){ echo ($thang==$i) ? $act_thang='selected' : $act_thang='';}
                              else{ echo ($xn_thang==$i) ? $act_thang='selected' : $act_thang='';}

                            echo '<option value="'.$i.'" '.$act_thang.'>'.$i.'</option>';
                          endfor;
                        ?>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="exampleInputEmail1">Năm sinh:</label>
                      <select class="form-control col-md-4" name="year">
                        <option>Năm</option>
                        <?php 
                          $act_nam='';

                          for($i=1930;$i<=2017;$i++):

                            if($s1=='add'){ echo ($nam==$i) ? $act_nam='selected' : $act_nam='';}
                              else{ echo ($xn_nam==$i) ? $act_nam='selected' : $act_nam='';}

                            echo '<option value="'.$i.'" '.$act_nam.'>'.$i.'</option>';
                          endfor;
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Số CMND:</label>
                  <input type="text" class="form-control" name="identify_card" placeholder="Số CMND" pattern="[0-9]{9}" value="<?php if($s1=='add'){echo $identify_card;}else{echo $row_users['identify_card'];}?>">
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Phân quyền:</label>
                  <select class="form-control col-md-4" name="role">
                    <option value="0">- Phân quyền -</option>
                    <?php
                      $act_role='';

                      $row_role=$this->M_users->row('role_user',array('user_id'=>$row_users['id']),NULL);

                      foreach ($role as $v_role):

                        if($s1=='add'){ echo ($xd_role==$v_role['id']) ? $act_role='selected' : $act_role='';}
                          else{ echo ($row_role['role_id']==$v_role['id']) ? $act_role='selected' : $act_role='';}

                        echo '<option value="'.$v_role['id'].'" '.$act_role.'>'.$v_role['name'].'</option>';
                      endforeach;
                    ?>
                  </select>
                </div>
                <div class="form-group col-md-2">
                  <label for="exampleInputEmail1">Kích hoạt:</label>
                  <select class="form-control col-md-4" name="status">
                    <?php
                      $status = array('1' => 'Có', '0' => 'Không');

                      $act_status='';

                      foreach ($status as $k_status => $v_status):

                         if($s1=='add'){ echo ($xd_status==$k_status) ? $act_status='selected' : $act_status='';}else{ echo ($row_users['status']==$k_status) ? $act_status='selected' : $act_status='';}

                        echo '<option value="'.$k_status.'" '.$act_status.'>'.$v_status.'</option>';
                      endforeach;
                    ?>
                  </select>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary"><?php if($s1=='add'){echo 'Thêm';}else{echo 'Cập nhật';} ?></button>
                <a href="<?php echo base_url('admin/users'); ?>"><button type="button" class="btn btn-default pull-right">Thoát</button></a>
              </div>
            </form>
          </div>
      </div>
      <div class="pad margin no-print">
        <div class="callout callout-warning" style="margin-bottom: 0!important;">
          <h4><i class="fa fa-warning"></i> Ghi chú:</h4>
          Số điện thoại: Nhập trong phạm vi 10 đến 11 số, không phải kí tự.<br>
          Số CMND: Nhập trong phạm vi 9 số, không phải kí tự.
        </div>
      </div>
    </section>
</div>