<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>public/assets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>16HCB</p>
        <a href="<?php echo base_url().'admin/edit/users/'.$row_session['id']; ?>"><i class="fa fa-circle text-success"></i> <?php echo $row_session['fullname']; ?></a>
      </div>
    </div>
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </span>
      </div>
    </form>
    <?php $s2=$this->uri->segment(2);$s1=$this->uri->segment(1); ?>
    <ul class="sidebar-menu">
      <li class="header">DANH MỤC CHÍNH</li>
      <li class="<?php if($s2==''){echo 'active';} ?>">
        <a href="<?php echo base_url('admin'); ?>">
          <i class="fa fa-dashboard"></i> <span>Bảng điều khiển</span>
        </a>
      </li>

      <li class="treeview <?php if($s1=='area'||$s1.'/'.$s2=='add/area'||$s1.'/'.$s2=='edit/area'){echo 'active';} ?>">
        <a href="javascript:;">
          <span class="glyphicon glyphicon-map-marker"></span>
          <span>Khu vực</span>
          <span class="pull-right-container">
            <span class="label label-primary pull-right">2</span>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($s1=='area'){echo 'active';} ?>">
            <a href="<?php echo base_url('admin/area'); ?>">
              <small><span class="glyphicon glyphicon-record"></span></small> Tất cả Khu vực
            </a>
          </li>
          <li class="<?php if($s1.'/'.$s2=='add/area'){echo 'active';} ?>">
            <a href="<?php echo base_url('admin/add/area'); ?>">
              <small><span class="glyphicon glyphicon-record"></span></small> Thêm Khu vực
            </a>
          </li>
        </ul>
      </li>
      <li class="header">DANH MỤC KHÁC</li>
      <li class="treeview <?php if($s1=='users'||$s1.'/'.$s2=='add/users'||$s1.'/'.$s2=='edit/users'){echo 'active';} ?>">
        <a href="javascript:;">
          <span class="glyphicon glyphicon-user"></span>
          <span>Thành viên</span>
          <span class="pull-right-container">
            <span class="label label-warning pull-right">2</span>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($s1=='users'){echo 'active';} ?>">
            <a href="<?php echo base_url('admin/users'); ?>">
              <small><span class="glyphicon glyphicon-record"></span></small> Tất cả Thành viên
            </a>
          </li>
          <li class="<?php if($s1.'/'.$s2=='add/users'){echo 'active';} ?>">
            <a href="<?php echo base_url('admin/add/users'); ?>">
              <small><span class="glyphicon glyphicon-record"></span></small> Thêm Thành viên
            </a>
          </li>
        </ul>
      </li>
      <li class="treeview <?php if($s1=='role'||$s1.'/'.$s2=='add/role'||$s1.'/'.$s2=='edit/role'){echo 'active';} ?>">
        <a href="javascript:;">
          <span class="glyphicon glyphicon-user"></span>
          <span>Phân quyền</span>
          <span class="pull-right-container">
            <span class="label label-danger pull-right">2</span>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($s1=='role'){echo 'active';} ?>">
            <a href="<?php echo base_url('admin/role'); ?>">
              <small><span class="glyphicon glyphicon-record"></span></small> Tất cả Phân quyền
            </a>
          </li>
          <li class="<?php if($s1.'/'.$s2=='add/role'){echo 'active';} ?>">
            <a href="<?php echo base_url('admin/add/role'); ?>">
              <small><span class="glyphicon glyphicon-record"></span></small> Thêm Phân quyền
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </section>
</aside>