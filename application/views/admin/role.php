<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Bảng phân quyền
      <small><a href="<?php echo base_url('admin/add/role'); ?>">
        <button type="button" class="btn btn-block btn-primary btn-sm">THÊM <span class="glyphicon glyphicon-plus"></span></button>
      </a></small>
    </h1>
  </section>
  <div class="pad margin no-print">
    <div class="callout callout-warning" style="margin-bottom: 0!important;">
      <h4><i class="fa fa-warning"></i> Ghi chú:</h4>
      Tìm kiếm theo tên phân quyền
    </div>
    <?php if($delete_success||$name_exist){ ?>
    <div class="alert alert-danger fade in alert-dismissable col-md-4" style="margin-top: 10px;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
      <strong>Thông báo!</strong> <?php if($delete_success){echo 'Đã xóa.';}elseif($name_exist){echo 'Tên Thành viên đã tồn tại.';} ?>
    </div>
    <?php }elseif($add_success||$status_success||$changeName_success){ ?>
    <div class="alert alert-success fade in alert-dismissable col-md-4" style="margin-top:10px;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close" style="text-decoration: none;">×</a>
      <strong>Thông báo!</strong> <?php if($add_success){echo 'Đã thêm.';}elseif($status_success){echo 'Đổi trạng thái thành công.';}elseif($changeName_success){echo 'Đổi tên thành công.';} ?>
    </div>
    <?php } ?>
  </div>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <form action="" method="get">
          <div class="box-header">
            <h3 class="box-title" style="text-transform: uppercase;"><small><span class="glyphicon glyphicon-th-list"></span></small> Danh sách phân quyền</h3>
            <div class="box-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="search" class="form-control pull-right" placeholder="Tìm kiếm.." value="<?php echo $this->input->get('search'); ?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>
                </div>
              </div>
            </div>
          </form>
          </div>
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover table-bordered">
              <tbody><tr>
                <th class="text-center" style="width: 100px;">STT</th>
                <th>Tên phân quyền</th>
                <th class="text-center" style="width: 150px;">Người tạo</th>
                <th class="text-center" style="width: 150px;">Ngày tạo</th>
                <th class="text-center" style="width: 150px;"><span class="glyphicon glyphicon-cog"></span></th>
              </tr>
              <?php  $i=0;
                foreach ($user as $k_user => $v_user): $i++; 
                  $row_session=$this->M_role->row('role',array('id'=>$v_user['created_by']),NULL);
              ?>
              <tr>
                <td class="text-center"><?php echo $i; ?></td>
                <td><?php echo $v_user['name'] ?>
                  <a href="#" data-toggle="modal" data-target="#change_name<?php echo $v_user['id'];?>" title="Đổi tên Thành viên"><span class="glyphicon glyphicon-edit pull-right"></span></a>
                </td>
                <td class="text-center"><?php echo $row_session['name']; ?></td>
                <td class="text-center"><?php echo $v_user['created_at']; ?></td>
                <td class="text-center">
                  <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url('admin/edit/role/'.$v_user['id']); ?>" title="Chỉnh sửa thành viên" style="color:#3c8dbc;">
                    <span class="glyphicon glyphicon-edit"></span>
                  </a> 
                  <a class="btn btn-circle btn-icon-only btn-default" href="#" style="color: #dd4b39;" data-toggle="modal" data-target="#delete_users<?php echo $v_user['id'];?>" title="Xóa thành viên">
                    <span class="glyphicon glyphicon-trash"></span>
                  </a>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody></table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php foreach ($user as $k_user => $v_user): ?>
<form action="" method="post">
  <input type="hidden" name="id" value="<?php echo $v_user['id'];?>">
  <div class="modal fade" id="delete_users<?php echo $v_user['id'];?>" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #5ac6de;color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
          <p>Bạn chắc chắn xóa phân quyền tên: <b style="color: #dd4b39;"><?php echo $v_user['name']; ?></b></p>
        </div>
        <div class="modal-footer">
          <input type="submit" name="delete" class="btn btn-danger" value="Xóa">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php endforeach; ?>

<?php foreach ($user as $k_user => $v_user): ?>
<form action="" method="post">
  <input type="hidden" name="id" value="<?php echo $v_user['id'];?>">
  <div class="modal fade" id="change_name<?php echo $v_user['id'];?>" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #5ac6de;color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
          <p><label>Đổi tên Phân quyền:</label>
            <input type="text" name="name" value="<?php echo $v_user['name']; ?>" class="form-control" required>
          </p>
        </div>
        <div class="modal-footer">
          <input type="submit" name="change_name" class="btn btn-primary" value="Đổi">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php endforeach; ?>