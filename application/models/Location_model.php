<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends MY_Model {

	private $table = 'location';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    /**
     * @author ntdat
     * get list id, name in location
     */
    public function getList() {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('parent_id', 0);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }

    /**
     * @author ntdat
     * get list location by category_id
     */
    public function getById($location_id)
    {
    	$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where('parent_id', $location_id);
		$result = $this->db->get();
		
		return $result->result();
    }


    // public function getById($id)
    // {
    //     $this->db->select("*");
    //     $this->db->from($this->table);
    //     $this->db->where('id', $id);
    //     $result = $this->db->get();
        
    //     return $result->result();
    // }


    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

    public function getAllArray() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result_array();
        
    }
    /* fix sau */
    public function listLocationLevel(){
        return $this->getItemlist_Recursion_level( $this->getAllArray(),[] );
    }

    public function getItemlist_Recursion_level($data = [], $return=[], $parent = 0 , $level = 0, $mode = 'child'){
        foreach( $data as $row ){
            if( $mode == 'child' ){
                if( $row['parent_id'] == $parent ){// kiem con
                    $row['level'] = $level;
                    $return[] =  $row;
                    $return = $this->getItemlist_Recursion_level( $data, $return, $row['id'], $level + 1 );
                }
            }elseif( $mode =='father' ){
                if( $row['id'] == $parent  ){
                    $row['level'] = $level;
                    $return[] =  $row;
                    $return = $this->getItemlist_Recursion_level( $data, $return, $row['parent_id'], $level + 1,'father' );
                }
            }
        }
        return $return;
    }

    /* test performing */
    public function findKeyInList($location_id,$listSearch){
        foreach( $listSearch as $key => $row ){
            if( $location_id == $row['data']->id ){
                return $key;
            }
        }
    }
}