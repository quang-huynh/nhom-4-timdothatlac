<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model {

	private $table = 'user';
    private $key = 'id';
    private $order = '';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }



    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->row();
    }


    public function getAll($input=array()) {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

    public function checkExists($where = array()) {
        $this->db->where($where);
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getByRules($rules = array())
    {
        $this->db->select("*");
        $this->db->where($rules);
        $query = $this->db->get($this->table);
        if ($query->num_rows()) {
            return $query->row();
        }

        return FALSE;
    }

    public function insert($data=array())
    {
        if ($this->db->insert($this->table, $data)) {
            $insert_id = $this->db->insert_id();
            if ($insert_id) {
                return $insert_id;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function update($id, $data = array()) {
        if (!$id) {
            return FALSE;
        }
        $where = array();
        $where[$this->key] = $id;

        if (!$where) {
            return FALSE;
        }
        $this->db->where($where);
        $this->db->update($this->table, $data);

        return TRUE;
    }

    public function delete($id = 0) {
        if (!$id) {
            return FALSE;
        }
        if (is_numeric($id)) {
            $where = array($this->key => $id);
        } else {
            $where = $this->key . " IN (" . $id . ") ";
        }
        
        if (!$where) {
            return FALSE;
        }
        $this->db->where($where);
        $this->db->delete($this->table);

        return TRUE;
    }

    function getTotal($input = array()) {
        $this->set_input($input);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    function getList($input = array()) {
        $this->set_input($input);

        $query = $this->db->get($this->table);
        //echo $this->db->last_query();
        return $query->result();
    }

    protected function set_input($input = array()) {
        if(isset($input['select']) && $input['select']) {
            $this->db->select($input['select']);
        }

        if (( isset($input['join']) ) && $input['join']) {
            foreach ($input['join'] as $join => $value_join) {
                $this->db->join($join, $value_join, 'left'); //type = 'left/inner'
            }
        }

        if ((isset($input['where'])) && $input['where']) {
            $this->db->where($input['where']);
        }

        if(isset($input['where_in'][0]) && $input['where_in'][1]){
            $this->db->where_in($input['where_in'][0], $input['where_in'][1]);
        }

        if ((isset($input['like'])) && $input['like']) {
            $this->db->like($input['like'][0], $input['like'][1]);
        }

        if (isset($input['order'][0]) && isset($input['order'][1])) {
            $this->db->order_by($input['order'][0], $input['order'][1]);
        } else {            
            $order = ($this->order == '') ? array($this->table . '.' . $this->key, 'desc') : $this->order;
            $this->db->order_by($order[0], $order[1]);
        }
        
        if (isset($input['limit'][0]) && isset($input['limit'][1])) {
            $this->db->limit($input['limit'][0], $input['limit'][1]);
        }
    }

}