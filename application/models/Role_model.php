<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends MY_Model {

	var $table = 'role';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getAll($input=array()) {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

    public function insert($data=array())
    {
        if ($this->db->insert($this->table, $data)) {
            $insert_id = $this->db->insert_id();
            if ($insert_id) {
                return $insert_id;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function insertUserRole($data=array())
    {
        if ($this->db->insert("role_user", $data)) {
            $insert_id = $this->db->insert_id();
            if ($insert_id) {
                return $insert_id;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function updateUserRole($where = array(), $data = array()) {
        if (!$where) {
            return FALSE;
        }
        $this->db->where($where);
        $this->db->update("role_user", $data);

        return TRUE;
    }

    public function delete($user_id = 0)
    {
        if (!$user_id) {
            return FALSE;
        }
        if (is_numeric($user_id)) {
            $where = array('user_id' => $user_id);
        } else {
            $where = "user_id  IN (" . $user_id . ") ";
        }
        
        if (!$where) {
            return FALSE;
        }
        $this->db->where($where);
        $this->db->delete("role_user");

        return TRUE;
    }

    public function getRoleByUserId($user_id = 0)
    {
		if (!$user_id) {
            return FALSE;
        }

        $this->db->select("role_id");
        $this->db->from("role_user");
        $this->db->where('user_id', $user_id);
        $result = $this->db->get();
        
        return $result->row();
    }
}