<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matched_Item_model extends MY_Model {

	private $table = 'matched_item';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->result();
    }


    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

    # fix sau
    public function insertRow($data = array())
    {
    	$this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id; 
    }

    public function insertArray($data = array())
    {
        if( !empty($data[0]) && is_array($data[0]) ){
            $this->db->insert_batch( $this->table , $data);
        }
        else{
            return -1;
        }
    }

    function getDataReportItemMatched() {
        $this->db->select("created_at, count(*) as count");
        $this->db->order_by('created_at','desc');
        $this->db->group_by("created_at");
        $query = $this->db->get($this->table); 
        $data = null;
        foreach ($query->result() as $row) {
            $row->created_at = date('m/d/Y', strtotime($row->created_at));
            $data[] = array(
                'x' => $row->created_at,
                'y' => $row->count
            );
        }
        return $data;
    }


    function getItemByFoundAndLost($idfound,$idlost){
        $this->db->select("item_lost_id,item_found_id,matched_date,status,percent");
        $this->db->from($this->table);
        $this->db->where('item_found_id', $idfound);
        $this->db->where('item_lost_id', $idlost);
        $result = $this->db->get()->result_array();
        if( isset($result[0]['percent']) ){
            return $result[0]['percent'];
        }
        return 0 ;
    }

    function getDataReportTotalMatched() {
        $this->db->select("created_at,id, count(*) as count");        
        $this->db->from($this->table);
        $this->db->order_by('created_at','desc');
        $this->db->group_by("created_at, id");
        $query = $this->db->get(); 
    
        return $query->num_rows();
    }

    function getByItemFoundID($id){
        $this->db->select("item_lost_id,item_found_id,matched_date,status,percent");
        $this->db->from($this->table);
        $this->db->where('item_found_id', $id);
        $result = $this->db->get();
        
        return $result->result_array();
    }
}