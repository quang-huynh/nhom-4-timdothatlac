<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_property_model extends MY_Model {

	private $table = 'item_property';

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function insert_item($item_id, $properties)
    {
        $date = date('Y-m-d H:i:s');
        foreach ($properties as $property) {
            $this->db->insert($this->table, array(
                'item_id'       =>  $item_id,
                'property_id'   =>  $property['id'],
                'value'         =>  $property['value'],
                'created_at'    =>  $date,
                'updated_at'    =>  $date
            ));
        }
    }


    /**
     * @author ntdat
     * add item-property into database
     */
    public function add($data, $itemID) {

        foreach($data as $item) {
            $item = array(
                'item_id' => $itemID,
                'property_id' => $item['id'],
                'value' => $item['value'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $result = $this->db->insert($this->table, $item);
        }
        return $result;     
    }

    public function delete($itemID) {
        return $this->db->delete($this->table, array('item_id' => $itemID)); 
    }


    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->result();
    }


    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

    public function getItemProperty($idItem,$limit = 0){
        
        return $this->db->select('value, property_id')->where('item_id',$idItem)->limit($limit)
        ->get($this->table)->result_array();
    }
    
    
}