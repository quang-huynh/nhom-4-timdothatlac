<?php
class Worker_model extends MY_Model {
    private $table = 'worker';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getWorker(){
        return $this->db->where('status !=',2)->limit(1)->order_by('stt','asc')
        ->get($this->table)->result_array();
    }
    
    public function getAll($options = []){
        if( !empty($options['select']) ){
            $this->db->select($options['select']);
        }
        else{
            $this->db->select('*');
        }
        if( !empty($options['where']) && is_array( $options['where'] ) ){
            $this->db->where($options['where']);
        }
        return $this->db->get($this->table)->result_array();
    }

    public function updateWorker($array,$field,$id){
        if(empty($array) ) return false;
        $this->db->set($array); $this->db->where($field,$id);
        return $this->db->update($this->table);
    }
}