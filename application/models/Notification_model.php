<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends MY_Model {

	private $table = 'notification';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function insertRow($data = array())
    {
    	$this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id; 
    }
    
    public function insertArray($data = array())
    {
        if( !empty($data[0]) && is_array($data[0]) ){
            $this->db->insert_batch( $this->table , $data);
        }
        else{
            return -1;
        }
    }
    
    // Fetch data according to per_page limit.
    public function fetch_data($limit, $start) {
        $offset = ($start-1)*$limit;
        $this->db->limit($limit, $offset);
        $this->db->order_by("created_at", "desc");
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            
            return $data;
        }

        return false;
    }
     
    public function get_total() {
        return $this->db->count_all($this->table);
    }

    public function getAll(){
        $this->db->select("*");
        $this->db->order_by("created_at", "desc");
        $this->db->from($this->table);
        // $this->db->join('user','user.id = notification.user_id','inner');
        // $this->db->where('user_id', $user_id);
        $result = $this->db->get();
        
        return $result->result();
    }

    public function getAllByStatus(){
        $this->db->select("*");
        $this->db->order_by("created_at", "desc");
        $this->db->limit(20);
        $this->db->from($this->table);
        // $this->db->join('user','user.id = notification.user_id','inner');
        $this->db->where('status', 'unread');
        $result = $this->db->get();
        
        return $result->result();
    }

    public function searchQuery($dateStart, $dateEnd)
    {
        $this->db->select("*");
        $this->db->order_by("created_at", "desc");
        $this->db->from($this->table);
        // $this->db->join('user','user.id = notification.user_id','inner');
        $this->db->where("created_at BETWEEN '$dateStart' AND '$dateEnd'");
        $result = $this->db->get();
        
        return $result->result();
    }

}