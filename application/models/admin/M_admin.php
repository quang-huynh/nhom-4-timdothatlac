<?php
	class M_admin extends MY_Model
	{
		public function row($table,$data,$arrange)
		{
			if($data!=NULL)
			{
				$this->db->where($data);
			}

			if($arrange!=NULL)
			{
				$this->db->order_by($arrange,'DESC');
			}

			$query=$this->db->get($table);

			return $query->row_array();
		}

		public function result($table)
		{
			$query=$this->db->get($table);

			return $query->result_array();
		}
	}
