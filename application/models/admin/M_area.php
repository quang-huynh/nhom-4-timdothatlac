<?php
	class M_area extends MY_Model
	{
		public function result($table,$data,$like)
		{
			$this->db->order_by('ID','ASC');

			if($data!=NULL)
			{
				$this->db->where($data);
			}

			if($like!=NULL)
			{
				$this->db->like('name',$like);
			}

			$query=$this->db->get($table);

			return $query->result_array();
		}

		public function row($table,$data,$arrange)
		{
			if($data!=NULL)
			{
				$this->db->where($data);
			}

			if($arrange!=NULL)
			{
				$this->db->order_by($arrange,'DESC');
			}

			$query=$this->db->get($table);

			return $query->row_array();
		}

		public function add($table,$data)
		{
			$this->db->insert($table,$data);
		}

		public function update($table,$data,$update)
		{
			$this->db->update($table,$data,$update);
		}

		public function delete($table,$data)
		{
			$this->db->delete($table,$data);
		}
	}
