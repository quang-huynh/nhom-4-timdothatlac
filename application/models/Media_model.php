<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media_model extends MY_Model {

	var $table = 'media';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @author ntdat
     * add path image item found into database
     */
    public function add($path, $itemID) {
        $item = array(
            'path' => $path,
            'item_id' => $itemID,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1,
            'updated_at' => date('Y-m-d H:i:s')
        );

        return $this->db->insert($this->table, $item);
    }

    public function delete($itemID)
    {
        return $this->db->delete($this->table, array('item_id'=>$itemID));
    }

    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->result();
    }

    public function getByItemid($itemId)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('item_id', $itemId);
        $result = $this->db->get();
        
        return $result->result();
    }


    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

}