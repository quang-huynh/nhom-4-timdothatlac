<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_Action_model extends MY_Model {

	private $table = 'item_action';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->result();
    }


    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

    function getDataReportItemAction() {
        $this->db->select("date_action, item_id, count(*) as count");
        $this->db->order_by('date_action','desc');
        $this->db->group_by("date_action, item_id");
        $query = $this->db->get($this->table); 
        $data = null;
        foreach ($query->result() as $row) {
            $row->date_action = date('m/d/Y', strtotime($row->date_action));
            $data[] = array(
                'x' => $row->date_action,
                'y' => $row->count
            );
        }
        return $data;
    }

    function getDataReportTotalAction() {
        $this->db->select("date_action,item_id, count(*) as count");        
        $this->db->from($this->table);
        $this->db->order_by('date_action','desc');
        $this->db->group_by("date_action, item_id");
        $query = $this->db->get(); 
    
        return $query->num_rows();
    }

}