<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Item_model extends MY_Model {

    private $table = 'item';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


    /**
     * @author ntdat
     * add item found into database
     */
    public function addItemFound($data) {

        $location = $data['item']['location'];
        $item_location = null;
        for ($i = count($location) -1 ; $i >= 0; $i--) {
            if(empty($location[$i])){
                continue;
            } else {
                $item_location = $location[$i];
                break;
            }
        }
        
        $item = array(
            'category_id' => $data['item']['customCategory'],
            'location_id' => $item_location,
            'storage_location_id' => $data['item']['storage_location_id'],
            'detail' => $data['item']['description'],
            'type' => unserialize(TYPE_ITEM)['found'],
            'status' => unserialize(STATUS_ITEM)['open'],
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $this->db->insert($this->table, $item);
        return $this->db->insert_id();
    }


    public function insert($data = array())
    {
    	$this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id; 
    }

    public function updateItem($id, $data = array())
    {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    public function deleteItem($id){
        return $this->db->delete($this->table, array('id' => $id));
    }

    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->result();
    }

    public function itemDetail($id)
    {
         $this->db->select("item.*, category.name as category_name, location.name as location_name, GROUP_CONCAT(property.name) as property_name");
        $this->db->from($this->table);
        $this->db->where('item.id', $id);
        $this->db->join('category', 'item.category_id = category.id', 'left');
        $this->db->join('location', 'item.location_id = location.id', 'left');
        $this->db->join('item_property', 'item.id = item_property.item_id', 'inner');
        $this->db->join('property', 'item_property.property_id = property.id', 'inner');
        $this->db->group_by('item.id');
        $result = $this->db->get();

        return $result->result();
    }

    public function getMatches($mixed = array(),$limit = 5)
    {
        // LAM fix order by percent , search , status , 
        $where = [];
        if($this->input->get('submit') == 'submit'){
            $locations = $this->getLocation();
            $locations = $this->getItemlist_Recursion_echo($locations,[],0 );
            $categoriesIn = $this->input->get('category');
            $locationsIn = $this->input->get('locations');
            $dateRangeStart = $this->input->get('dateRangeStart');
            $dateRangeEnd = $this->input->get('dateRangeEnd');
            $where= [];
            if(!empty($categoriesIn)){
                $where['item.category_id'] = $categoriesIn;
            }
            if(!empty($locationsIn)){
                $arrLocation_tmp = [];
                foreach( $locationsIn as $value ){
                    if(empty($arrLocation_tmp)){
                        $arrLocation_tmp=  $this->getItemlist_Recursion_Location($locations, $locationsIn, $value );
                    }else {
                        $arrLocation_tmp=  $this->getItemlist_Recursion_Location($locations, $arrLocation_tmp, $value );
                    }
                }
                $where['item.location_id'] =  $arrLocation_tmp;
            }
            if( strtotime($dateRangeEnd) !== false ){
                $where['item.created_at <=' ] = date('Y-m-d H:i:s',strtotime($dateRangeEnd));
            }
            if( strtotime($dateRangeStart) !== false ){
                $where['item.created_at >= '] = date('Y-m-d H:i:s',strtotime($dateRangeStart));
            }
        }
        if( !empty($mixed) ) {
            switch ($mixed['type']) {
                case 'lost':
                    $this->db->select("item.*,matched_item.percent as percent, category.name as category_name, location.name as location_name, GROUP_CONCAT(CONCAT(property.name, '||' ,item_property.value) SEPARATOR '::') as property_name");
                    $this->db->from($this->table);
                    $this->db->where('item.type', '2');
                    if( $mixed['status'] ==  unserialize(STATUS_ITEM)['matched'] ){
                        $this->db->where('item.status',unserialize(STATUS_ITEM)['matched']);
                    }
                    if( !empty($this->input->get('keys'))  ){
                        $key = $this->input->get('keys');
                        $this->db->group_start();            
                        $this->db->like('item.email',$key); $db->or_like('item.fullname',$key);
                        $this->db->or_like('item.phone_number',$key); $db->or_like('category.name',$key);
                        $this->db->group_end();            
                    }
                    foreach($where as $key => $row){
                        if(is_array($row)){
                            $this->db->where_in($key,$row);
                        }else{
                            $this->db->where($key,$row);
                        }
                    }
                    $this->db->where('matched_item.item_lost_id',  $mixed['id']);
                    //$this->db->where_not_in('item.id', $mixed['id']);
                    $this->db->order_by('matched_item.percent', 'DESC');
                    $this->db->join('category', 'item.category_id = category.id', 'left');
                    $this->db->join('location', 'item.location_id = location.id', 'left');
                    $this->db->join('item_property', 'item.id = item_property.item_id', 'left');
                    $this->db->join('property', 'item_property.property_id = property.id', 'left');
                    $this->db->join('matched_item', 'item.id = matched_item.item_found_id', 'left');
                    $this->db->group_by('item.id');
                    
                    $this->db->limit($limit);
                    $result = $this->db->get();
                    break;
                case 'found':
                default:
                    $this->db->select("item.*,matched_item.percent as percent, category.name as category_name, location.name as location_name, GROUP_CONCAT(CONCAT(property.name, '||' ,item_property.value) SEPARATOR '::') as property_name");
                    $this->db->from($this->table);
                    $this->db->where('item.type', '1');
                    if( $mixed['status'] ==  unserialize(STATUS_ITEM)['matched'] ){
                        $this->db->where('item.status',unserialize(STATUS_ITEM)['matched']);
                    }
                    if( !empty($this->input->get('keys'))  ){
                        $key = $this->input->get('keys');
                        $this->db->group_start();            
                        $this->db->like('item.email',$key); $this->db->or_like('item.fullname',$key);
                        $this->db->or_like('item.phone_number',$key); $this->db->or_like('category.name',$key);
                        $this->db->group_end();            
                    }
                    foreach($where as $key => $row){
                        if(is_array($row)){
                            $this->db->where_in($key,$row);
                        }else{
                            $this->db->where($key,$row);
                        }
                    }
                    $this->db->where('matched_item.item_found_id',  $mixed['id']);
                    //$this->db->where_not_in('item.id', $mixed['id']);
                    $this->db->order_by('matched_item.percent', 'DESC');
                    $this->db->join('category', 'item.category_id = category.id', 'left');
                    $this->db->join('location', 'item.location_id = location.id', 'left');
                    $this->db->join('item_property', 'item.id = item_property.item_id', 'left');
                    $this->db->join('property', 'item_property.property_id = property.id', 'left');
                    $this->db->join('matched_item', 'item.id = matched_item.item_lost_id', 'left');
                    $this->db->group_by('item.id');
                    
                    $this->db->limit($limit);
                    $result = $this->db->get();
                    break;
            }
            return $result->result();
        }

        return "";
    }

    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
    }


    
    public function updateArray($array,$field,$id){
        if(empty($array) ) return false;
        $this->db->set($array); $this->db->where($field,$id);
        return $this->db->update($this->table);

    }    
    

    public function getItemMatched($itemID = 0 , $type , $status ){
        if( empty( $itemID ) || empty( $type ) || empty( $status ) ){
            return [];
        }else{
            return $this->db->where('item_'. array_search($type,unserialize(TYPE_ITEM))  .'_id' , $itemID )
            ->where('status' , $status)->get('matched_item')->result_array();
        }
    }
    public function getCategory($id=0){
        if(!empty($id)) $this->db->where('id',$id);
        return $this->db->get('category')->result_array();
    }
    
    public function getLocation($id=0){
        if(!empty($id)) $this->db->where('id',$id);
        return $this->db->get('location')->result_array();
    }

    public function getItem_Worker(){
        return $this->db->select('*')->where('type', unserialize(TYPE_ITEM)['found'] )->where('status !=', unserialize(STATUS_ITEM)['matched'])->get($this->table)->result_array();
    }

    public function getAllItemForMatch($idItem = 0 , $category_id = 0){
        $this->db->select('item.id as id, location_id, item.type as type')->from( $this->table );
        if(!empty($idItem)) $db->where('id !=',$idItem);
        if(!empty($category_id)) $db->join('category','category.id = ' . $category_id ,'inner');
        return $db->get()->result_array(); # gom chung the loai;
        #->join('item_property','item_property.item_id = item.id','inner') # xem item nay co bao nhieu property
        #->join('property','item_property.property_id = property.id','inner')
    }

    public function getProperty($idItem,$limit = 5){
        return $this->db->select('item_property.value as value, property_id, property.name as name')
        ->where('item_id',$idItem)
        ->join('property','item_property.property_id = property.id','left')
        ->limit($limit)->from('item_property')->get()->result_array();
    }
    
    public function filterTagNumber($currentItem){
        return $this->db->select('id,type,category_id')
        ->where('status !=', unserialize(STATUS_ITEM)['matched'] )
        ->where('id !=',$currentItem->id )
        ->where('tag_number', $currentItem->tag_number )
        ->where('type !=',$currentItem->type)->get($this->table)->result_array();
    }

    public function filterCategory($currentItem){
        return $this->db->where('id !=',$currentItem->id )
        ->where('type !=',$currentItem->type)
        ->where('category_id',$currentItem->category_id )
        ->where('status !=', unserialize(STATUS_ITEM)['matched'] )
        ->get($this->table)->result_array();
    }

    public function getItemlist($type,$perpage=0,&$totalrows,$currentpage,&$title){
        
        $locations = $this->getLocation();
        $locations = $this->getItemlist_Recursion_echo($locations,[],0 );
        
        $start = ($currentpage-1)*$perpage; $where = [];
        switch($type){ // security access from url
            case 'lost':
                $title = 'Danh sách báo mất';
                break;
            case 'found':
                $title = 'Danh sách tìm thấy';
                break;
            case 'filter':
                $title = "Lọc báo mất và tìm thấy";
                break;
            case 'search':
                if(empty( $this->input->get('keys'))) {
                    $title = ' Không tìm thấy keys ';
                    return ['data'=>[],'locations'=>[]];
                }
                $title = 'Danh sách tìm kiếm ' . $this->input->get('keys');
                break;
            case 'matched':
                $title = 'Danh sách trùng khớp';
                break;
            case '':
                $title = 'Báo mất và tìm thấy';
                break;
            default:
                $title = 'Không thấy url';
                return ['data'=>[],'locations'=>[]];
        }
        $role = $this->getRoleById( $this->session->userdata('login')['role_id'] ); 
        if( $perpage != 0 ){
            $query = $this->db->select('COUNT(item.id) as dem')->from('item');
            $this->getItemlist_Where($type,$query , $role[0]);
            $this->getItemlist_Filter($query,$locations);
            $counts = $query->get()->result_array();
            $totalrows = isset($counts[0]['dem'])? $counts[0]['dem']:0 ;
            
        }
        
        
        $results = $this->db->select('item.id as ma,item.phone_number as sdt, item.status as status, item.created_at as created_at, item.fullname as fullname , item.email as email, location.name as place, category.name as category, item.type as type')->from('item');
        $this->getItemlist_Where($type,$results, $role[0]);
        $this->getItemlist_Filter($results,$locations);
        if( $perpage != 0 ){
            $results->limit($perpage,$start);
        }
        $return =  $results->order_by('item.id','desc')->get()->result_array();
        #echo $str = $this->db->last_query(); die;
        return ['data'=> $return, 'locations'=>$locations];
    }

    public function getRoleById($id = 0){
        
        $this->db->where( 'id',$id );
        return $this->db->get('role')->result_array();
    }

    public function getItemlist_Where($type,&$db, $role){
        
        // role_id phân quyền user list_item
        if( isset($role['name']) && isset($this->session->userdata('login')['_iduser']) ){
            if(  $role['name'] != 'Admin' && $role['name'] != 'Staff' ) {
                $db->where('item.created_by', $this->session->userdata('login')['_iduser'] );
            }
        }else{
            // redirect( base_url('error') );
            die('chưa biết user nào {LAM} ') ;
        }
        // end phân quyền user

        if( $type == 'matched' ){
            $db->where('item.status', unserialize(STATUS_ITEM)['matched']);
        }else{
            // ko lấy item đã được matched
            $db->where('item.status !=', unserialize(STATUS_ITEM)['matched']);
        }
        if($type == 'lost' || $type == 'found'){
            $where['type'] = isset(unserialize(TYPE_ITEM)[$type])?  unserialize(TYPE_ITEM)[$type]:'';
            $db->where($where);
        }elseif($type == 'search'){
            $key = $this->input->get('keys');
            $db->group_start();            
            $db->like('item.email',$key); $db->or_like('item.fullname',$key);
            $db->or_like('item.phone_number',$key); $db->or_like('category.name',$key);
            $db->group_end();            
        }
    }
    public function getItemlist_Filter(&$db,$locations = []){
        if($this->input->get('submit') == 'submit'){
            $categoriesIn = $this->input->get('category');
            $locationsIn = $this->input->get('locations');
            $dateRangeStart = $this->input->get('dateRangeStart');
            $dateRangeEnd = $this->input->get('dateRangeEnd');
            $where= [];
            if(!empty($categoriesIn)){
                $where['item.category_id'] = $categoriesIn;
            }
            if(!empty($locationsIn)){
                $arrLocation_tmp = [];
                foreach( $locationsIn as $value ){
                    if(empty($arrLocation_tmp)){
                        $arrLocation_tmp=  $this->getItemlist_Recursion_Location($locations, $locationsIn, $value );
                    }else {
                        $arrLocation_tmp=  $this->getItemlist_Recursion_Location($locations, $arrLocation_tmp, $value );
                    }
                }
                $where['item.location_id'] =  $arrLocation_tmp;
            }
            if( strtotime($dateRangeEnd) !== false ){
                $where['item.created_at <=' ] = date('Y-m-d H:i:s',strtotime($dateRangeEnd));
            }
            if( strtotime($dateRangeStart) !== false ){
                $where['item.created_at >= '] = date('Y-m-d H:i:s',strtotime($dateRangeStart));
            }
    
            foreach($where as $key => $row){
                if(is_array($row)){
                    $db->where_in($key,$row);
                }else{
                    $db->where($key,$row);
                }
            }
        }

        $db->join('location','item.location_id = location.id','inner')
        // ->join('storage_location','item.storage_location_id = storage_location.id','left')
        #->join('storage_location','item.storage_location_id = storage_location.id','left')
        #->join('user','user.id = item.found_by','left')
        ->join('category','category.id = item.category_id','inner')
        #->join('item_property','item_property.item_id = item.id','inner')
        #->join('property','item_property.property_id = property.id','inner')
        ;
        #return $db;
    }
    public function getItemlist_Recursion_Location($data = [], $return = [] , $current_id = 0){
        if(empty($data)) return;
        foreach( $data as $row ){
            if( $row['parent_id'] == $current_id ){
                if( !in_array( $row['id'],$return ) ){
                    $return[] = $row['id'];
                }else continue;
                $return = $this->getItemlist_Recursion_Location($data,$return,$row['id']);
            }
        }
        return $return;
    }
    public function getItemlist_Recursion_echo($data = [], $return=[], $parent = 0 , $id = 0){
        foreach( $data as $row ){
            if( $row['parent_id']== $parent ){
                $str = '';
                for($i = 0 ; $i<$id ; $i++) $str .= '&nbsp&nbsp';
                $row['name'] = $str . $row['name'];
                $return[] = $row;
                $return = $this->getItemlist_Recursion_echo( $data, $return, $row['id'], $id + 1 );
            }
        }
        return $return;
    }
    public function pagesLink($arrUrl, $totalrow, $currentpage = 1, $perpage = 50) {
        $baseURL= $arrUrl['base_url']; $queryGet = empty($arrUrl['queryGet'])? '':'?'.$arrUrl['queryGet'] ;
        if($totalrow <= 0) return 0;
        $totalpage = ceil($totalrow / $perpage);
        if($totalpage <= 1) return 0;
        $firstLink = "";
        $prevLink = "";
        $lastLink = "";
        $nextLink = "";
        if($currentpage > 1) {
            $firstLink = "$baseURL$queryGet";
            $prevPage = $currentpage - 1;
            if($prevPage != 1){
                $prevLink = "$baseURL/page-$prevPage/$queryGet";
            }else $prevLink = "$baseURL$queryGet";
        }
        if($currentpage < $totalpage) {
            $lastLink = "$baseURL/page-$totalpage/$queryGet";
            $nextPage = $currentpage + 1;
            $nextLink = "$baseURL/page-$nextPage/$queryGet";
        }
        $return['firstLink'] = $firstLink;
        $return['prevLink'] = $prevLink;
        $return['nextLink'] = $nextLink;
        $return['lastLink'] = $lastLink;
        return $return;
    }
    public function pagesList($arrUrl, $totalrow, $currentpage = 1, $perpage = 50, $offset = 2) {
        
        $baseURL= $arrUrl['base_url']; $queryGet = empty($arrUrl['queryGet'])? '':'?'.$arrUrl['queryGet'] ;
        if($totalrow <= 0) return 0;
        $totalpage = ceil($totalrow / $perpage);
        
        if($totalpage <= 1) return 0;
        $from = $currentpage - $offset;
        $to = $currentpage + $offset;
        
        if($from <= 0) {
            $from = 1;
            $to = $offset * 2;
        }
        if($to > $totalpage) {
            $to = $totalpage;
            #$from = $totalpage - $offset;
            #$from = ($from<=0)? 1:$from;
        }
        
        $links = "";
        for ($j = $from; $j <= $to; $j++) {
            if($j == $currentpage) $return[$j] = "";
            elseif($j==1) $return[$j] = $baseURL.$queryGet;
            else  $return[$j] = "$baseURL/page-$j/$queryGet";
        } //for
        return $return;
    }   

    function getDataReportItemLost() {
        $this->db->select("created_at, count(*) as count");
        $this->db->where('type', 0 );
        $this->db->order_by('created_at','desc');
        $this->db->group_by("created_at");
        $query = $this->db->get($this->table); 
        $data = null;
        foreach ($query->result() as $row) {
            $row->created_at = date('m/d/Y', strtotime($row->created_at));
            $data[] = array(
                'x' => $row->created_at,
                'y' => $row->count
            );
        }
    return $data;
    }

    function getDataReportItemFound() {
        $this->db->select("created_at, count(*) as count");        
        $this->db->from($this->table);
        $this->db->where('type', '1');
        $this->db->order_by('created_at','desc');
        $this->db->group_by("created_at");
        $query = $this->db->get(); 
        $data = null;
        foreach ($query->result() as $row) {
            $row->created_at = date('m/d/Y', strtotime($row->created_at));
            $data[] = array(
                'x' => $row->created_at,
                'y' => $row->count
            );
        }
    
    return $data;
    }

    function getDataReportTotalFound() {
        $this->db->select("created_at,id, count(*) as count");        
        $this->db->from($this->table);
        $this->db->where('type', '1');
        $this->db->order_by('created_at','desc');
        $this->db->group_by("created_at, id");
        $query = $this->db->get(); 
    
        return $query->num_rows();
    }

    function getDataReportTotalLost() {
        $this->db->select("created_at,id, count(*) as count");        
        $this->db->from($this->table);
        $this->db->where('type', '0');
        $this->db->order_by('created_at','desc');
        $this->db->group_by("created_at, id");
        $query = $this->db->get(); 
    
        return $query->num_rows();
    }
}