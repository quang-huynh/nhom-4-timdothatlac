<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property_model extends MY_Model {

	private $table = 'property';

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


     /**
     * @author ntdat
     * get list properties by category_id
     */

    public function getByCategoryId($category_id)
    {
    	$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where('category_id', $category_id);
		$result = $this->db->get();
		
		return $result->result();
    }


    /**
     * @author ntdat
     * update properties by list id
     */
    public function updateByCategoryId($data)
    {
        foreach($data as $item) {
            $property = array(
                'description' => $item['description'],
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', $item['id']);
            $result = $this->db->update($this->table, $property);
        }        
        return $result;
    }


    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->result();
    }


    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

}