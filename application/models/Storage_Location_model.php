<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Storage_Location_model extends MY_Model {

	var $table = 'storage_location';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @author ntdat
     * get list id, name in categories
     */
    public function getList() {
        $this->db->select("id, name");
        $query = $this->db->get($this->table);
        $data = $query->result();
        return $data;
    }


    public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $result = $this->db->get();
        
        return $result->result();
    }


    public function getAll() {
        
        $this->db->select("*");
        $this->db->from($this->table);
        $result = $this->db->get();
        
        return $result->result();
        
    }

}