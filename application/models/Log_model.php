<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_model extends MY_Model {

	private $table = 'log';
    
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function ghilog($content){
        $data = ['content' => $content , 'date_log' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ];
		return $this->insert($data);
    }

    public function insert($array = [] ) {
        if(empty($array)) return false;
        $this->db->insert($this->table, $array);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
}