<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missingpage_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['admin/login'] = 'admin/C_login/index';
$route['admin/logout'] = 'admin/C_login/logout';
$route['admin'] = 'admin/C_admin/index';

// Điều hướng users
$route['admin/users']='admin/C_users/index';
$route['admin/add/users']='admin/C_users/add_vs_edit';
$route['admin/edit/users/(:num)']='admin/C_users/add_vs_edit/$1';

// Điều hướng role
$route['admin/role']='admin/C_role/index';
$route['admin/add/role']='admin/C_role/add_vs_edit';
$route['admin/edit/role/(:num)']='admin/C_role/add_vs_edit/$1';

// Điều hướng area
$route['admin/area']='admin/C_area/index';
$route['admin/add/area']='admin/C_area/add_vs_edit';
$route['admin/edit/area/(:num)']='admin/C_area/add_vs_edit/$1';

$route['404'] = 'admin/C_404/index';

$route['default_controller'] = 'dashboard';
$route['translate_uri_dashes'] = FALSE;

$route['bao-mat-do-vat'] = 'ItemController/lost';
$route['bao-tim-thay-do-vat'] = 'ItemController/found';
$route['thong-bao'] = 'NotificationController/notification';
$route['thong-bao/:num'] = 'NotificationController/notification/$1';
$route['thong-bao-tim-theo-ngay'] = 'NotificationController/search';
$route['thong-ke'] = 'ReportController/report';
$route['xoa-cache'] = 'ReportController/clearCache';


$route['danh-sach-do-vat/(:any)/page-(:num)'] = "ItemController/item_list/$1/$2";
$route['danh-sach-do-vat/page-(:num)'] = "ItemController/item_list//$1";  
$route['danh-sach-do-vat/(:any)'] = "ItemController/item_list/$1";
$route['danh-sach-do-vat'] = "ItemController/item_list/";
$route['tim-kiem/page-(:num)'] = 'ItemController/item_list/tim-kiem/$1';

$route['tim-kiem'] = 'ItemController/item_list/tim-kiem/';

$route['chi-tiet/edit-lost-item-details-(:num).html'] = 'ItemController/editItem/$1';
$route['chi-tiet/bo-loc/(:num)'] = 'ItemController/itemDetailFilter/$1';
$route['chi-tiet/(:num)/so-sanh/(:num)'] = 'ItemController/itemDetailMatches/$1/$2';
$route['chi-tiet/(:num)'] = 'ItemController/itemDetail/$1';

$route['error'] = 'HomeController/error/';
$route['404_override'] = 'HomeController/error';
//User
$route['user/register'] = 'UserController/register';
//Auth
$route['user/login'] = 'AuthController/login';
$route['user/logout'] = 'AuthController/logout';
