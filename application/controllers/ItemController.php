<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ItemController extends CI_Controller 
{
	private $login = null;
	public function __construct()
	{
		parent::__construct();
		// always call this to check login in construct function

		$this->load->helper('url');
		$this->load->model('item_model', 'itemModel');

		$this->load->model('category_model', 'categoryModel');
		$this->load->model('location_model', 'locationModel');
		$this->load->model('storage_location_model', 'storageLocationModel');
		$this->load->model('item_property_model', 'itemPropertyModel');
		$this->load->model('media_model', 'mediaModel');
		$this->load->model('log_model', 'logModel');
		$this->load->library('session');
		$this->load->helper('form');

		if( empty($this->session->userdata('login')) && $this->router->fetch_method() != 'worker' ){
			redirect(site_url('user/login'));
		} else {
			$this->login = $this->session->userdata('login');
		}
	}

	public function lost()
	{
		$self = $this->input->server('REQUEST_URI');
		if( $this->input->server('REQUEST_METHOD') == 'POST') {
			$data = $this->input->post();
			if( !empty($data) ) {
				date_default_timezone_set('Asia/Ho_Chi_Minh');
				
				$temp = array(
					'type'					=>	1,
					'fullname'				=>	$data['item']['owner']['firstName'] . " " . $data['item']['owner']['lastName'],
					'email'					=>	$data['item']['owner']['email'],
					'phone_number'			=>	$data['item']['owner']['contactTel'],
					'found_by'				=>	NULL,
					'tag_number'			=>	$data['item']['tag_number'],
					'detail'				=>	$data['item']['description'],
					'status'				=>	unserialize(TYPE_ITEM)['lost'],
					'storage_location_id'	=>	0,
					'category_id'			=>	$data['item']['customCategory'],
					'location_id'			=>	$data['item']['location'],
					'created_at'			=>	date('Y-m-d H:i:s'),
					'created_by'			=>	$this->login['_iduser'],
					'updated_at'			=>	date('Y-m-d H:i:s')
				);


				/*echo '<pre>';
				print_r($temp);die;*/
				$flag = $this->itemModel->insert($temp);

				if( $flag ) {
					$path = $this->upLoadFile();
					$this->mediaModel->add($path, $flag);
					if( !empty($data['item']['customDetails']) ) {
						$properties = $data['item']['customDetails'];
						$this->load->model('Item_property_model', 'itempropertyModel');
						$queryProperty = $this->itempropertyModel->insert_item($flag, $properties);

					}
					$this->session->set_flashdata('notify', array('status'=>'success', 'caption'=>'Thành công!', 'message'=>'Đăng thông tin báo mất thành công'));
					// call matched
					$this->itemMatching( $flag ); 
				} else {
					$this->session->set_flashdata('notify', array('status'=>'error', 'caption'=>'Thất bại!','message'=>'Lỗi hệ thống'));
				}
				redirect( base_url( 'bao-mat-do-vat' ) );
			}
		} else {
			$this->load->library('nestedset'); 
			$this->load->helper('form');
			$this->load->model('location_model', 'locationModel');
			$this->load->model('category_model', 'categoryModel');

			$data = array();
			$data['title'] = "Trang Báo Mất";
			$data['locations'] =$this->locationModel->getAll();
			$data['categories'] =$this->categoryModel->getAll();
			$data['notify'] = $this->session->flashdata('notify');
			
			$data['content'] = 'frontend/item/item_create_lost';
			$this->load->view('frontend/template/master', $data);
		}		
	}



	public function found()
	{
		$url = $this->input->server('REQUEST_URI');
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$isCheck = false;
			$data = $this->input->post();
			if(isset($data['item']['customDetails'])) {
				$itemProperties =  $data['item']['customDetails'];
				$itemID = $this->itemModel->addItemFound($data);
				$path = $this->upLoadFile();
				$this->mediaModel->add($path, $itemID);
				$isCheck = $this->itemPropertyModel->add($itemProperties, $itemID);
			}
			if($isCheck) {
				$this->session->set_flashdata('notify', array(
					'status' => SUCCESS,
					'message' => MESSAGE_SUCCESS
				));

				// call match item
				$this->itemMatching( $itemID );
			} else {
				$this->session->set_flashdata('notify', array(
					'status' => FAILED, 
					'message' => MESSAGE_FAILED
				));
			}
			redirect($url);
		} else {
			$notify = $this->session->flashdata('notify');
			$categories = $this->categoryModel->getList();
			$locations = $this->locationModel->getList();
			$storageLocations = $this->storageLocationModel->getList();
			$data = array(
				'content' => 'frontend/item/item_create_found',
				'categories' => $categories,
				'locations' => $locations,
				'storageLocations' => $storageLocations,
				'notify' => $notify
			);
			$this->load->view('frontend/template/master', $data);
		}
	}

	public function editItem( $id = 0 )
	{
		$self = $this->input->server('REQUEST_URI');
		if( $this->input->server('REQUEST_METHOD') == 'POST') {
			$data = $this->input->post();
			if( !empty($data) ) {
				date_default_timezone_set('Asia/Ho_Chi_Minh');
				
				$temp = array(
					'detail'				=>	$data['item']['description'],
					'category_id'			=>	$data['item']['customCategory'],
					'location_id'			=>	$data['item']['location'],
					'created_at'			=>	date('Y-m-d H:i:s'),
					'updated_at'			=>	date('Y-m-d H:i:s')
				);

				if(!empty($data['item']['tag_number'])) {
					$temp['fullname']	= $data['item']['fullname'];
				}

				if(!empty($data['item']['email'])) {
					$temp['email']	= $data['item']['email'];
				}

				if(!empty($data['item']['phone_number'])) {
					$temp['phone_number']	= $data['item']['phone_number'];
				}

				if(!empty($data['item']['tag_number'])) {
					$temp['tag_number']	= $data['item']['tag_number'];
				}

				if(!empty($data['item']['storage_location_id'])) {
					$temp['storage_location_id']	= $data['item']['storage_location_id'];
				}


				/*echo '<pre>';
				print_r($temp);die;*/
				$flag = $this->itemModel->updateItem($id, $temp);
				if( $flag ) {
					$path = $this->upLoadFile();
					if( $path != false ) {
						$this->mediaModel->delete($id);
						$this->mediaModel->add($path, $id);
					}
					
					if( !empty($data['item']['customDetails']) ) {
						$properties = $data['item']['customDetails'];
						$this->load->model('Item_property_model', 'itempropertyModel');

						 $this->itempropertyModel->delete($id);

						$queryProperty = $this->itempropertyModel->insert_item($id, $properties);
					}					

					$this->session->set_flashdata('notify', array('status'=>'success', 'caption'=>'Thành công!', 'message'=>'Cập nhật chi tiết vật thành công'));
				} else {
					$this->session->set_flashdata('notify', array('status'=>'error', 'caption'=>'Thất bại!','message'=>'Lỗi hệ thống'));
				}
				redirect($self);
			}
		} else {
			$this->load->library('nestedset'); 
			$this->load->helper('form');
			$this->load->model('location_model', 'locationModel');
			$this->load->model('category_model', 'categoryModel');

			$data = array();
			$data['title'] = "Chỉnh sửa chi tiết";

			$data['detail'] = $this->itemModel->getById($id);
			//print_r($this->login['role_id'] );die;
			if( $this->login['role_id'] != 1 && ( $this->login['_iduser'] !=  $data['detail'][0]->created_by) ) {
				redirect(base_url());
			}

			if( !empty($data['detail']) ) {
				$data['category_detail'] = $this->itemModel->getCategory($data['detail'][0]->category_id);
				$data['property_detail'] = $this->itemModel->getProperty($id);
				$data['location_detail'] = $this->itemModel->getLocation($data['detail'][0]->location_id);
			}	
			//print_r($data);die;
			$data['locations'] =$this->locationModel->getAll();
			$data['categories'] =$this->categoryModel->getAll();
			$data['notify'] = $this->session->flashdata('notify');
			if( $data['detail'][0]->type == 1 ) {
				$data['content'] = 'frontend/item/edit_item_lost';
			} else {
				$data['content'] = 'frontend/item/edit_item_found';
			}
			
			$this->load->view('frontend/template/master', $data);
		}	
	}

	public function deleteItem($id)
	{
		$data['detail'] = $this->itemModel->getById($id);
		if( $this->login['role_id'] != 1 && ( $this->login['_iduser'] !=  $data['detail'][0]->created_by) ) {

			$this->session->set_flashdata('notify', array('status'=>'warning', 'caption'=>'Cảnh báo!', 'message'=>'Bạn không được phép xoá'));
		} else {
			$this->load->model('Item_property_model', 'itempropertyModel');	 
		 	$this->itemModel->deleteItem($id);
			$this->itempropertyModel->delete($id);
			$this->session->set_flashdata('notify', array('status'=>'success', 'caption'=>'Thành công!', 'message'=>'Xoá Item thành công'));			
		}

		redirect(base_url());
	}

	/**
	 * @author ntdat
	 * api - get list properties by category_id
	 * @return: list properties
	 */
	public function getPropertiesByCategory()
	{
		$this->load->model('property_model', 'propertyModel');
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$category_id = $this->input->post('category_id');
			$data = $this->propertyModel->getByCategoryId($category_id);
			echo json_encode($data);
		}
	}

	/**
	 * @author ntdat
	 * api - get list location by location_id
	 * @return: list location
	 */
	public function getLocationById()
	{
		$this->load->model('location_model', 'locationModel');
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$location_id = $this->input->post('location_id');
			$data = $this->locationModel->getById($location_id);
			echo json_encode($data);
		}
	}

	/**
	 * @author ntdat
	 * upload file image
	 */
	public function upLoadFile() {

		if( isset($_FILES["imageItem"]["name"]) ) {
			$album_dir = 'uploads/images/';
			if(!is_dir($album_dir)) {
				mkdir($album_dir, 0777, TRUE);
			}
			$config['upload_path']	 =  $album_dir;
			$config['allowed_types'] =  'jpg|png|jpeg|gif';
			$config['max_size'] =  5120;
				
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$image =  $this->upload->do_upload("imageItem");
			$image_data  = 	$this->upload->data();
			if($image) {
				$path =	$config['upload_path'].$image_data['file_name'];
				// $this->session->set_userdata('album', $image_data);
			}
			return $path;
		}
		return false;
	}

	public function itemDetail($id=0){
		//$this->output->enable_profiler(true);
		settype( $id,'int' );
		if(empty($id)){
			redirect( base_url('error') );
		}

		$data['detail'] = $this->itemModel->getById($id);
		if( !empty($data['detail']) ) { 
			$data['itemStatus'] = array_search($data['detail'][0]->status ,unserialize(STATUS_ITEM)) ;
			$data['image'] = $this->mediaModel->getByItemid($id);
			$data['category'] = $this->itemModel->getCategory($data['detail'][0]->category_id);
			$data['property'] = $this->itemModel->getProperty($id);
			$data['location'] = $this->itemModel->getLocation($data['detail'][0]->location_id);

			//echo  $data['detail'][0]->type ;
			$mixed = array(
				'type' 	=> ( $data['detail'][0]->type ==  1) ? "lost" :  "found",
				'id'	=>	$id,
				'status' => $data['detail'][0]->status
			);
			$role = $this->itemModel->getRoleById( $this->session->userdata('login')['role_id'] ); 
			if( $role[0]['name'] == 'Admin' || $role[0]['name'] == 'Staff' ){
				$data['matches'] = $this->itemModel->getMatches($mixed);
			}else{
				if( isset($this->session->userdata('login')['_iduser']) && $data['detail'][0]->created_by != $this->session->userdata('login')['_iduser'] ) {
					show_error('Bạn ko đủ quyền để xem sp ko phải của bạn đăng , click <a href="'.base_url().'">đây</a>  để về trang chủ ')			;	die;
				}
			}
			// print_r($data['matches']);die;
			$data['itemID'] = $id;
			$data['title'] = 'chi tiết ';
			$data['content'] = 'frontend/item/item_detail';
			$this->load->view('frontend/template/master', $data);
		} else {
			redirect(base_url('error'));
		}		
	}

	public function itemMatches(){
		$id=$this->input->post('id');
		settype( $id,'int' );
		if(empty($id)){
			echo json_encode("");
			exit();
		}

		$detail = $this->itemModel->getById($id);
		$itemStatus = array_search($detail[0]->status ,unserialize(STATUS_ITEM)) ; 
		$mixed = array(
			'type' 	=> ( $detail[0]->type ==  1) ? "lost" :  "found",
			'id'	=>	$id,
			'status' => 0
		);
		$role = $this->itemModel->getRoleById( $this->session->userdata('login')['role_id'] ); 
		if( $role[0]['name'] == 'Admin' || $role[0]['name'] == 'Staff' ){
			$matches = $this->itemModel->getMatches($mixed);
		}else{
			die;
		}
		
		$html = '';
		foreach ($matches as $key => $value) {
			$html .= '<a class="item" href="'.base_url("chi-tiet/$id/so-sanh/". $value->id).'">
						<div class="row">
							<div class="col-xs-2">
								<p>
									<span class="small text-muted">Mã</span>
									'.$value->id.'
								</p>
							</div>
							<div class="col-xs-7">
								<h4>'.$value->category_name.'</h4>
								<div class="table-responsive">
									<table class="table table-condensed">
										<colgroup>
											<col width="30%">
											<col>
										</colgroup>
										<tbody>';

											if( !empty( $value->tag_number ) ) {
											$html .= '<tr>
												<th>Serial Number</th>
												<td>'.$value->tag_number.'</td>
											</tr>';
											}

												$property = explode('::', $value->property_name );
												foreach ($property as $property_item) {
													$item = explode('||', $property_item );
													$html .= '<tr>
														<th>'.$item[0].'</th>
														<td>'.$item[1].'</td>
													</tr>';
												}
									$html.='
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-xs-3">
								<ul class="list-unstyled">
									<li>Phần trăm :
										<span class="item-status bg-default">';
										$html .= $value->percent.'%</span>
									</li>';
										$html .=  '
									<li>Status:
										';
										$html .= $itemStatus;
										$html .= '
									</li>
									<li>
										<span class="item-type bg-default">';
										$html .= ( $value->type == 1 ) ? "Mất" : "Thấy";
										$html .= '</span>
										lúc '.date('d/m/Y H:i:s', strtotime($value->created_at)).'
									</li>
									<li>';
									$html .= ( $value->type == 1 ) ? "Mất" : "Thấy"; 
									$html .= 'tại
										<span class="item-location bg-default">'.$value->location_name.'</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
			';
		}

		echo json_encode($html);
	}

	public function itemDetailFilter($id=0){
		settype( $id,'int' );
		if(empty($id)){
			show_error('ko bik id này'); die;
		}
		

		$data['detail'] = $this->itemModel->getById($id);
		
		if( !empty($data['detail']) ) { 
			$data['itemStatus'] = array_search($data['detail'][0]->status ,unserialize(STATUS_ITEM)) ;
			$data['image'] = $this->mediaModel->getByItemid($id);
			$data['category'] = $this->itemModel->getCategory($data['detail'][0]->category_id);
			$data['property'] = $this->itemModel->getProperty($id);
			$data['location'] = $this->itemModel->getLocation($data['detail'][0]->location_id);

			//echo  $data['detail'][0]->type ;
			$mixed = array(
				'type' 	=> ( $data['detail'][0]->type ==  1) ? "lost" :  "found",
				'id'	=>	$id,
				'status' => $data['detail'][0]->status,
				'filter' => 1
			);
			$role = $this->itemModel->getRoleById( $this->session->userdata('login')['role_id'] ); 
			if( $role[0]['name'] == 'Admin' || $role[0]['name'] == 'Staff' ){
				$data['matches'] = $this->itemModel->getMatches($mixed);
			}else{
				if( isset($this->session->userdata('login')['_iduser']) && $data['detail'][0]->created_by != $this->session->userdata('login')['_iduser'] ) {
					show_error('Bạn ko đủ quyền để xem sp ko phải của bạn đăng , click <a href="'.base_url().'">đây</a>  để về trang chủ ')			;	die;
				}
			}
			// print_r($data['matches']);die;
			$data['keys'] = $this->input->get('keys');
			$data['categorys'] = $this->itemModel->getCategory();
			$data['locations'] = $this->itemModel->getLocation();
			$data['action'] = 'chi-tiet/bo-loc';
			$data['itemID'] = $id;
			$data['title'] = 'chi tiết ';
			$data['content'] = 'frontend/item/item_detail_filter';
			$this->load->view('frontend/template/master', $data);
		} else {
			redirect(base_url('error'));
		}
	}

	public function itemDetailMatches($id=0,$idmatched=0){
		settype( $id,'int' );
		settype( $idmatched,'int' );
		if(empty($id) || empty($idmatched) ){
			show_error('ko bik id này'); die;
		}
		$insertNotification = [];
		$this->load->model('Property_model', 'propertyModel');
		$this->load->model('Matched_Item_model', 'matchedItemModel');
		$this->load->model('Notification_model', 'notificationModel');
		
		$status_item = unserialize(STATUS_ITEM);
		$matching_status = unserialize(MATCHING_STATUS);
		$type_item = unserialize(TYPE_ITEM);
		$data['detail'] = $this->itemModel->getById($id);
		$data['detailMatched'] = $this->itemModel->getById($idmatched);
		if( empty( $data['detail'] ) || empty( $data['detailMatched'] ) ){
			show_error(' chưa có thông tin của item '); die;
		}elseif( $data['detail'][0]->type == $data['detailMatched'][0]->type ){
			show_error(' 2 item cùng loại ko thể matched '); die;
		}elseif( !in_array($data['detail'][0]->type , $type_item ) || !in_array( $data['detailMatched'][0]->type, $type_item )  ){
			show_error(' 1 trong 2 item có type khác quy định '); die;
		}
		
		if( array_search( $data['detail'][0]->type, $type_item ) == 'lost' ){
			$lostId = $id; $lostItem = $data['detail'][0];
			$foundId = $idmatched; $foundItem = $data['detailMatched'][0];
		}elseif( array_search( $data['detailMatched'][0]->type, $type_item ) == 'lost' ){
			$lostId = $idmatched; $lostItem = $data['detailMatched'][0];
			$foundId = $id;  $foundItem = $data['detail'][0];
		}
		if( $this->input->server('REQUEST_METHOD') == 'POST' && isset( $this->input->post('match_items')['confirmMatch'] ) ){
			if( $data['detail'][0]->status == $status_item['matched'] || $data['detailMatched'][0]->status == $status_item['matched'] ){
				show_error(' 1 trong 2 item đã được match'); die;
			}
			//print_r( $this->input->post('match_items') ); die;
			// Array ( [sendOwnerEmail] => 1 [ownerMessage] => asdasdasdasd [confirmMatch] => [item1_id] => 58020 [item2_id] => 58005 )
			 
			$match_items = $this->input->post('match_items');
			// cap nhat  2 item đã matched
			$updates = ['status' => $status_item['matched'] ];
			$updatesMatched = ['status' =>$status_item['matched'] ];

			// them vao matched_item
			$insertMatchedArr = [ 'status' => $matching_status['matched'], 'created_at' => date('Y-m-d H:i:s',time()), 'matched_date' => date('Y-m-d H:i:s',time()), 'percent' => $match_items['percent'] , 'item_lost_id' => $lostId , 'item_found_id' => $foundId ];
			
			// , notification thong báo cho user
			
				$title = 'Vật bị mất mã '. $lostItem->id .' đã được matched ' ;
					$insertNotification[ ] = [ 
						'type' => unserialize(NOTIFICATION_TYPE)['admin'], 
						'short_description' => $match_items['ownerMessage'] , 
						'user_id' => $lostItem->created_by , 
						'title' => $title , 
						'content' => $lostItem->id,
						'status' => unserialize(NOTIFICATION_STATUS)['unread'],
						'created_at' => date('Y-m-d H:i:s',time()) , 'updated_at' => date('Y-m-d H:i:s',time()) 
					];

			if( isset( $match_items['sendOwnerEmail'] ) && $match_items['sendOwnerEmail'] == 1 ){
				$categoryFound = $this->itemModel->getCategory( $foundItem->category_id);
				$title = 'Vật tìm thấy <b>'. $categoryFound[0]['name'] .'</b> mã '. $foundItem->id ;
					$short_description = 'Đã tìm thấy sản phẩm tương đồng cho vật này có mã ' .$lostItem->id ;
					$insertNotification[ ] = [ 
						'type' => unserialize(NOTIFICATION_TYPE)['admin'], 
						'short_description' => $short_description, 
						'user_id' => $foundItem->created_by , 
						'title' => $title, 
						'content' => $foundItem->id,
						'status' => unserialize(NOTIFICATION_STATUS)['unread'],
						'created_at' => date('Y-m-d H:i:s',time()) , 'updated_at' => date('Y-m-d H:i:s',time()) 
					];
			}
			
			$this->itemModel->updateArray($updates,'id',$id );
			$this->itemModel->updateArray($updatesMatched,'id',$idmatched );
			$id_new_match = $this->matchedItemModel->insertRow( $insertMatchedArr );
			if($this->notificationModel->insertArray($insertNotification) == -1 ) {
				// show_error(' loi thong bao'); die;
			}
			// chuyển về trang trung khop
			if( $id_new_match ){
				redirect( base_url('danh-sach-do-vat/trung-khop/#'.$id ) );
				die;
			}
		}
		
			$data['image'] = $this->mediaModel->getByItemid($id);
			$data['itemStatus'] = array_search($data['detail'][0]->status ,$status_item) ;
			$data['location'] = $this->itemModel->getLocation($data['detail'][0]->location_id);
			$data['property'] = $this->itemModel->getProperty($id);
			$data['category'] = $this->itemModel->getCategory($data['detail'][0]->category_id);
			$data['percent'] = $this->matchedItemModel->getItemByFoundAndLost( $foundId,$lostId );
			
			$data['imageMatched'] = $this->mediaModel->getByItemid($idmatched);
			$data['itemStatusMatched'] = array_search($data['detailMatched'][0]->status ,$status_item) ;
			$data['locationMatched'] = $this->itemModel->getLocation($data['detailMatched'][0]->location_id);
			$data['propertyMatched'] = $this->itemModel->getProperty($idmatched);
			$data['categoryMatched'] = $this->itemModel->getCategory($data['detailMatched'][0]->category_id);
		
		
		$data['itemID'] = $id;
		$data['itemIDMatched'] = $idmatched;
		$data['title'] = 'chi tiết ';
		$data['content'] = 'frontend/item/item_detail_match';
		$this->load->view('frontend/template/master', $data);
	}

	public function item_list($type='',$page='')
	{
        $data = []; $dataOut = []; $type = empty($type)? '': $type;
		$title = ''; $base_url = '';
		$currentpage = $page; settype($currentpage, "int"); if($currentpage <= 0) $currentpage = 1;
		$perpage = 10; $offset = 3;
		$totalrows = 0; $queryGet = preg_replace("/^([\w\W]*?)(\&page=[0-9]+)$/i",'$1',$this->input->server('QUERY_STRING') ); $uri = ''; $breadcrumbs = [];
		$breadcrumbs[] = ['uri' => 'danh-sach-do-vat', 'text' => 'Báo mất và tìm thấy' ];
        if( $type == 'bi-mat' ){
			$type = 'lost'; $uri = 'danh-sach-do-vat/bi-mat';
        }elseif($type == 'tim-thay'){
            $type = 'found'; $uri = 'danh-sach-do-vat/tim-thay';
        }elseif($type == 'tim-kiem'){
            $type = 'search'; $uri = 'tim-kiem'; $data['keys'] = $this->input->get('keys');
        }elseif($type == 'loc-ket-qua' ){
            $type= 'filter'; $uri = 'danh-sach-do-vat/loc-ket-qua';
		}elseif( $type == 'trung-khop' ){
			$type = 'matched'; $uri = 'danh-sach-do-vat/trung-khop';
		}else{
            $uri = 'danh-sach-do-vat'; $breadcrumbs = [];
        }
        $base_url = base_url($uri);
		$data_tmp = $this->itemModel->getItemlist($type,$perpage,$totalrows,$currentpage,$title);		
		$all_data_tmp = $this->itemModel->getItemlist($type,0,$totalrows,$currentpage,$title);
		
        // bắt  nếu request là ajax
        if(!empty( $this->input->get('ajax'))){ 
            echo json_encode($data_tmp['data']); die;
		}
		$breadcrumbs[] = ['uri' => $uri, 'text' => $title ];
		#$data['method'] = $this->router->fetch_method();
		$data['breadcrumbs'] = $breadcrumbs;
        $data['action'] = $uri;
		$data['items'] = $data_tmp['data'];
		$data['all_items'] = $all_data_tmp['data'];
        $data['title'] = $title;
		$data['categorys'] = $this->itemModel->getCategory();
        $data['locations'] = $data_tmp['locations'];
		$data['pagelink'] = $this->itemModel->pagesLink(['base_url'=>$base_url,'queryGet'=>$queryGet],$totalrows,$currentpage,$perpage); 
		$data['pagelist'] = $this->itemModel->pagesList( ['base_url'=>$base_url,'queryGet'=>$queryGet] ,$totalrows,$currentpage,$perpage,$offset);
		$data['content'] = 'frontend/item/item_list';
		$this->load->view('frontend/template/master', $data);
	}
	
    private function searchString($string, $array){
        foreach($array as $key => $row){
            if( stristr( $string,$row  ) ){
                return $key;
            }
        }
        return -1;
    }
    /*  tham số iditem muốn match , perToSave số % muốn save vào db  , option[]
    return -1 loi du lieu input 
    return 0 ko có dữ liệu
	return 1 thành công
    ( chỉ quét những item cùng category trước nâng cao khác category làm sau )
    */
	public function itemMatching($itemID = 0, $options = [] ){
		
		# check input
		# tag_number = 100% , location = 60%, propertye = 30% , detail 10 %
        $matchedList = (object)['ids' => [],'insert'=>[], 'blacklist' => [], 'matched' => [], 'maxLevel' => 0, 'locationList' => [] ];
		$this->load->model('Property_model', 'propertyModel');
		$this->load->model('Matched_Item_model', 'matchedItemModel');
		$this->load->model('Notification_model', 'notificationModel');
		settype($itemID,'int');
		if(empty($itemID)) return -1;
		// echo 'itemID = '. $itemID .'<br>';
		// ob_flush();
		// flush();
		if( isset($options['worker']) )
		$this->logModel->ghilog('bắt đầu chạy worker ' . $options['worker'] .' với idtem = ' . $itemID);

		$item = $this->itemModel->getById($itemID); // lấy item hien tai
		if( empty($item) ) return 0; 
		$categories = $this->itemModel->getCategory( $item[0]->category_id );
		$itemBlacklist = $this->matchedItemModel->getByItemFoundID($itemID); // lấy ds các item đã quét
		foreach( $itemBlacklist as $key => $value ){ // thêm vào matchedlist để chuyển qua các function khác tương tac
			$matchedList->blacklist[$value['item_lost_id']] = $key;
		}
		// lấy ra các item cùng thể loại với id hiện tại và khác id hiện tại , khác type , 
		// nếu các id đã lấy ra có id nào đã quét với id hiện tại thì bỏ ra
		$result_tmp = $this->itemModel->filterCategory($item[0]);
		$result = [];
		foreach( $result_tmp as $keyResult => $valueResult ){
			if( !isset( $matchedList->blacklist[ $valueResult['id']] ) ){
				$result[] = $valueResult;
			}
		}
		//unserialize(TYPE_ITEM)[$type] 
		//$options = ['worker'=>true,'maxLevel'=>$maxLevel];
		if( !empty($options['maxLevel'] ) ){
			$matchedList->maxLevel = $options['maxLevel'];
		} 
		
		$this->itemMatching_filterTagNumber($item[0],$matchedList, $options); // lọc tag number , nếu có pecent = 100% , nếu khác thẻ laoi5 thì status = 2 
		$matchedList->locationList = $this->locationModel->getAllArray();
		$this->itemMatching_filterLocation( $item[0],$matchedList, $result , $options ); // lọc location theo phân cấp , phân cấp càng chính xác % càng cao
		
		$this->itemMatching_filterProperty($item[0],$matchedList, $result, $options); // lọc theo property % đúng càng nhìu property % càng cao
		
		$insertArr = []; $insertNotification = []; 
		if( isset( $options['worker'] ) ){
			$type =  unserialize(NOTIFICATION_TYPE)['system'];
		}else{
			$type =  unserialize(NOTIFICATION_TYPE)['item'];
		}
		
		// $value => ['id]
		foreach( $matchedList->insert as $key => $value ){
			$short_description = ''; $title ='';
			if( $value['percent'] >= PERCENT_MIN_SUCCESS ){
				$insertArr[] = $value;
				$itemLost = $this->itemModel->getById($value['item_lost_id']);
				$itemFound = $this->itemModel->getById($value['item_found_id']);
				if( empty( $itemLost ) || empty($itemFound) ) continue;
				if( !isset( $insertNotification[ $value['item_lost_id'] ] ) ){
					$title = 'Vật bị mất <b>'. $categories[0]['name'] .'</b> mã '. $value['item_lost_id'] ;
					$short_description = 'Đã tìm thấy các sản phẩm tương đồng cho vật này có mã ' .$value['item_found_id'].' trùng '. $value['percent'] .'%' ;
					$insertNotification[ $value['item_lost_id'] ] = [ 
						'type' => 2, 
						'short_description' => $short_description , 
						'user_id' => $itemLost[0]->created_by , 
						'title' => $title, 
						'content' => $value['item_lost_id'],
						'status' => unserialize(NOTIFICATION_STATUS)['unread'],
						'created_at' => date('Y-m-d H:i:s',time()) , 'updated_at' => date('Y-m-d H:i:s',time()) 
					];
				}elseif( !isset( $insertNotification[ $value['item_found_id'] ] ) ){
					$title = 'Vật tìm thấy <b>'. $categories[0]['name'] .'</b> mã '. $value['item_found_id'] ;
					$short_description = 'Đã tìm thấy các sản phẩm tương đồng cho vật này có mã ' .$value['item_lost_id'] .' trùng '. $value['percent'] .'%' ;
					$insertNotification[ $value['item_found_id'] ] = [
						'type' => 2, 
						'short_description' => $short_description , 
						'user_id' => $itemFound[0]->created_by, 
						'title' => $title, 
						'content' => $value['item_found_id'],
						'status' => unserialize(NOTIFICATION_STATUS)['unread'],
						'created_at' => date('Y-m-d H:i:s',time()) , 'updated_at' => date('Y-m-d H:i:s',time()) 
					];
				}
			}
		}
		$insertNotification = array_values($insertNotification);
		if($this->notificationModel->insertArray($insertNotification) == -1 ) {
			// show_error(' loi thong bao'); die;
		}

		// insert vao match item
		if($this->matchedItemModel->insertArray( $insertArr ) == -1 ) {

			// die(' check loi tai day');
		}
		// cap nhat status
		if( !empty( $matchedList->matched ) ){
			foreach( $matchedList->matched as $key => $row ){
				$updates = ['status' => unserialize(STATUS_ITEM)['matched'] ];
				$this->itemModel->updateArray($updates,'id',$key);
			}
		}
		
		// ghi log
		if( isset($options['worker']) )
		$this->logModel->ghilog('kết thúc chạy worker ' . $options['worker'] .' với idtem = ' . $itemID);
	}

	/* ý nghĩa : trả về tổng % các phần tử của b có trong a
	return -1 input sai */
	private function sosanhMatch($a = [] ,$b = [] ){
		// settype($total,'int');
		$dem = 0;
		if( is_array($a) && is_array($b) && !empty($a) && !empty($b)  ){ # nếu sai nên check lại total đã đúng giá tri chưa
			foreach( $b as $keyB => $valB ){
				foreach( $a as $keyA => $valA ){
					if( ($valB['property_id'] == $valA['property_id']) && !empty($valA) && !empty($valB)  ){
						if( $this->boTatCa( $valB['value'] ) ==  $this->boTatCa( $valA['value'] ) ){
							$dem++;
						}
						break;
					}
				}
			}
		}elseif( empty($a) || empty($b) ) {
			return 0;
		}
		else
			return -1;
		// $kq = array_diff( $a , $b );
			// print_r($kq); die;
		return round((float)$dem/count($a), 2) * PERCENT_PROPERTY ;
    }
    
    /* tìm kiếm sản phảm đơn giản
        truyền vào object item get từ id.
        return array các sản phẩm đã đc match trong item_matched
    */
    public function itemMatching_filterTagNumber($item,&$matchedList = [], $options = []){
		if( !isset($item->id) ) return [];
		$item_filterTagNumber = $this->itemModel->filterTagNumber($item); // loc tagnumber
        //$id_matchedNew = [];
        if( empty($matchedList) ) {
			$matchedList = (object)['ids' => [],'insert'=>[], 'blacklist' => [], 'matched' => [], 'maxLevel' => 0, 'insertNotification' => [] ];
			$i = 0;
		}else{
			$i = count( $matchedList->insert );
		}
		
		foreach( $item_filterTagNumber as $key => $row_filterTagNumber ){
			// nếu như sp đã đc đánh giá % với món đó rồi , có đánh giá lại nữa ko .
			// id tìm thấy hiện tại
			if( isset( $matchedList->blacklist[ $row_filterTagNumber['id']] ) ){
				continue;
			}else{
				if( isset( $matchedList->ids[$row_filterTagNumber['id']] ) ){
					$key_Insert = $matchedList->ids[$row_filterTagNumber['id']];
					$matchedList->insert[ $key_Insert ]['percent'] += 100;
				}else{
					$insert = [];
					$insert['matched_date'] = date('Y-m-d H:i:s',time());
					$insert['percent'] = 100;
					$insert['created_at'] = date('Y-m-d H:i:s',time());
					$this->detachLostFoundMatched( $insert,$row_filterTagNumber['type'],$row_filterTagNumber['id'],$item->id );
					// check category = nhau => status 1 nếu có
					if( $item->category_id == $row_filterTagNumber['category_id'] ){
						$insert['status'] = unserialize(MATCHING_STATUS)['matched'];
						$matchedList->matched[ $row_filterTagNumber['id'] ] = $i;
						$matchedList->matched[ $item->id ] = $i;
					}else{
					// else status 2
						$insert['status'] = unserialize(MATCHING_STATUS)['suggest'];
					}
					$matchedList->ids[$row_filterTagNumber['id']] = $i;
					$matchedList->insert[$i] = $insert;
					// nâng cao quy tac them , neu id user đã có rồi thì ko gửi nữa mà gửi tiếp vao content
					// $matchedList->insertNotification[]  = [ 'type' => 1, 'content' => ' Đã tìm thấy sản phẩm gần giống với sản phẩm  ' , 'user_id' => $lostItem->created_by , 'created_at' => date('Y-m-d H:i:s',time()) , 'updated_at' => date('Y-m-d H:i:s',time()) ];
					
				}
				$i++;
			}
            //$id_matchedNew[] = $this->matchedItemModel->insert($insert);
		} // end foreach

		if( isset($options['worker']) )
		$this->logModel->ghilog('tagNumber với i '. $i);
        return $matchedList;
    }

    private function itemMatching_filterLocation($item,&$matchedList = [] , $result , $options = []){
        // check matchedlist đã có chưa    
		if( empty($matchedList) ) {
			$matchedList = (object)['ids' => [],'insert'=>[], 'blacklist' => [], 'matched' => [], 'maxLevel' => 0 ];
			$i = 0;
		}else{
			$i = count( $matchedList->insert );
		}

		#$result = $this->itemModel->filterCategory($item);
		$listLocationLevel = $this->locationModel->getItemlist_Recursion_level($matchedList->locationList,[], $item->location_id , 1 , 'father' );
		if( empty( $listLocationLevel ) ) return;
			$maxLevel_Item = max( array_column($listLocationLevel , 'level' ) ); // tim level lon nhat của item
		if( empty( $matchedList->maxLevel ) ){
			if( isset( $options['maxLevel'] ) ){
				$maxLevel = $options['maxLevel'];
				$matchedList->maxLevel = $options['maxLevel'];
			}else{
				$allLocationLevel =  $this->locationModel->getItemlist_Recursion_level($matchedList->locationList,[], 0 , 1  );
				$maxLevel = max( array_column($allLocationLevel , 'level' ) ); // tim level lon nhat toàn item
				$matchedList->maxLevel = $maxLevel;
			}
		}else{
			$maxLevel = $matchedList->maxLevel; //asdasd
		}
		// print_r($maxLevel); die;
        foreach( $result as $row ){
			$insert = [];
			$insert['created_at'] = date('Y-m-d H:i:s',time());
			$insert['status'] = unserialize(MATCHING_STATUS)['suggest'];
			$insert['matched_date'] = date('Y-m-d H:i:s',time());
			// lấy ra các location cha của item đang loop
			$key_InList = false;
			$listLocationLevel_row = $this->locationModel->getItemlist_Recursion_level( $matchedList->locationList,[], $row['location_id'] , 1 , 'father' ); 
			
			foreach( $listLocationLevel_row as $location_loop ){
				$key_InList = array_search( $location_loop['id'], array_column( $listLocationLevel, 'id'  ) );
				if( $key_InList !== false ) break;
			}
            
            if( $key_InList !== false ){
				// lay level , từ đó => % ,  
				$maxLevel_row = max( array_column($listLocationLevel_row , 'level' ) ); // tim max level item đang loop
				$currentLevel = $listLocationLevel[$key_InList]['level'] ;
				// $percent = round((float)$currentLevel/$maxLevel ,2)*60; // làm tròn 2 chữ số
				$currentLevel = ( abs( $currentLevel - $maxLevel_Item  ) + 1 );
				//$percent > 1
				// nếu item hien tai va item đang xét cùng level , thì level max phải bằng nhau , ngc lại item đang macht phải khác nhau
				if( (($maxLevel_row == $maxLevel_Item && $currentLevel == $maxLevel_row) || ($maxLevel_row != $maxLevel_Item ) ) && $currentLevel > 1 ){
					$percent = round((float)$currentLevel/$maxLevel ,2)* PERCENT_LOCATION ;
				}else {
					$percent = 0;
				}
            }else{
				$percent = 0;
			}
			//lấy id kiem tra xem id đã có chưa nếu có + % lên
			if( isset( $matchedList->ids[$row['id']] ) ){
				$key_Insert = $matchedList->ids[$row['id']];
				$matchedList->insert[ $key_Insert ]['percent'] += $percent;
			}else{
			// nếu chưa thêm mới vào list insert
				$this->detachLostFoundMatched( $insert,$row['type'],$row['id'], $item->id  );
				// $matchedList->insert[] = $insert;
				$matchedList->ids[$row['id']] = $i;
				$insert['percent'] = $percent;
				$matchedList->insert[$i] = $insert;	
			}
			$i++;
		}// end foreach

		if( isset($options['worker']) )
		$this->logModel->ghilog('fillocation với i '. $i);
	}
	
	private function itemMatching_filterProperty($item,&$matchedList = [], $result, $options = []){
		$this->load->model('property_model', 'propertyModel');
		if( empty($matchedList) ) {
			$matchedList = (object)['ids' => [],'insert'=>[], 'blacklist' => [], 'matched' => [], 'maxLevel' => 0 ];
			$i = 0;
		}else{
			$i = count( $matchedList->insert );
		}

		// $result = $this->itemModel->filterCategory($item);
		$listProperty = $this->itemPropertyModel->getItemProperty($item->id);
		
		
		$totalProperty = $this->propertyModel->getByCategoryId( $item->category_id);
		foreach( $result as $row ){

			$matchProperty = $this->itemPropertyModel->getItemProperty($row['id']);
			$percent = $this->sosanhMatch( $listProperty, $matchProperty );
			if( $percent == -1 ) die(' check lai loi so sanh');
			if( isset( $matchedList->ids[$row['id']] ) ){
				$key_Insert = $matchedList->ids[$row['id']];
				$matchedList->insert[ $key_Insert ]['percent'] += $percent;
			}else{
			// nếu chưa thêm mới vào list insert
				$insert = [];
				$insert['matched_date'] = date('Y-m-d H:i:s',time());
				$insert['percent'] = $percent;
				$insert['created_at'] = date('Y-m-d H:i:s',time());
				$insert['status'] = unserialize(MATCHING_STATUS)['suggest'];
				$this->detachLostFoundMatched( $insert,$row['type'],$row['id'],$item->id );
				// $matchedList->insert[] = $insert;
				$matchedList->ids[$row['id']] = $i;
				$matchedList->insert[$i] = $insert;
				
			}
			$i++;
		}

		if( isset($options['worker']) )
		$this->logModel->ghilog('property với i '. $i);
	}

	private function detachLostFoundMatched(&$insert,$loop_type,$loop_id,$item_id) {
		if( $loop_type == unserialize(TYPE_ITEM)['lost']  ){
			$insert['item_lost_id'] = $loop_id;
			$insert['item_found_id'] = $item_id;
		}elseif($loop_type == unserialize(TYPE_ITEM)['found']) {
			$insert['item_lost_id'] = $item_id;
			$insert['item_found_id'] = $loop_id;
		}else{
			die('check funciton itemMatching , type khong co trong dinh nghia');
		}
    }

	function boTatCa($str) {
		$str = trim($str);
		$str = strip_tags($str);
		$str = $this->bodau($str);
		if(!$str) return "empty";
		else {
		  $str = str_replace("?", "", $str);
		  $str = str_replace("&", "", $str);
		  $str = str_replace("'", "", $str);
		  $str = str_replace("+", "", $str);
		  $str = str_replace("=", "", $str);
		  $str = str_replace("(", "", $str);
		  $str = str_replace(")", "", $str);
		  $str = str_replace("/", "-", $str);
		  $str = str_replace("$", "", $str);
		  $str = str_replace("\"", "", $str);
		  $str = str_replace("\\", "", $str);
		  while (strpos($str, '  ')) {
			$str = str_replace('  ', ' ', $str);
		  }
		  $str = mb_convert_case($str, MB_CASE_LOWER, 'utf-8');
		  #$str = str_replace(" ", "-", $str);
		}
		return $str;
	  }
	  
	  function bodau($str) {
		
		if(!$str) return false;
		$unicode = array(
		  'a' => 'á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ',
		  'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
		  'd' => 'đ',
		  'D' => 'Đ',
		  'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
		  'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
		  'i' => 'í|ì|ỉ|ĩ|ị',
		  'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
		  'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
		  'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
		  'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
		  'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
		  'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
		  'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ');
		foreach ($unicode as $khongdau => $codau) {
		  // echo $khongdau .'<br>';
		  //$arr = explode("|", $codau);
		  //echo $str = str_replace($arr, $khongdau, $str) .' ';
		  $str = preg_replace("/($codau)/i", $khongdau, $str);
		}
		return $str;
	  }
	
	public function worker($second = 0){
		ignore_user_abort(true);
		$this->load->model('Worker_model','workerModel');
		$worker = $this->workerModel->getWorker();
		if(empty($worker)) die('ko tìm thấy worker');
		$updates['status'] = 2;
		$updateStatus = $this->workerModel->updateWorker( $updates,'id',$worker[0]['id'] );
		if($updateStatus){
			$items = $this->itemModel->getItem_Worker(); // list cac item found
			if(empty($items)){
				die('ko còn item');
			}
			$allLocationLevel =  $this->locationModel->getItemlist_Recursion_level( $this->locationModel->getAllArray(),[], 0 , 1  );
			$maxLevel = max( array_column($allLocationLevel , 'level' ) ); // tim level lon nhat toàn item
			$options = ['worker'=> $worker[0]['name'] ,'maxLevel'=>$maxLevel];
			foreach( $items as $item ){
				$this->itemMatching($item['id'], $options );
				if( !empty($second) ) sleep($second);
			}
		}
		$updates['status'] = 1;
		$this->workerModel->updateWorker( $updates,'id',$worker[0]['id'] );
		// flow
		// 2 tham số , thời gian item , và số giây chạy mỗi lần ( for test )
		// select worker is active
		// get worker id , => update worker to running
		// get all ite -> dieu kien status chua match , va type phai = 2 ( tuc la found )
		// chạy function matching
		// neu co du lieu insert vao 
		// update status item về 2 update worker về 1
		// https://qtpm.wwsgo.com/itemController/testWorker
		// 
		// ob_flush();
		// flush();
		// ob_end_flush();
		// $data = ['content' => 'start worker ' . date('d/m/Y H:i:s',time()) , 'date_log' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ];
		// $this->db->insert('log', $data);
		// sleep(60);
		// $data = ['content' => 'end worker ' . date('d/m/Y H:i:s',time()) , 'date_log' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ];
		// echo 'it\'s done';
		// $this->db->insert('log', $data);
		
	}
	/* test case for matchitem chỉ insert dữ liệu vào table item và item_propety ( chưa insert hình )
	type là loại tìm thấy hay bị mất
	n = số lượng insert
	*/
    public function testMatch($type = 0,$n = 0){
		#if(empty($type)) die(' not found type');
		$temp = []; $item = [];
        $str = "a b c d e f g h i j k l m n o p q x y z";
        $chuoiho = "Nguyễn Trần Lê Phạm Hoàng Huỳnh Phan Võ Đặng Bùi Đỗ Hồ Ngô Kim Dương Lý ";
				$chuoiten = "Trường An Thiên Ân Minh Anh Quốc Bảo Ðức Bình Hùng Cường Hữu Đạt Minh Đức Ðức Tài Hữu Thiện Phúc Thịnh Đó là sự mong muốn của bố mẹ để bé luôn có một cuộc sống an lành may mắn hạnh phúc nhờ tài năng đức mình có sự đức độ để bình yên thiên hạ Mỹ Nhân Thảo Nhi Tuệ Nhiên Hạnh Nhơn Hoàng Oanh Kim Oanh Lâm Oanh";
				
        $strArr =explode(" ",$str);  
        $hoarr = explode(" ",$chuoiho); 
        $tenarr = explode(" ",$chuoiten); $tong = count($tenarr)-1;
        for( $i = 0 ;  $i<$n;$i++ ){
						$type = mt_rand( 1 , 2 );
            $hoten = mb_strtoupper($hoarr[mt_rand(0,15)].' '.$tenarr[mt_rand(0,$tong)].' '.$tenarr[mt_rand(0,$tong)],"UTF-8");
            $email = $strArr[ mt_rand(0,19)] .  $strArr[ mt_rand(0,19)]  . $strArr[ mt_rand(0,19)]  . $strArr[ mt_rand(0,19)]  . $strArr[ mt_rand(0,19)]  . $strArr[ mt_rand(0,19)] . '@wwsgo.com';
            $sdt = '09'.mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
            $tag_number = $strArr[ mt_rand(0,19)] .  $strArr[ mt_rand(0,19)] . mt_rand(0,9).mt_rand(0,9) ;
            $detail = "Tại sao nên mua điện thoại Apple iPhone X?
            Thiết kế “không viền màn hình” độc đáo, lạ mắt.
            Sử dụng chất liệu nhôm kết hợp kính toát lên vẻ đẹp sang trọng, quyến rũ.
            Hiệu năng mạnh nhất trên thị trường.
            Trang bị công nghệ bảo mật bằng nhận dạng gương mặt.
            Hỗ trợ sạc nhanh, sạc không dây.
            Camera kép chụp ảnh chất lượng hàng đầu.";
            $category_id = mt_rand(1,10); $location_id =  mt_rand(1,82);
            $where = ['category_id' => $category_id];
            $arrId_Property = $this->db->select('id,name')->where($where)->get('property')->result_array();
            $charArr = explode(',','sản,màu,tên,giả,model,hình,chất,kiện,kiểu,bản');
            $numArr = explode(',','kích cỡ,phẩm') ;
            $data['item']['customDetails'] = [];
            foreach( $arrId_Property as $row ){
                $value = [];
                if( strpos(mb_strtolower($row['name'],"utf-8") ,$charArr[5] )  !== false    ){
                    $value = ['tròn','méo','tam giác'];
                }elseif( strpos( mb_strtolower($row['name'],"utf-8"), $charArr[6] )  !== false ){
                    $value = ['colton','silicon','nhựa','vải'];
                }elseif(  $this->searchString( mb_strtolower($row['name'],"utf-8") , [$charArr[0],$charArr[2],$charArr[3],$charArr[9]] )  != -1 ){
                    $value = ['Apple','Iphone','Samsung','Steven Job','Bill Gate'];
                }elseif(strpos(mb_strtolower($row['name'],"utf-8") ,$charArr[1] )  !== false){
                    $value = ['Đỏ','Đen'];
                }elseif(strpos(mb_strtolower($row['name'],"utf-8") ,$charArr[4] )  !== false){
                    $value = ['1','2'];
                }elseif(strpos(mb_strtolower($row['name'],"utf-8") ,$charArr[7] )  !== false){
                    $value = ['túi xách','ốp lưng','sạc đa năng'];
                }elseif(strpos(mb_strtolower($row['name'],"utf-8") ,$charArr[8] )  !== false){
                    $value = ['ngôi sao'];
                }elseif(strpos(mb_strtolower($row['name'],"utf-8") ,$numArr[0] )  !== false){
                    $value = ['vừa','nhỏ','bự'];
                }elseif(strpos(mb_strtolower($row['name'],"utf-8") ,$numArr[1] )  !== false){
                    $value = ['1'];
                }
                $dem_arr =  empty( count( $value ) )? 0:count( $value );
                $giatri = empty($dem_arr)? '':$value[ rand(0,$dem_arr - 1)];
                $data['item']['customDetails'][]  = ['id' => $row['id'] , 'value'  => $giatri ] ;
            }
            
			// $data['item']['customDetails'] = ['id'];
			$date_create =  time() + mt_rand( 100000, 900000 );
            if( $type == 1 ){
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                    
                    $temp = array(
                        'type'					=>	1,
                        'fullname'				=>	$hoten ,
                        'email'					=>	$email  ,
                        'phone_number'			=>	$sdt  ,
                        'found_by'				=>	NULL,
                        'tag_number'			=>	$tag_number ,
                        'detail'				=>	$detail,
                        'status'				=>	0,
                        'storage_location_id'	=>	0,
                        'category_id'			=>	$category_id ,
                        'location_id'			=>	$location_id ,
                        'created_at'			=>	date('Y-m-d H:i:s', $date_create ),
                        'created_by'			=>	1007,
                        'updated_at'			=>	date('Y-m-d H:i:s' , $date_create )
                    );
                    
                    $flag = $this->itemModel->insert($temp) .' ';
                    
                    if( $flag ) {
                        $properties = $data['item']['customDetails'];
                        $this->load->model('Item_property_model', 'itempropertyModel');
                        $queryProperty = $this->itempropertyModel->insert_item($flag, $properties); // change this
                    }
            }elseif($type == 2) {
                $itemProperties =  $data['item']['customDetails'];
                
                $item = array(
                    'type'					=>	2,
                        'fullname'				=>	$hoten ,
                        'email'					=>	$email  ,
                        'phone_number'			=>	$sdt  ,
                        'found_by'				=>	NULL,
                        'tag_number'			=>	$tag_number ,
                        'detail'				=>	$detail,
                        'status'				=>	0,
                        'storage_location_id'	=>	1,
                        'category_id'			=>	$category_id ,
                        'location_id'			=>	$location_id ,
                        'created_at'			=>	date('Y-m-d H:i:s', $date_create ),
                        'created_by'			=>	1007,
                        'updated_at'			=>	date('Y-m-d H:i:s' , $date_create )
                );
                $this->db->insert('item', $item);
                $itemID = $this->db->insert_id();
                $isCheck = $this->itemPropertyModel->insert_item($itemID,$itemProperties);
            }else{
                die("not found type");
            }
        }
        
    }
    
}