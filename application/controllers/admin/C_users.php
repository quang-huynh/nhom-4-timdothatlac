<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('smtp-validate-email.php');

class C_users extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

		$session=$this->session->userdata('login');
		if (empty($session)) {
			redirect(base_url('admin/login'));
		}
		if( intval($session['role_id']) != intval(unserialize(ROLE_USER)['admin']) ) {
			redirect(base_url('admin/login'));
		}

		$this->load->model('admin/M_admin');
		$this->load->model('admin/M_users');
	}

	public function index()
	{
		$session=$this->session->userdata('login');

		if(isset($session['session_email']))
		{
			$user=$this->M_users->result('user',NULL);

			$id=$this->input->post('id');

			$row_users=$this->M_users->row('user',array('id'=>$id),NULL);

			if($this->input->post('delete'))
			{
				$data=array('id'=>$id);
				$data_2=array('user_id'=>$id);

				$this->M_users->delete('user',$data);

				$this->M_users->delete('role_user',$data_2);

				$this->session->set_flashdata('delete_success','delete_success');

				redirect(base_url('admin/users'));
			}

			if($this->input->post('status'))
			{
				($row_users['status']==1) ? $status=0 : $status=1;

				$data=array('status'=>$status);

				$this->M_users->update('user',$data,array('id'=>$id));

				$this->session->set_flashdata('status_success','status_success');

				redirect(base_url('admin/users'));
			}

			$name=$this->input->post('name');

			if($this->input->post('change_name'))
			{
				$kiemtra_name=$this->M_users->row('user',array('fullname'=>$name),NULL);

				if($name!=$row_users['fullname'])
				{
					if($kiemtra_name!=0)
					{
						$this->session->set_flashdata('name_exist','name_exist');
					}
					else
					{
						$data=array('fullname'=>$name);

						$this->M_users->update('user',$data,array('id'=>$id));

						$this->session->set_flashdata('changeName_success','changeName_success');
					}
				}

				redirect(base_url('admin/users'));
			}

			$role=$this->input->post('role');

			if($this->input->post('change_role'))
			{
				if($role!=$row_users['created_by'])
				{
					$this->session->set_flashdata('changeRole_success','changeRole_success');
				}

				$data=array('created_by'=>$role);

				$this->M_users->update('user',$data,array('id'=>$id));

				redirect(base_url('admin/users'));
			}

			$password=$this->input->post('password');

			if($this->input->post('change_password'))
			{
				$password=md5($password);

				if($password!=$row_users['password'])
				{
					$this->session->set_flashdata('changePassword_success','changePassword_success');

					$data=array('password'=>$password);

					$this->M_users->update('user',$data,array('id'=>$id));
				}
				else
				{
					$this->session->set_flashdata('NochangePassword_success','NochangePassword_success');
				}

				redirect(base_url('admin/users'));
			}

			$search=$this->input->get('search');

			if($search!=NULL)
			{
				$user=$this->M_users->result('user',$search);
			}

			$data['user']=$user;

			$data['role']=$this->M_users->result('role',NULL);

			$data['delete_success']=$this->session->flashdata('delete_success');
			$data['add_success']=$this->session->flashdata('add_success');
			$data['status_success']=$this->session->flashdata('status_success');
			$data['changeName_success']=$this->session->flashdata('changeName_success');
			$data['changeRole_success']=$this->session->flashdata('changeRole_success');
			$data['changePassword_success']=$this->session->flashdata('changePassword_success');
			$data['NochangePassword_success']=$this->session->flashdata('NochangePassword_success');
			$data['name_exist']=$this->session->flashdata('name_exist');

			$row_session=$this->M_users->row('user',array('email'=>$session['session_email']),NULL);
			$data['row_session']=$row_session;

			$data['main']='users';

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect(base_url('404'));
		}
	}

	public function add_vs_edit()
	{
		$session=$this->session->userdata('login');

		if(isset($session['session_email']))
		{
			$s1=$this->uri->segment(1);
			$s4=$this->uri->segment(4);

			$row_users=$this->M_users->row('user',array('id'=>$s4),NULL);
			$row_session=$this->M_users->row('user',array('email'=>$session['session_email']),NULL);

			$fullname=$this->input->post('fullname');
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			$address=$this->input->post('address');
			$phone_number=$this->input->post('phone_number');
			$gender=$this->input->post('gender');

			// xử lý ngày
			$ngay=$this->input->post('ngay');
			$thang=$this->input->post('thang');
			$year=$this->input->post('year');

			$birthday=$year.':'.$thang.':'.$ngay;
			//end

			$identify_card=$this->input->post('identify_card');
			$role=$this->input->post('role');
			$status=$this->input->post('status');

			$email_list=strtolower($email);
	    	$email_list=explode("\n", str_replace("\r", "", $email_list));

	    	$from='a-happy-camper@campspot.net';

	    	$data['fullname']=$data['email']=$data['address']=$data['phone_number']=$data['gender']=$data['ngay']=$data['thang']=$data['nam']=$data['identify_card']=$data['status']=$data['password']=$data['xd_role']=$data['xd_status']='';

			if($this->input->post())
			{
				if($email)
				{
					foreach($email_list as $item):

		        		$validator=new SMTP_Validate_Email($item,$from);
		    			$smtp_results=$validator->validate();
		                
		                if(!$smtp_results[$item])
		                {
		                	$xd='false';
		                }
		                
		            endforeach;
				}

				$kiemtra_name=$this->M_users->row('user',array('fullname'=>$fullname),NULL);
				$kiemtra_email=$this->M_users->row('user',array('email'=>$email),NULL);

				if($s1=='add')
				{
					if($kiemtra_name!=0)
					{
						$data['fullname']=$fullname;
						$data['email']=$email;
						$data['address']=$address;
						$data['phone_number']=$phone_number;
						$data['gender']=$gender;
						$data['ngay']=$ngay;
						$data['thang']=$thang;
						$data['password']=$password;
						$data['nam']=$year;
						$data['identify_card']=$identify_card;
						$data['xd_role']=$role;
						$data['xd_status']=$status;

						$this->session->set_flashdata('name_exist','name_exist');
					}
					if($kiemtra_email!=0)
					{
						$data['fullname']=$fullname;
						$data['email']=$email;
						$data['address']=$address;
						$data['phone_number']=$phone_number;
						$data['gender']=$gender;
						$data['ngay']=$ngay;
						$data['thang']=$thang;
						$data['password']=$password;
						$data['nam']=$year;
						$data['identify_card']=$identify_card;
						$data['xd_role']=$role;
						$data['xd_status']=$status;

						$this->session->set_flashdata('email_exist','email_exist');
					}
					/*else if($xd=='false')
					{
						$data['fullname']=$fullname;
						$data['email']=$email;
						$data['address']=$address;
						$data['phone_number']=$phone_number;
						$data['gender']=$gender;
						$data['password']=$password;
						$data['ngay']=$ngay;
						$data['thang']=$thang;
						$data['nam']=$year;
						$data['identify_card']=$identify_card;
						$data['status']=$status;

						$this->session->set_flashdata('email_fail','email_fail');
					}*/
					else
					{
						$password=md5($password);

						$add_user = array
						(
							'fullname' => $fullname, 
							'email' => $email, 
							'password' => $password, 
							'address' => $address, 
							'phone_number' => $phone_number, 
							'gender' => $gender,
							'created_by' => $role,
							'birthday' => $birthday,
							'identify_card' => $identify_card,
							'status' => $status,
							'created_by' => $role,
							'created_at' => date("y:m:d h:i:s")
						);

						$this->M_users->add('user',$add_user);

						$row_id_last=$this->M_users->row('user',NULL,'id');

						$add_role = array
						(
							'user_id' => $row_id_last['id'], 
							'role_id' => $role, 
							'created_at' => date("y:m:d h:i:s")
						);

						$this->M_users->add('role_user',$add_role);

						$this->session->set_flashdata('add_success','add_success');

						redirect(base_url('admin/users'));
					}
				}
				else
				{
					$add_user = array
					(
						'fullname' => $fullname, 
						'email' => $email,  
						'address' => $address, 
						'phone_number' => $phone_number, 
						'gender' => $gender,
						'created_by' => $role,
						'birthday' => $birthday,
						'identify_card' => $identify_card,
						'status' => $status,
						'updated_at' => date("y:m:d h:i:s")
					);

					$add_role = array
					(
						'user_id' => $s4, 
						'role_id' => $role, 
						'updated_at' => date("y:m:d h:i:s")
					);

					if($row_users['fullname']!=$fullname)
					{
						if($kiemtra_name!=0)
						{
							$this->session->set_flashdata('name_exist','name_exist');
						}
						else
						{
							if($row_users['fullname']!=$fullname||$row_users['email']!=$email||$row_users['address']!=$address||$row_users['phone_number']!=$phone_number||$row_users['gender']!=$gender||$row_users['created_by']!=$role||$row_users['identify_card']!=$identify_card||$row_users['status']!=$status)
							{
								$this->session->set_flashdata('edit_success','edit_success');
							}

							$this->M_users->update('user',$add_user,array('id' => $s4));
							$this->M_users->update('role_user',$add_role,array('user_id' => $s4));
						}
					}	
					else if($row_users['email']!=$email)
					{
						if($kiemtra_email!=0)
						{
							$this->session->set_flashdata('email_exist','email_exist');
						}
						/*else if($xd=='false')
						{
							$this->session->set_flashdata('email_fail','email_fail');
						}*/
						else
						{
							if($row_users['fullname']!=$fullname||$row_users['email']!=$email||$row_users['address']!=$address||$row_users['phone_number']!=$phone_number||$row_users['gender']!=$gender||$row_users['created_by']!=$role||$row_users['identify_card']!=$identify_card||$row_users['status']!=$status)
							{
								$this->session->set_flashdata('edit_success','edit_success');
							}

							$this->M_users->update('user',$add_user,array('id' => $s4));
							$this->M_users->update('role_user',$add_role,array('user_id' => $s4));
						}
					}
					else
					{
						if($row_users['fullname']!=$fullname||$row_users['email']!=$email||$row_users['address']!=$address||$row_users['phone_number']!=$phone_number||$row_users['gender']!=$gender||$row_users['created_by']!=$role||$row_users['identify_card']!=$identify_card||$row_users['status']!=$status)
							{
								$this->session->set_flashdata('edit_success','edit_success');
							}

						$this->M_users->update('user',$add_user,array('id' => $s4));
						$this->M_users->update('role_user',$add_role,array('user_id' => $s4));
					}

					redirect(base_url('admin/edit/users/'.$s4));
				}
			}

			$data['main']='add_vs_edit_users';
			$data['s1']=$s1;
			$data['row_users']=$row_users;

			$data['role']=$this->M_users->result('role',NULL);

			$data['email_fail']=$this->session->flashdata('email_fail');
			$data['add_success']=$this->session->flashdata('add_success');
			$data['edit_success']=$this->session->flashdata('edit_success');
			$data['name_exist']=$this->session->flashdata('name_exist');
			$data['email_exist']=$this->session->flashdata('email_exist');
			
			$data['row_session']=$row_session;

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect(base_url('404'));
		}
	} 
}