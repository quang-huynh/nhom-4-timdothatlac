<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_admin extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

		$session=$this->session->userdata('login');
		if (empty($session)) {
			redirect(base_url('admin/login'));
		}
		if( intval($session['role_id']) != intval(unserialize(ROLE_USER)['admin']) ) {
			redirect(base_url('admin/login'));
		}

		$this->load->model('admin/M_admin');
	}

	public function index()
	{
		$session = $this->session->userdata('login');
		if(isset($session['email']) && !empty($session['email'])  )
		{
			$row_session=$this->M_admin->row('user',array('email'=>$session['session_email']),NULL);
			$data['row_session']=$row_session;

			$data['count_area']=count($this->M_admin->result('location'));
			$data['count_user']=count($this->M_admin->result('user'));
			$data['count_role']=count($this->M_admin->result('role'));

			$data['main']='main';

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect(base_url('404'));
		}
	}
}