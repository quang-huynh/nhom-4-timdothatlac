<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_area extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$session = $this->session->userdata('login');
		if (empty($session)) {
			redirect(base_url('admin/login'));
		}
		if( intval($session['role_id']) != intval(unserialize(ROLE_USER)['admin']) ) {
			redirect(base_url('admin/login'));
		}


		$this->load->model('admin/M_admin');
		$this->load->model('admin/M_area');
	}

	public function index()
	{
		$session= $this->session->userdata('login');

		if(isset($session['session_email']))
		{
			$user=$this->M_area->result('location',NULL,NULL);

			$id=$this->input->post('id');

			$row_users=$this->M_area->row('location',array('id'=>$id),NULL);

			if($this->input->post('status'))
			{
				($row_users['status']==1) ? $status=0 : $status=1;

				$data=array('status'=>$status);

				$this->M_area->update('location',$data,array('id'=>$id));

				$this->session->set_flashdata('status_success','status_success');

				redirect(base_url('admin/area'));
			}

			if($this->input->post('delete'))
			{
				$data=array('id'=>$id);

				$this->M_area->delete('location',$data);

				$child_cat=$this->M_area->result('location',array('parent_id'=>$id),NULL);

				foreach($child_cat as $v_child):

					$this->M_area->delete('location',array('id'=>$v_child['id']));

					$child_cat2=$this->M_area->result('location',array('parent_id'=>$v_child['id']),NULL);

					foreach($child_cat2 as $v_child2):

						$this->M_area->delete('M_area',array('id'=>$v_child2['id']));

					endforeach;

				endforeach;

				$this->session->set_flashdata('delete_success','delete_success');

				redirect(base_url('admin/area'));
			}

			$name=$this->input->post('name');

			if($this->input->post('change_name'))
			{
				$kiemtra_name=$this->M_area->row('location',array('name'=>$name),NULL);

				if($name!=$row_users['name'])
				{
					if($kiemtra_name!=0)
					{
						$this->session->set_flashdata('name_exist','name_exist');
					}
					else
					{
						$data=array('name'=>$name);

						$this->M_area->update('location',$data,array('id'=>$id));

						$this->session->set_flashdata('changeName_success','changeName_success');
					}
				}

				redirect(base_url('admin/area'));
			}

			$search=$this->input->get('search');

			if($search!=NULL)
			{
				$user=$this->M_area->result('location',NULL,$search);
			}

			$data['user']=$user;

			$data['delete_success']=$this->session->flashdata('delete_success');
			$data['add_success']=$this->session->flashdata('add_success');
			$data['status_success']=$this->session->flashdata('status_success');
			$data['changeName_success']=$this->session->flashdata('changeName_success');
			$data['name_exist']=$this->session->flashdata('name_exist');

			$data['main']='area';

			$row_session=$this->M_area->row('user',array('email'=>$session['session_email']),NULL);
			$data['row_session']=$row_session;

			$this->load->view('admin/index',$data);
		} else
		{
			redirect(base_url('404'));
		}
	}

	public function add_vs_edit()
	{
		$session = $this->session->userdata('login');

		if(isset($session['session_email']))
		{
			$s1=$this->uri->segment(1);
			$s4=$this->uri->segment(4);

			$row_session=$this->M_area->row('user',array('email'=>$session['session_email']),NULL);

			$row_users=$this->M_area->row('location',array('id'=>$s4),NULL);

			$name=$this->input->post('name');
			$description=$this->input->post('description');
			$parent_id=$this->input->post('parent_id');
			$status=$this->input->post('status');

			if($this->input->post())
			{
				if($s1=='add')
				{
					$add_user = array
					(
						'name' => $name, 
						'description' => $description, 
						'parent_id' => $parent_id, 
						'status' => $status,
						'created_by' => $row_session['id'],
						'created_at' => date("y:m:d h:i:s")
					);

					$this->M_area->add('location',$add_user);

					$this->session->set_flashdata('add_success','add_success');

					redirect(base_url('admin/area'));
				}
				else
				{
					$date='';

					if($row_users['name']!=$name || $row_users['description']!=$description || $row_users['parent_id']!=$parent_id||$row_users['status']!=$status)
					{
						$this->session->set_flashdata('edit_success','edit_success');

						$date=date("y:m:d h:i:s");
					}

					$add_user = array
					(
						'name' => $name, 
						'description' => $description, 
						'parent_id' => $parent_id, 
						'status' => $status,
						'updated_at' => $date
					);

					$this->M_area->update('location',$add_user,array('id' => $s4));

					redirect(base_url('admin/edit/area/'.$s4));
				}
			}

			$data['main']='add_vs_edit_area';

			$data['s1']=$s1;

			$data['location']=$this->M_area->result('location',NULL,NULL);

			$data['row_session']=$row_session;
			$data['row_users']=$row_users;

			$data['add_success']=$this->session->flashdata('add_success');
			$data['edit_success']=$this->session->flashdata('edit_success');

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect(base_url('404'));
		}
	}
}