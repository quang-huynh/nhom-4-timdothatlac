<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_role extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

		$session=$this->session->userdata('login');
		if (empty($session)) {
			redirect(base_url('admin/login'));
		}
		if( intval($session['role_id']) != intval(unserialize(ROLE_USER)['admin']) ) {
			redirect(base_url('admin/login'));
		}

		$this->load->model('admin/M_admin');
		$this->load->model('admin/M_role');
	}

	public function index()
	{
		$session=$this->session->userdata('login');

		if(isset($session['session_email']))
		{
			$role=$this->M_role->result('role',NULL);

			$id=$this->input->post('id');

			$row_users=$this->M_role->row('role',array('id'=>$id),NULL);

			if($this->input->post('delete'))
			{
				$data=array('id'=>$id);

				$this->M_role->delete('role',$data);

				$this->session->set_flashdata('delete_success','delete_success');

				redirect(base_url('role/admin'));
			}

			$name=$this->input->post('name');

			if($this->input->post('change_name'))
			{
				$kiemtra_name=$this->M_role->row('role',array('name'=>$name),NULL);

				if($name!=$row_users['name'])
				{
					if($kiemtra_name!=0)
					{
						$this->session->set_flashdata('name_exist','name_exist');
					}
					else
					{
						$data=array('name'=>$name);

						$this->M_role->update('role',$data,array('id'=>$id));

						$this->session->set_flashdata('changeName_success','changeName_success');
					}
				}

				redirect(base_url('role/admin'));
			}

			$search=$this->input->get('search');

			if($search!=NULL)
			{
				$role=$this->M_role->result('role',$search);
			}

			$data['user']=$role;

			$data['delete_success']=$this->session->flashdata('delete_success');
			$data['add_success']=$this->session->flashdata('add_success');
			$data['status_success']=$this->session->flashdata('status_success');
			$data['changeName_success']=$this->session->flashdata('changeName_success');
			$data['name_exist']=$this->session->flashdata('name_exist');

			$row_session=$this->M_role->row('user',array('email'=>$session['session_email']),NULL);
			$data['row_session']=$row_session;

			$data['main']='role';

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect(base_url('404'));
		}
	}

	public function add_vs_edit()
	{
		$session=$this->session->userdata('login');

		if(isset($session['session_email']))
		{
			$s1=$this->uri->segment(1);
			$s4=$this->uri->segment(4);

			$row_role=$this->M_role->row('role',array('id'=>$s4),NULL);
			$row_session=$this->M_role->row('user',array('email'=>$session['session_email']),NULL);

			$name=$this->input->post('name');
			$description=$this->input->post('description');

			$kiemtra_name=$this->M_role->row('role',array('name'=>$name),NULL);

			$data['name']=$data['description']='';

			if($this->input->post())
			{
				if($s1=='add')
				{
					if($kiemtra_name!=0)
					{
						$this->session->set_flashdata('name_exist','name_exist');

						$data['name']=$name;
						$data['description']=$description;
					}
					else
					{
						$add_user = array
						(
							'name' => $name, 
							'description' => $description, 
							'created_by' => $row_session['id'],
							'created_at' => date("y:m:d h:i:s")
						);

						$this->M_role->add('role',$add_user);

						$this->session->set_flashdata('add_success','add_success');

						redirect(base_url('role/admin'));
					}
				}
				else
				{
					if($row_role['name']!=$name)
					{
						if($kiemtra_name!=0)
						{
							$this->session->set_flashdata('name_exist','name_exist');
						}
						else
						{
							$add_user = array
							(
								'name' => $name, 
								'description' => $description, 
								'updated_at' => date("y:m:d h:i:s")
							);

							$this->M_role->update('role',$add_user,array('id' => $s4));

							$this->session->set_flashdata('edit_success','edit_success');
						}
					}
					else
					{
						if($row_role['description']!=$description)
						{
							$this->session->set_flashdata('edit_success','edit_success');

							$date=date("y:m:d h:i:s");
						}

						$add_user = array
						(
							'description' => $description, 
							'updated_at' => $date
						);

						$this->M_role->update('role',$add_user,array('id' => $s4));
					}

					redirect(base_url('edit/role/admin/'.$s4));
				}
			}

			$data['main']='add_vs_edit_role';
			$data['s1']=$s1;
			$data['row_role']=$row_role;

			$data['name_exist']=$this->session->flashdata('name_exist');
			$data['add_success']=$this->session->flashdata('add_success');
			$data['edit_success']=$this->session->flashdata('edit_success');

			$data['row_session']=$row_session;

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect(base_url('404'));
		}
	} 
}