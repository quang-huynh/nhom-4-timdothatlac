<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_login extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

		$this->load->model('admin/M_admin');
		$this->load->model('admin/M_login');
		$this->load->model('user_model', 'userModel');
		$this->load->model('role_model', 'roleModel');
	}

	public function index()
	{
		$session=$this->session->userdata('login');
		// var_dump($session['email']);exit;
		if(!empty($session['email']) && $session['role_id'] == intval(unserialize(ROLE_USER)['admin']) ) {
			redirect(base_url('admin'));
		}
		
		$email=$this->input->post('email');
		$password=$this->input->post('password');

		$password=md5($password);

		$data['email']=$data['password']='';

		if($this->input->post('login'))
		{
			$test_email=$this->M_login->row('user',array('email'=>$email));
			$test_password=$this->M_login->row('user',array('password'=>$password,'status'=>1));

			if($test_email==0)
			{
				$this->session->set_flashdata('email_fail','email_fail');

				$data['email']=$email;
				$data['password']=$password;
			}
			else if($test_password==0)
			{
				$this->session->set_flashdata('password_fail','password_fail');

				$data['email']=$email;
				$data['password']=$password;
			}
			else
			{
				$user = $this->userModel->getByRules( array('email' => trim($email) ) );
		 		$role = $this->roleModel->getRoleByUserId($user->id);
				$this->session->set_userdata('login',array(
														'islogin'			=>	true,
													 	'_iduser'			=>	$user->id,
													 	'role_id'			=>	$role->role_id,
													 	'fullname'			=>	$user->fullname,
													 	'email'				=>	$email,
													 	'session_email'		=> 	$email,
													 	'lastlogin'			=>	time()
														));
				redirect(base_url('admin'));
			}
		}

		$data['email_fail']=$this->session->flashdata('email_fail');
		$data['password_fail']=$this->session->flashdata('password_fail');

		$this->load->view('admin/login',$data);
	}

	function logout()
	{
		$this->session->sess_destroy();
		
		redirect(base_url('admin/login'));
	}
}