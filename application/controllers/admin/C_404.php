<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_404 extends CI_Controller 
{
	public function index()
	{
		$this->load->view('admin/404');
	}
}