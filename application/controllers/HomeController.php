<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// always call this to check login in construct function
	}

    public function error(){
        // $data['title'] = 'trang bạn vừa tìm không thấy';
        // $data['content'] = 'frontend/item/item_detail';
        $this->load->view('error_404');
  	}
}