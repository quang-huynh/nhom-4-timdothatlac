<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		// always call this to check login in construct function
		$login = $this->session->userdata('login');
		//var_dump($login);exit;
		if( !$login ) 
			redirect(base_url('user/login'));
		

	}

	public function index()
	{

		$data = array();
		$data['title'] = '';
		$data['content'] = 'frontend/dashboard';
		$this->load->view('frontend/template/master', $data);
	}

}