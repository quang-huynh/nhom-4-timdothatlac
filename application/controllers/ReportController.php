<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ReportController extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        // always call this to check login in construct function
        $this->load->helper('url');
        $this->load->helper('url');
        $login = $this->session->userdata('login');
        if( !$login ) 
            redirect(base_url('user/login'));
        $this->load->model('item_model', 'itemModel');
        $this->load->model('Matched_Item_model', 'matchedItemModel');
        $this->load->model('Item_Action_model', 'itemActionItemModel');
        $this->load->library('session');
        $this->load->helper('form');
    }

    /**
     * @author ntdat
     * @return: list data item report
     */
    public function report()
    {
        $data = array(
            'content' => 'frontend/report/report_page'           
        );
        $this->load->view('frontend/template/master', $data);
    }

    /**
     * api
     */
    public function getDataReport() {
        $dataItemLost = $this->itemModel->getDataReportItemLost();
        $dataItemFound = $this->itemModel->getDataReportItemFound();
        $dataItemMatched = $this->matchedItemModel->getDataReportItemMatched();
        $dataItemAction = $this->itemActionItemModel->getDataReportItemAction();

        $dataItemFoundTotal = $this->itemModel->getDataReportTotalFound();
        $dataItemLostTotal = $this->itemModel->getDataReportTotalLost();
        $dataItemMatchedTotal = $this->matchedItemModel->getDataReportTotalMatched();
        $dataItemActionTotal = $this->itemActionItemModel->getDataReportTotalAction();

        $listDataItem = array(
            'dataItemLost'    => $dataItemLost,
            'dataItemFound'   => $dataItemFound,
            'dataItemMatched' => $dataItemMatched,
            'dataItemAction'  => $dataItemAction, 
            'dataItemFoundTotal'  => $dataItemFoundTotal,
            'dataItemLostTotal'  => $dataItemLostTotal,
            'dataItemMatchedTotal'  => $dataItemMatchedTotal,
            'dataItemActionTotal'  => $dataItemActionTotal,
        );
        $this->output->cache(1);
        echo json_encode($listDataItem);
    }

    /**
     * clear cache
     */
    public function clearCache() {
        $this->output->delete_cache('/ReportController/getDataReport');
    }
}