<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserController extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// always call this to check login in construct function
		$this->load->helper('url');
		$this->load->helper('url');
        $login = $this->session->userdata('login');
        if( $login ) 
            redirect(base_url());
		$this->load->model('user_model', 'userModel');

		$this->load->library('session');
		$this->load->helper('form');
	}

	public function register()
	{
		$self = $this->input->server('REQUEST_URI');
		if( $this->input->server('REQUEST_METHOD') == 'POST' ) {
		 	$this->load->library('form_validation');

			 $data = $this->input->post('user');

			date_default_timezone_set('Asia/Ho_Chi_Minh');

			$data['password'] = md5($data['password']);
			$data['birthday'] = DateTime::createFromFormat('m/d/Y', $data['birthday']);
			if( false!==$data['birthday'] )
				$data['birthday'] = $data['birthday']->format('Y-m-d');
			$data['created_at'] = date('Y-m-d');
			$data['created_by'] = '';
			$image = $this->upload_profile();
			if( !empty($image['file_name']) ) {
				$data['profile_picture'] = '/public/upload/user/'. $image['file_name'];
			}

			$this->form_validation->set_rules('user[birthday]', 'Ngày sinh', 'callback_valid_date');
			$this->form_validation->set_rules('user[email]', 'Email', 'required|valid_email|is_unique[user.email]', array('required'=>'Vui lòng nhập email', 'valid_email'=>'Email không đúng định dạng', 'is_unique' => 'Email đã tồn tại trong hệ thống'));
			$this->form_validation->set_rules('user[fullname]', 'Họ tên', 'required', array('required'=>'Vui lòng nhập họ tên', 'is_unique' => 'Email đã tồn tại trong hệ thống'));

			if( $this->form_validation->run() ) {
			 	$flag = $this->userModel->insert($data);
			 	if( $flag ) {
			 		$this->load->model('role_model', 'roleModel');
			 		$this->roleModel->insertUserRole(array(
			 			'user_id'		=>	$flag,
			 			'role_id'		=>	3,
			 			'created_at'	=>	date('Y-m-d H:i:s'),
			 			'updated_at'	=>	date('Y-m-d H:i:s')
			 		));
					$this->session->set_flashdata('notify', array('status'=>'success', 'caption'=>'Đăng ký thành công!', 'message'=>'Đăng ký thành công. <a href="'.site_url('user/login').'" style="color:red">Đăng nhập</a> ngay'));
				} else {
					$this->session->set_flashdata('notify', array('status'=>'error', 'caption'=>'Thất bại!','message'=>'Lỗi hệ thống'));
				}

				redirect($self);
			}			
		} 

		$this->load->helper('form');
		$data['title'] = "Đăng ký tài khoản";
		$data['content'] = 'frontend/users/register_user';
		$data['notify'] = $this->session->flashdata('notify');
		$this->load->view('frontend/template/master', $data);	
	}

	public function valid_date()
	{
		$data = $this->input->post('user');
		$birthday = DateTime::createFromFormat('m/d/Y', $data['birthday']);
		if( !empty($birthday) &&  false===$birthday ) {
		  	$this->form_validation->set_message(__FUNCTION__, 'Ngày sinh không đúng định dạng');
			return false;
		}
		return true;
	}

	public function upload_profile()
	{
		//Khai bao bien cau hinh
		$config = array();
		//thuc mục chứa file
		$config['upload_path']   = './public/upload/user';
		//Định dạng file được phép tải
		$config['allowed_types'] = 'jpg|png|gif';
		//Dung lượng tối đa
		$config['max_size']      = '500';
		//Chiều rộng tối đa
		$config['max_width']     = '1028';
		//Chiều cao tối đa
		$config['max_height']    = '1028';
		//load thư viện upload
		$this->load->library('upload', $config);
		//thuc hien upload
		if($this->upload->do_upload('image')) {
			//chua mang thong tin upload thanh con
			$data = $this->upload->data();
			//in cau truc du lieu cua file da upload
			return $data;
		} else {
			$error = $this->upload->display_errors();
			return $error;
		}
	}
}