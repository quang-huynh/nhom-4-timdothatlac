<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NotificationController extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        // always call this to check login in construct function
        $this->load->helper('url');
        $login = $this->session->userdata('login');
        if( !$login ) 
            redirect(base_url('user/login'));
        $this->load->model('notification_model', 'notificationModel');
        // load Pagination library
        $this->load->library('pagination');
    }

    /**
     * @author ntdat
     * @return: list notification by user
     */
    public function notification()
    {
        $config = array();
        $config["base_url"] = base_url("thong-bao");
        $total_row = $this->notificationModel->get_total();
        $config["total_rows"] = $total_row;
        $config["per_page"] = 6;
        $config['full_tag_open'] = '<ul class="pager__items js-pager__items">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="pager__item is-active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li class="pager__item pager__item--next">';
        $config['next_tagl_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="pager__item pager__item--previous">';
        $config['prev_tagl_close'] = '</li>';
        $config['first_tag_open'] = '<li class="pager__item is-active">';
        $config['first_tagl_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tagl_close'] = '</a></li>';                
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);

        if($this->uri->segment(2)) {
            $page = ($this->uri->segment(2)) ;
        } else {
            $page = 1;
        }

        $notifications = $this->notificationModel->fetch_data($config["per_page"], $page);
        $str_links = $this->pagination->create_links();
        $links = explode('&nbsp;',$str_links );
        
        if($notifications) {    
            //$notifications = $this->notificationModel->getAll();
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            foreach($notifications as $item) {
                $item->created_at = date('d/m/Y h:i A', strtotime($item->created_at));
            }
        }
        $data = array(
            'content'       => 'frontend/notification/notification_page',
            'notifications' => $notifications,
            'links'         => $links
        );
        $this->load->view('frontend/template/master', $data);
    }

     /**
     * @author ntdat
     * @return: list notification by user
     */
    public function search()
    { 
        if( $this->input->server('REQUEST_METHOD') == 'GET') {
            $data = $this->input->get();
            $dateStart = $data['dateRangeStart'];
            $dateEnd = $data['dateRangeEnd'];
            $dateStart = date('Y-m-d h:i:s', strtotime($dateStart));
            $dateEnd = date('Y-m-d h:i:s', strtotime($dateEnd));
            $notifications = $this->notificationModel->searchQuery($dateStart, $dateEnd);
    
            foreach($notifications as $item) {
                $item->created_at = date('d/m/Y h:i A', strtotime($item->created_at));
            }
    
            $data = array(
                'content' => 'frontend/notification/notification_page',
                'notifications' => $notifications,
            );
            $this->load->view('frontend/template/master', $data);
        }
    }

    /**
     * @author  ntdat
     * api - notification
     */
    public function getAllByStatus() {
		if($this->input->server('REQUEST_METHOD') == 'GET'){
			$data = $this->notificationModel->getAllByStatus();
            echo json_encode($data);
		}
    }
}