
<?php 
class WorkerController extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('Curl');
        $this->load->model('Worker_model','workerModel');
    }

    public function index(){
        if( $this->input->server('REQUEST_METHOD') == 'POST' ){
            $this->action( $this->input->post('second') );
        }
        $dataWorker = $this->workerModel->getAll();
        if( isset($dataWorker) ) $data['dataWorker'] =  $dataWorker;
        $data['title'] = 'some thing';
        $this->load->view('frontend/worker/worker',$data);

    }
    
    public function action($second=0){
        $headers[] = 'Content-type: application/x-www-form-urlencoded; charset=UTF-8';
        $this->curl->create(base_url('ItemController/worker/'.$second));
        $this->curl->options(array(CURLOPT_BUFFERSIZE => 10, 
            CURLOPT_HTTPHEADER => $headers, 
            CURLOPT_HEADER => 0,
            CURLOPT_TIMEOUT => 4,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0
        ));
        echo $return = $this->curl->execute();
    }
}