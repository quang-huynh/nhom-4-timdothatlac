<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AuthController extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// always call this to check login in construct function
		$this->load->helper('url');

		$this->load->model('user_model', 'userModel');
		$this->load->model('role_model', 'roleModel');
	}

	public function login()
	{
		$session = $this->session->userdata('session');
		if(isset($session['session_email'])){
			$user = $this->userModel->getByRules(array('email' => trim($session['session_email'])));
		 	$role = $this->roleModel->getRoleByUserId($user->id);

		 	$this->session->set_userdata('login', array(
			 	'islogin'			=>	true,
			 	'_iduser'			=>	$user->id,
			 	'role_id'			=>	$role->role_id,
			 	'fullname'			=>	$user->fullname,
			 	'email'				=>	$email,
			 	'session_email'		=> 	$email,
			 	'lastlogin'			=>	time()
		 	));

		 	redirect(base_url());
		} else {
			$login = $this->session->userdata('login');
			if( $login ) 
				redirect(base_url());

			$self = $this->input->server('REQUEST_URI');
			if( $this->input->server('REQUEST_METHOD') == 'POST' ) {
				 $this->load->library('form_validation');
				 $this->form_validation->set_rules('email', 'Email', 'required|valid_email', array('required'=>'Vui lòng nhập email', 'valid_email'=>'Email không đúng định dạng'));
				  $this->form_validation->set_rules('password', 'Mật khẩu', 'required|callback_checklogin', array('required'=>'Vui lòng nhập mật khẩu'));
				 $email = $this->input->post('email');
				 $password = $this->input->post('password');
				 if( $this->form_validation->run() ) {
				 	$user = $this->userModel->getByRules(array('email' => trim($email)));
				 	$role = $this->roleModel->getRoleByUserId($user->id);

				 	$this->session->set_userdata('login', array(
					 	'islogin'	=>	true,
					 	'_iduser'	=>	$user->id,
					 	'role_id'	=>	$role->role_id,
					 	'fullname'	=>	$user->fullname,
					 	'email'		=>	$email,
					 	'session_email'		=> 	$email,
					 	'lastlogin'	=>	time()
				 	));
				 	$this->session->set_userdata('session',array('session_email'=>$email));
				 	redirect(base_url());
				 }
			} 

			$this->load->helper('form');
			$data['title'] = "Đăng nhập";
			$data['content'] = 'frontend/users/login';
			$data['notify'] = $this->session->flashdata('notify');
			$this->load->view('frontend/template/master', $data);
		}			
	}

	public function checklogin()
	{
		$email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->userModel->getByRules(array('email' => trim($email)));
        
        if ( !count($user) ) {
            $this->form_validation->set_message(__FUNCTION__, 'Email không tồn tại!');
            return false;
        } else {
            if ($this->userModel->checkExists(array('email' => $email, 'password' => md5( trim($password) )))) {
                return true;
            } else {
                $this->form_validation->set_message(__FUNCTION__, 'Sai mật khẩu!');
                return false;
            }
        }
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}