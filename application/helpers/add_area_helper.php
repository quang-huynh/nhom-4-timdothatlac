<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('add_area'))
{
    function add_area($array, $i=0, $char='')
    {
        foreach($array as $k_array => $v_array):
          
            if($v_array['parent_id']==$i)
            {
                echo '<option value="'.$v_array['id'].'">'.$char.$v_array['name'].'</option>';
                
                unset($array['k_array']);

                add_area($array, $v_array['id'], $char.' |----- ');
            }

        endforeach;
    }
}