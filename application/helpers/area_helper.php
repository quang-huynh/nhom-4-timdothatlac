<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('area'))
{
    function area($array, $i=0, $char='')
    {
        $CI=&get_instance();

        $count=0;

        foreach($array as $k_array => $v_array): $count++;

        	if($v_array['status']==1){$name='Đã kích hoạt'; $label='success';}
        	else{$name='Đã tắt'; $label='danger';}

            //$row_role=$CI->M_area->row('role',array('id'=>$v_array['created_by']),NULL);<td class="text-center">'.$row_role['name'].'</td>

            if($v_array['parent_id']==$i)
            {
                echo '
                <tr>
                    <td class="text-center">'.$count.'</td>
                    <td>'.$char.$v_array['name'].'<a href="#" data-toggle="modal" data-target="#change_name'.$v_array['id'].'" title="Đổi tên khu vực"></a></td>
                    
                    <td class="text-center">'.$v_array['created_at'].'</td>
                    <td class="text-center"><span class="label label-'.$label.'">'.$name.'</span></td>
                    <td class="text-center">
                        <a class="btn btn-circle btn-icon-only btn-default" style="color:#3c8dbc;" href="'.base_url('admin/edit/area/'.$v_array['id']).'" title="Chỉnh sửa khu vực">
                        	<span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" style="color:#dd4b39;" data-toggle="modal" data-target="#delete_users'.$v_array['id'].'" title="Xóa khu vực">
                        	<span class="glyphicon glyphicon-trash"></span>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" style="color:green;" data-toggle="modal" data-target="#change_status'.$v_array['id'].'" title="Đổi trạng thái">
                        	<span class="glyphicon glyphicon-refresh"></span>
                        </a>
                    </td>
                </tr>';

        		unset($array['k_array']);

        		area($array, $v_array['id'], $char.' |----- ');
            }

        endforeach;
    }
}