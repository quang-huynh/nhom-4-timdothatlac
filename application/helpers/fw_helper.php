<?php 
if(!function_exists('_pagination')){
    function _pagination(){
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['last_link'] = 'Last &laquo;';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';
        $config['next_link'] = 'Next &raquo;';
        $config['next_tag_open'] = '';
        $config['next_tag_close'] = '';
        $config['prev_link'] = '&laquo; Previous';
        $config['prev_tag_open'] = '';
        $config['prev_tag_close'] = '';
        $config['cur_tag_open'] = '<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['per_page'] = 20;
        
        return $config;
    }
}