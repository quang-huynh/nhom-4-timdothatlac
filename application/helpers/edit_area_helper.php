<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('edit_area'))
{
    function edit_area($array, $row, $i=0, $char='')
    {
        foreach($array as $k_array => $v_array):

            if($v_array['parent_id']==$i && $v_array['id']!=$row['id'])
            {
                ($v_array['id']==$row['parent_id']) ? $act_area='selected' : $act_area='';

                echo '<option value = "'.$v_array['id'].'" '.$act_area.'>'.$char.$v_array['name'].'</option>';
                
                unset($array['k_array']);

                edit_area($array,$row,$v_array['id'],$char.' |----- ');
            }

        endforeach;
    }
}