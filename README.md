# README #

This README would normally document whatever steps are necessary to get your application up and running.

I.	Mô tả dự án
Tên đồ án: WEBSITE TÌM KIẾM VẬT THẤT LẠC
-	Website này cho phép người dùng có thể đăng thông tin vật thất lạc hoặc người tìm được vật thất lạc có thể đăng thông tin mà vật tìm được, mục đích website gắn kết giữa 2 đối tượng này lại với nhau.
-	Thống kê dữ liệu về tình trạng đồ thất lạc của từng nhóm khu vực, địa điểm.
1.	Vấn đề cần giải quyết
-	Một ngày nào đó bạn đánh mất vật cá nhân ở trường, bất kỳ phòng học hay khu vực nào, bạn không biết cách để tìm lại nó hoặc nhờ ai giúp đỡ ?
-	Bạn tìm được vật thất lạc trong trường nhưng không biết của ai, trả lại họ như thế nào ?
-	Bạn muốn biết tình trạng hiện tại đồ thất lạc của trường mình ra sao và đã có những đồ vật nào được tìm thấy.
-	Bạn đang viết một website tin tức dành cho trường, cần  biết về tình trạng an toản của trường nhưng không biết lấy dữ liệu từ đâu và không biết dữ liệu đó chính xác hay không ? 
	Website sẽ giải quyết được vấn đề đó cho bạn.
2.	Giải pháp cho vấn đề
-	Website cho phép người bị mất hoặc người tìm được đồ thất lạc có thể đăng thông tin của món đồ đó lên website như là: hình ảnh, tên của món đồ, phân loại, tìm thấy hoặc bị mất ở khu vực nào, mô tả thông tin món đồ, thông tin người đăng v..v….Khi đăng thông tin thành công website sẽ hiển thị những món đồ cùng loại hoặc những thông tin liên quan đến sản phẩm đó, cho phép người đăng trực tiếp liên hệ với nhau.
-	Dựa vào những thông tin, dữ liệu mà người đăng trên website, sẽ tổng hợp và thống kê lại tình trạng thất lạc của từng nhóm, địa điểm, khu vực.
II.	Đối tượng liên quan
1.	Nhóm phát triển phần mềm
-	Các thành viên Nhóm 4
-	Môn học: Quản lý quy trình phần mềm, Khoa CNTT, Trường Đại học khoa học tự nhiên TP.HCM.
2.	Nhóm người dùng
-	Tất cả sinh viên ở Trường Đại học khoa học tự nhiên TP.HCM có nhu cầu tìm kiếm đồ thất lạc.
